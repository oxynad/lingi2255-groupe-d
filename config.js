  
var _ = require('underscore');

var defaultConfig = {
  db: {
    url: "mongodb://asmae:asmae1337@ds033734.mongolab.com:33734/asmae"
    // url: "mongodb://localhost/ASMAE"
  },
  session: {
    secret: 'sjqhz38s9!S',
    resave: true,
    saveUninitialized: false,
    cookie: {
      maxAge: 10000000,
      httpOnly: true
    },
    rolling: true // will 'touch' cookie every time a request is made
  },
  server: {
    url: "/"
  },
  paypal: {
    "port": 5000,
    "api": {
      "mode": "sandbox",
      "host": "api.sandbox.paypal.com",
      "port": "",
      "client_id": "Aar1KfCTiKfOi1Qm5zE7_8cXnDMvbPpF7UP1SEm3XdafXAWMgnIqeN2GCPpa8atxtpqA9_SAvP-3hgxk",
      "client_secret": "EF8GuZ8hwPk6Wr32cuYJgOqx7iOISJaykUXxrLGzb0a90oGqo47Z4-OQiUowl_k3hb-9x0mmOGw8660-"
    }
  },
  mail: {
    service: 'Gmail',
    auth: {
      user: 'asmaetennis@gmail.com',
      pass: 'c0TyBOBpLG'
    }
  },
  tournoiDesFamilles:{
    ageMaxEnfant: 15,
    ageMinAdulte: 25
  }

};

var Config = function(override) {
  return _.extend(_.clone(defaultConfig), override);
};

var configs = {
  dev : new Config(),
  test : new Config({db : { url : "mongodb://localhost/ASMAE-TEST"}}),
  prod : new Config()
};

// module.exports = configs[(process.env.NODE_ENV || 'dev')];
module.exports = configs.prod;