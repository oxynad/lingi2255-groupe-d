var request = require('supertest');

var chai = require('chai');
chai.should();

var mongoose = require('mongoose');
var Pair = mongoose.model('Pair');
var datalayer = require('./../app/datalayer');
var PlayerPersister = datalayer.persisters.PlayerPersister;
var Pairpersister = datalayer.persisters.PairPersister;


module.exports = function(server) {

    describe('Pair registration', function() {


        it('should get the player registration page', function(done) {
            request(server)
                .get('/register')
                .expect(200, done);
        });

        it('should create the pair', function(done) {

            var body = {
                title1: 'Mr',
                first_name1: 'Arthur',
                last_name1: 'Node',
                street1: 'Rue du tennis',
                streetNumber1: 55,
                box1: 33,
                zipCode1: 1050,
                city1: 'Bruxelles',
                gsm1: '049848293',
                birthDate1: '21/11/1993',
                email1: 'arthur@nodejs.com',
                aftRanking1: 'C30.5',
                title2: 'Mr',
                first_name2: 'Dan',
                last_name2: 'Rails',
                street2: 'Rue du ping pong',
                streetNumber1: 100,
                box1: 11,
                zipCode1: 1200,
                city1: 'Bruxelles',
                gsm1: '042348293',
                birthDate1: '18/10/1993',
                email1: 'dan@rails.com',
                aftRanking1: 'C15.3',
                comments: 'Commentaire'
            };


            request(server)
                .post('/register/add')
                .send(body)
                .expect(302) // Redirect
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.headers.location.should.equal('/registered/');
                    done();
                })
        });

        it('should exist in the database', function(done) {

            Pair.find({}).populate('players').execAsync().then(function(pairs) {
                pairs[0].players.length.should.equal(2); // 2 players in the pair
                pairs[0].comment.should.be.not.empty;
                return Pair.findByIdAndRemoveAsync(pairs[0]._id);
            }).then(function() {
                done();
            }).catch(function(err) {
                throw err;
            })
        });


        

    });



};