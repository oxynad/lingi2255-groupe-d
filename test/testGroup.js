var request = require('supertest');

var chai = require('chai');
chai.should();

var mongoose = require('mongoose');
var datalayer = require('./../app/datalayer');
var TournamentPersister = datalayer.persisters.TournamentPersister;
var PairPersister = datalayer.persisters.PairPersister;
var persisters = require('./../app/datalayer/persistence');
var Tournament = mongoose.model('Tournament');
var _ = require('underscore');


module.exports = function(server) {


    var agent = request.agent(server);

    function loginUser() {
        return function(done) {
            agent
                .post('/login')
                .send({
                    username: 'admin',
                    password: 'password'
                })
                .expect(302)
                .expect('Location', '/staff')
                .end(onResponse);

            function onResponse(err, res) {
                if (err) return done(err);
                return done();
            }
        };
    };


    describe('Group creation', function() {

        var testTournamentId;
        var testTournament;
        before(function(done) {
            // Tournament creation

            // Pop the tournamentPersister so we don't call the populate method below
            var TournamentPersister = persisters.All.pop();
            // call the populate method on all the other persisters
            var promises = persisters.All.map(function(persister) {
                return persister.populate();
            });

            Promise.all(promises).then(function() {

                // Create a tournament
                TournamentPersister.createTournament({
                    status: 'Poule',
                    category: "Pré-minimes",
                    day: "Saturday",
                    type: "homme",
                    ageMin: 1999,
                    ageMax: 2000
                }).spread(function(tournament) {
                    testTournamentId = tournament._id;
                    done();
                })
            });

        });

        describe('Group methods', function() {

            it('should find pairs not already in group', function(done) {
                TournamentPersister.findPairsNotInGroup(testTournamentId).then(function(pairs) {
                    pairs.length.should.equal(4);
                    done();
                })
            });

            it('should find courts not already used by a group', function(done) {
                TournamentPersister.findCourtNotAlreadyUsed(testTournamentId).then(function(courts) {
                    courts.length.should.equal(2);
                    done();
                })
            });
        });

        describe('Creating a group', function() {

            it('should login staff', loginUser());

            // it('should get the group creation page', function(done) {
            //     agent.get('/staff/tournaments/edit/' + testTournamentId + '/group/new')
            //         .expect('Location', '/staff/tournaments/edit/' + testTournamentId + '/group/new')
            //         .end(function(err, res) {
            //             if (err) {
            //                 throw err;
            //             }
            //             done();
            //         })
            // });

            it('should create a group', function(done) {

                var params = {
                    pairs: ['5669d939e7f5784620f56d1c',
                        '5669d939e7f5784620f56d1d',
                        '5669d939e7f5784620f56d1e',
                        '5669d939e7f5784620f56d1f',
                        '5669d939e7f5784620f56d20'
                    ],
                    tLeader: 'Dan Martens',
                    nbPairs: '2',
                    courtId: '5669093532ed88a07ad8ea39'
                };
                params = JSON.stringify(params);
                agent.post('/staff/tournaments/edit/' + testTournamentId + '/group/create/' + params)
                    .expect(200)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        done();
                    })
            });

            it('should exist in database', function(done) {
                Tournament.findByIdAsync(testTournamentId).then(function(tournament) {
                    testTournament = tournament;
                    tournament.groups.length.should.equal(1);
                    // var pairs = _.map(['5669d939e7f5784620f56d1c',
                    //     '5669d939e7f5784620f56d1d',
                    //     '5669d939e7f5784620f56d1e',
                    //     '5669d939e7f5784620f56d1f',
                    //     '5669d939e7f5784620f56d20'
                    // ], function(pairId) {
                    //     return mongoose.Types.ObjectId(pairId);
                    // });
                    // tournament.groups[0].pairs.should.equal(pairs);
                    done();
                })
            });

        });


        describe('Encode match results', function() {

            it('should login staff', loginUser());

            it('should encode a match result', function(done) {

                var body = {
                    pair1: '5669d939e7f5784620f56d1c',
                    pair1_score: 33,
                    pair2: '5669d939e7f5784620f56d1d',
                    pair2_score: 99

                };

                agent.post('/staff/tournaments/edit/' + testTournamentId + '/group/encode/' + testTournament.groups[0]._id)
                    .send(body)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        done();
                    })
            });

            it('should exist in database', function(done) {
                Tournament.findByIdAsync(testTournamentId).then(function(tournament) {
                    testTournament = tournament;
                    tournament.groups[0].gamesHistory.length.should.equal(1);
                    tournament.groups[0].gamesHistory[0].pairs[0].pair.toString().should.equal('5669d939e7f5784620f56d1c');
                    tournament.groups[0].gamesHistory[0].pairs[1].pair.toString().should.equal('5669d939e7f5784620f56d1d');
                    done();
                })

            });



        });


    });
};