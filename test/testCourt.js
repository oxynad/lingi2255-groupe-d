var request = require('supertest');

var chai = require('chai');
chai.should();

var mongoose = require('mongoose');
var Court = mongoose.model('Court');
var datalayer = require('./../app/datalayer');
var CourtPersister = datalayer.persisters.CourtPersister;



module.exports = function(server) {

  describe('Court registration', function() {
        it('should get the court registration page', function(done) {
            request(server)
                .get('/register-owner')
                .expect(200, done);
        });

        it('should create the court', function(done) {

            var body = {
                title: 'Mr',
                first_name: 'Arthur',
                last_name: 'Node',
                street: 'Rue du tennis',
                streetNumber: 55,
                zipCode: 1050,
                city: 'Bruxelles',
                gsm: '049848293',
                email: 'arthur@nodejs.com',
                surface: 'Béton',
                ownerType: 'Club',
                saturdayFree: 'free',
                sundayFree: 'free',
                sameAddress: 'off',
                courtNumber: 1
            };


            request(server)
                .post('/register-owner/add')
                .send(body)
                .expect(302) // Redirect
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.headers.location.should.equal('/register-owner/done');
                    done();
                })
        });


        it('should exist in the database', function(done) {

            Court.findAsync({}).then(function(courts) {
                return Court.findByIdAndRemoveAsync(courts[0]._id);
            }).then(function() {
                done();
            }).catch(function(err) {
                throw err;
            })
        });

  });


};
