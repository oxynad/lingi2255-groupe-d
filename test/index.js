process.env.NODE_ENV = 'test';
var datalayer = require('../app/datalayer');
var app = require("../app");

var importTest = function(name, path) {
    describe(name, function() {

        before(function (done) {
            datalayer.clean().then(function() {
                done();
            });
        });

        afterEach(function (done) {
            done();
        });

        require(path)(app.server);
    });
};

describe("tests", function() {
    before(function() {
        app.boot();
    });
    after(function() {
        app.shutdown();
    });

    importTest("Courts", "./testCourt");
    importTest("Players", "./testPlayer");
    importTest("Groups", "./testGroup");
    importTest("Tournament", "./testTournament");
});