var request = require('supertest');
var _ = require('underscore');
var chai = require('chai');
chai.should();

var persisters = require('./../app/datalayer/persistence');
var Promise = require('bluebird');

module.exports = function(server) {
    var agent = request.agent(server);

    function loginUser() {
        return function(done) {
            agent
                .post('/login')
                .send({
                    username: 'admin',
                    password: 'password'
                })
                .expect(302)
                .expect('Location', '/staff')
                .end(onResponse);

            function onResponse(err, res) {
                if (err) return done(err);
                return done();
            }
        };
    };

    describe("tournament Elimination", function() {

        var testTournamentId;
        var testTournament;

        before(function(done) {
            // Tournament creation
            // Pop the tournamentPersister so we don't call the populate method below
            //var TournamentPersister = persisters.All.pop();
            // call the populate method on all the other persisters
            var promises = persisters.All.map(function(persister) {
                return persister.populate();
            });

            Promise.all(promises).then(function() {
                // Create a tournament
                return Promise.props({
                    pairs: persisters.PairPersister.findAllPairs(),
                    courts: persisters.CourtPersister.findAllCourts()
                }).then(function(result) {
                    return persisters.TournamentPersister.createTournament({
                        status: 'Poule',
                        category: "Pré-minimes",
                        day: "Saturday",
                        type: "homme",
                        ageMin: 1999,
                        ageMax: 2000,
                        groups: [{
                            court: result.courts[0]._id,
                            pairs: [result.pairs[1]._id, result.pairs[2]._id, result.pairs[3]._id],
                            maxWinners: 2,
                            leader: result.pairs[1].players[0]
                        }, {
                            court: result.courts[1]._id,
                            pairs: [result.pairs[4]._id, result.pairs[5]._id],
                            maxWinners: 1,
                            leader: result.pairs[4].players[0]
                        }]
                    });
                }).then(function(tournaments) {
                    testTournamentId = tournaments[0]._id;
                    done();
                })
            });
        });
        it('should login staff', loginUser());

        it('should access the elimination page', function(done) {
           agent.get('/staff/tournaments/edit/' + testTournamentId + '/elimination')
               .expect(200)
               .end(function(err, res) {
                   if (err) {
                       throw err;
                   }
                   done();
               });
        });

        it('should have saved the tournament status', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                tournament.status.should.equals("Elimination");
                done();
            });
        });

        it('should let choose an new nbOfKnockOffPairs', function(done) {
            agent.post('/staff/tournaments/edit/' + testTournamentId + '/elimination')
                .send({ nbOfPairs: 3 })
                .expect(302)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });

        it('should have updated tournament in db', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                //should have updated status
                tournament.status.should.equals("Assignment");
                //should have updated the attribute nbOfKnockOffPairs
                tournament.nbOfKnockOffPairs.should.equals(3);
                //should have added the knockOffGames
                tournament.knockOffTournament.length.should.equals(2);
                _.each(tournament.knockOffTournament, function(game) {
                    if (game.isFinale) {
                        game.fighterA.type.should.equals('ToBeDetermined');
                        game.fighterA.generatorId.should.equals(1);
                        game.fighterB.type.should.equals("Game");
                        game.fighterB.generatorId.should.equals(2);
                        game.id.should.equals('1');
                    } else {
                        game.fighterA.type.should.equals('ToBeDetermined');
                        game.fighterA.generatorId.should.equals(2);
                        game.fighterB.type.should.equals('ToBeDetermined');
                        game.fighterB.generatorId.should.equals(3);
                        game.id.should.equals('2');
                    }
                });
                done();
            });
        });

        var match1Court;
        var match2Court;

        var pairsSaved = {};

        it('should be able to assign pairs & courts to knockoff', function(done) {
            var params = {};

            persisters.TournamentPersister.getWinningPairs(testTournamentId).then(function(groups) {
                var i = 1;
                _.each(groups, function(pairs) {
                    _.each(pairs, function(pair) {
                        params[pair.pairId] = i;
                        pairsSaved[i] = pair.pairId;
                        i++;
                    });
                });
                return persisters.CourtPersister.findAllCourts();
            }).then(function(courts) {
                params.courts = {};
                params.courts['g_1'] = courts[0]._id;
                match1Court = courts[0]._id;
                params.courts['g_2'] = courts[1]._id;
                match2Court = courts[1]._id;
            }).then(function() {
                agent.post('/staff/tournaments/edit/' + testTournamentId + '/elimination')
                    .send(params)
                    .expect(302)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        done();
                    });
            });
        });

        it('should have saved the assignments in database', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                tournament.status.should.equals("KnockOff");

                _.each(tournament.knockOffTournament, function(game) {
                    if (game.isFinale) {
                        game.fighterA.type.should.equals('Pair');
                        game.fighterA.generatorId.should.equals(1);
                        game.fighterA.id.should.equals('' + pairsSaved[1]);

                        game.fighterB.type.should.equals("Game");
                        game.fighterB.generatorId.should.equals(2);

                        game.id.should.equals('1');
                        game.court.toString().should.equals('' + match1Court);
                    } else {
                        game.fighterA.type.should.equals('Pair');
                        game.fighterA.generatorId.should.equals(2);
                        game.fighterA.id.should.equals('' + pairsSaved[2]);

                        game.fighterB.type.should.equals('Pair');
                        game.fighterB.generatorId.should.equals(3);
                        game.fighterB.id.should.equals('' + pairsSaved[3]);

                        game.id.should.equals('2');
                        game.court.toString().should.equals('' + match2Court);
                    }
                });

                done();
            });
        });

        it('should be able to encode results of 1st match', function(done) {
            var params = {
                points: {
                    g_2 : {
                        f_2 : "1,3,4",
                        f_3 : "6,6,6"
                    }
                }
            };

            agent.post('/staff/tournaments/edit/' + testTournamentId + '/elimination')
                .send(params)
                .expect(302)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });

        it('should have saved 1st match in database', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                _.each(tournament.knockOffTournament, function(game) {
                    if (game.id == '2') {
                        game.fighterA.score.length.should.equals(3);
                        game.fighterA.score.should.have.members([1,3,4]);

                        game.fighterB.score.length.should.equals(3);
                        game.fighterB.score.should.have.members([6,6,6]);

                        game.winner.toString().should.equals('' + pairsSaved[3]);
                    }
                });
                done();
            });
        });

        it('should be able to encode result of last game', function(done) {
            var params = {
                points: {
                    g_1 : {
                        f_1 : "6,3,5",
                        f_2 : "2,6,7"
                    }
                }
            };

            agent.post('/staff/tournaments/edit/' + testTournamentId + '/elimination')
                .send(params)
                .expect(302)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    done();
                });
        });

        it('should have saved last match in database', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                _.each(tournament.knockOffTournament, function(game) {
                    if (game.id == '1') {
                        game.fighterA.score.length.should.equals(3);
                        game.fighterA.score.should.have.members([6,3,5]);

                        game.fighterB.score.length.should.equals(3);
                        game.fighterB.score.should.have.members([2,6,7]);

                        game.winner.toString().should.equals('' + pairsSaved[3]);
                    }
                });
                done();
            });
        });

        it('should be done', function(done) {
            persisters.TournamentPersister.getTournament(testTournamentId).then(function(tournament) {
                tournament.status.should.equals('Terminé');
                done();
            });
        });
    });
};