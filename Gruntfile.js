module.exports = function(grunt) {

  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-services');
  grunt.loadNpmTasks('grunt-jsdoc');

  var target = grunt.option('target') || 'dev';
  var nodeArgs = [];
  if (grunt.option("fresh-db"))
    nodeArgs.push("fresh-db");

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    /*
      Clean build directory task
     */
    clean: {
      build: ['build'],
      dev: {
        src: ['build/bundle.css', 'build/bundle.css.map']
      },
      prod: ['dist']
    },

    /*
      Sass compiling task
     */
    sass: {
      dev: {
        options: {
          style: 'expanded'
        },
        files: {
          'build/bundle.css': 'client/css/app.scss'
        }
      },
      prod: {
        options: {
          style: 'compressed'
        },
        files: {
          'build/bundle.css': 'client/css/app.scss'
        }
      }
    },

    /*
      Wire bower deps into the main html file.
     */
    wiredep: {
      task: {
        src: [
          'views/layouts/main.handlebars',
        ],
        options: {}
      }
    },

    /*
      Browserify task for the client's js files.
     */
    browserify: {
      dev: { // During dev, watch is on.
        options: {
          transform: ['hbsfy'],
          watch: true, // Watch for changes and do incremental change.
          keepAlive: true // Doesn't kill process if browserify fails.
        },
        src: 'client/src/main.js',
        dest: 'public/js/bundle.js'
      },
      prod: {
        options: {
          transform: ['hbsfy']
        },
        src: 'client/src/main.js',
        dest: 'public/js/bundle.js'
      }
    },

    /*
      Copy files from build to public
     */
    copy: {
      dev: {
        files: [{
          src: 'build/bundle.css',
          dest: 'public/css/bundle.css'
        }, {
          src: 'build/bundle.css.map',
          dest: 'public/css/bundle.css.map'
        }, {
          expand: true,
          cwd: 'client/img/',
          src: ['**'],
          dest: 'public/img/'
        }]
      },

    },

    /*
      CSS minification.
     */
    cssmin: {
      minify: {
        src: ['build/bundle.css'],
        dest: 'public/css/bundle.css'
      }
    },

    /*
      Javascript minification.
     */
    uglify: {
      compile: {
        options: {
          compress: true,
          verbose: true,
          preserveComments: false
        },
        files: [{
          src: 'public/js/bundle.js',
          dest: 'public/js/bundle.js'
        }]
      }
    },

    /*
      Watch files in the project folder.
     */
    watch: {
      options: {
        livereload: true,
        spawn: false,
        interrupt: true
      },
      app: {
        files: ['public/js/bundle.js', 'views/**/*.handlebars']
      },
      // This could be removed... because of browserify's watch option set to true. Keeping for now.
      // frontend: {
      //  files: [
      //    'client/src/**/*.js',
      //    'client/src/**/*.hbs',
      //    'views/**/*.handlebars'
      //  ],
      //  tasks: ['clean:dev', 'browserify:app', 'copy:dev']
      // },
      sass: {
        files: 'client/css/**/*.scss',
        tasks: ['sass:dev', 'copy:dev']
      }

    },


    /*
      Node server runner and watcher
     */
    nodemon: {
      dev: {
        options: {
          file: 'app.js',
          nodeArgs: ["--debug"],
          watch: ['app/'],
          args: nodeArgs,
        }
      }
    },

    /*
      Run tasks concurrently
     */
    concurrent: {
      dev: {
        tasks: ['nodemon:dev', 'watch', 'browserify:dev'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    jshint: {
      all: ['Gruntfile.js', 'client/src/**/*.js'],
      dev: ['client/src/**/*.js']
    },

    jsdoc: {
      dist: {
        src: ['app.js', 'app/*'],
        options: {
          destination: 'docs',
          recurse: true
        }
      }
    },

    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
        },
        src: ['test/**/*.js']
      }
    }
  });

  grunt.registerTask('build:dev', ['clean:dev', 'sass:dev', 'copy:dev']);
  grunt.registerTask('build:prod', ['clean:dev', 'sass:prod', 'cssmin', 'browserify:prod', 'uglify', 'copy:dev']);

  grunt.registerTask('server', ['build:' + target, 'startMongo', 'concurrent:dev']);

  grunt.registerTask('test', ['startMongo', 'mochaTest', 'stopMongo']);
};