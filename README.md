# README #

## Installation du répertoire ##

### Pour Windows
Prérequis: Git doit être installé.

1. Clonez le repository. Dans le prompt de Git, entrez la commande "git clone git@bitbucket.org:oxynad/lingi2255-groupe-d.git"
2. Téléchargez et installez NodeJS (https://nodejs.org/en/)
3. Dans un prompt, où vous êtes localisé dans le repository que vous avez copié, entrez "npm install -g bower"
4. Tapez "bower install"
5. Téléchargez et installez MongoDB (https://www.mongodb.org/downloads#production) et créez le dossier data\db dans la racine du disque dans lequel le projet sera copié
6. Ajoutez à votre variable système PATH le chemin vers le dossier MongoDB/Server/3.0/bin.
7. Entrez la commande "npm install -g grunt-cli" pour installer Grunt
8. Entrez la commande "npm install" pour installer les différents nodes_modules
9. Téléchargez et installez Ruby. Lors de l'installation, sélectionnez "Add Ruby executables to your PATH"
10. Entrez la commande "gem install sass" pour installer sass
11. Entrez à nouveau la commande "bower install"
12. Entrez "grunt server -fresh-db" pour lancer le server. Si erreur avec MongoDB, redémarrez votre machine.

## Utilisation de l'application ##

### USAGE ###
* grunt server [--fresh-db] [--target=[dev,prod]]
>   Demarre la base de donnée ainsi que le serveur sur le port 3000.
>
>   Si l'argument fresh-db est renseigné, la db sera nettoyée et les élements de base seront ajoutés (populate), sinon elle est inchangée
>
>   Si l'argument target est renseigné, le serveur est lancé sur cette configuration spécifique, sinon la configuration dev est utilisée


### Comment lancer le serveur? ###

1. Clonez le répo sur votre ordinateur (git clone <repo> <directory>) où repo = git@bitbucket.org:oxynad/lingi2255-groupe-d.git

2. Installez NodeJS & mongoDB
3. Allez dans le dossier du projet en console et tapez la commande "npm install" (requiert sudo)
4. Allez dans le dossier du projet en console et tapez la comande "bower install"
5. Allez dans le dossier du projet et tapez la commande "grunt server" dans la console. Cela lancera le serveur sur le port 3000
6. Ouvrez votre navigateur et tapez l'url http://localhost:3000/


### Paypal fake accounts

1. Fake Buyer Account : 
	1. Email ID : arthur.englebert-buyer@skynet.be
	2. Passwrd : azerty123
	3. Bank account : 
		* ID : BE88163131618384
		* Credit card : VISA 4020028851776251 exp.11/2020
		* Actual balance : 9755€
2. Fake Merchant Account :
	1. Email ID : arthur.englebert@student.uclouvain.be
	2. Passwrd : azerty123
	3. Bank account :
	    * Credit card : VISA 4020020944000013 exp.11/2020
		* Actual balance : 1.000.000€
3. Fake App :
	1. Merchant account linked : arthur.englebert@student.uclouvain.be
	2. Sandbox endpoint : api.sandbox.paypal.com
	3. Credentials : 
		* Client ID : Aar1KfCTiKfOi1Qm5zE7_8cXnDMvbPpF7UP1SEm3XdafXAWMgnIqeN2GCPpa8atxtpqA9_SAvP-3hgxk
		* Secret : EF8GuZ8hwPk6Wr32cuYJgOqx7iOISJaykUXxrLGzb0a90oGqo47Z4-OQiUowl_k3hb-9x0mmOGw8660-