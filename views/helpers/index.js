var _ = require('underscore');
var moment = require('moment');
moment.locale('fr');

/**
 * The Handlebars helpers file.
 * It is used to add text/html formatting helpers to the template engine Handlebars.
 * @module views/helpers/index.js
 * @type {Object}
 */
module.exports = {

    commentFormat: function(commentDate) {
        return moment(commentDate).format("dddd DD MMMM YYYY à hh:mm:ss");
    },

    nextYear: function() {
        return (new Date().getFullYear() + 1);
    },

    dateFormat: function(date) {
        return moment(date).format("DD MMMM YYYY");
    },

    groupIndex: function(index) {
        return ++index;
    },

    isTournoiDesFamillesManage: function(tournament) {
        if (tournament.category == 'Familles') {
            return '';
        } else {
            return '<p>Mixité : ' + tournament.type + '</p> Age: ' + tournament.ageMin + ' - ' + tournament.ageMax;
        }
    },

    isTournoiDesFamillesEdit: function(tournament) {
        if (tournament.category == 'Familles') {
            return '';
        } else {
            return tournament.type
        }

    },

    allmatches: function(tournamentId, groupId, games) {
        var str = "";
        var winners, loosers;
        _.each(games, function(game) {

            if (game.pairs[0].score > game.pairs[1].score) {
                winners = game.pairs[0];
                loosers = game.pairs[1];
            } else {
                winners = game.pairs[1];
                loosers = game.pairs[0];
            }

            str += '<div class="match"><span class="winners"> ' + winners.pair.players[0].firstName + ' ' + winners.pair.players[0].lastName + '/' + winners.pair.players[1].firstName + ' ' + winners.pair.players[1].lastName + '</span>';
            str += " <span class='vs'>vs</span> " + '<span class="loosers"> ' + loosers.pair.players[0].firstName + ' ' + loosers.pair.players[0].lastName + '/' + loosers.pair.players[1].firstName + ' ' + loosers.pair.players[1].lastName + '</span>';
            str += " <span class='score'><span class='score1'>" + winners.score + "</span>/<span class='score2'>" + loosers.score + '</span></span>';
            str += "<form action='/staff/tournaments/edit/" + tournamentId + "/group/encode/" + groupId + "/delete-match/" + game._id + "' method='post'>";
            str += "<button type='submit' class='delete-match'>Delete</button>";
            str += "</form>";
            str += "</div>";
        });
        return str;
    },

    translateDay: function(day) {
        return (day === 'Saturday') ? 'Samedi' : 'Dimanche';
    },

    hasSeveralGroups: function(t) {
        return t.groups.length > 1;
    },

    ifCond: function(v1, operator, v2, options) {
        switch (operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
            case '<':
                return (v1 < v2) ? options.fn(this) : options.inverse(this);
            case '<=':
                return (v1 <= v2) ? options.fn(this) : options.inverse(this);
            case '>':
                return (v1 > v2) ? options.fn(this) : options.inverse(this);
            case '>=':
                return (v1 >= v2) ? options.fn(this) : options.inverse(this);
            case '&&':
                return (v1 && v2) ? options.fn(this) : options.inverse(this);
            case '||':
                return (v1 || v2) ? options.fn(this) : options.inverse(this);
            default:
                return options.inverse(this);
        }
    },

    concatAddress: function (street, streetNumber, zipCode, city) {
        if (street === undefined ||
            streetNumber === undefined ||
            zipCode === undefined ||
            city === undefined) {

            return "Incomplet";
        } else {
            return street.concat(streetNumber, ', ', zipCode, ', ', city);
        }
    }




};