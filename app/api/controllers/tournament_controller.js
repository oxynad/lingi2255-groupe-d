var _ = require('underscore');
var datalayer = require('./../../datalayer');
var CourtPersister = datalayer.persisters.CourtPersister;
var PlayerPersister = datalayer.persisters.PlayerPersister;
var GroupPersister = datalayer.persisters.GroupPersister;
var StaffPersister = datalayer.persisters.StaffPersister;
var BracketBuilder = require("../../helpers/HTMLBuilder");
var TournamentPersister = datalayer.persisters.TournamentPersister;
var PairPersister = datalayer.persisters.PairPersister;
var utilsController = require('./utils_controller');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var PDFDocument = require('pdfkit');
var lorem = require('lorem-ipsum');
var logger = require('winston');

/**
 *     The tournament's section controller.
 *     @module app/api/controllers/tournament_controller
 *     @type {Object}
 */
 module.exports = {

    /**
     *     GET - Show the tournaments page.
     *     @param  {Object} req - Express's request object. Req contains a possible message status.
     *     @param  {Object} res - Express's response object. Res render the view to manage tournaments
     */
     manageTournaments: function(req, res) {

        var msgStatus = req.msgStatus;
        var msgError = req.msgError;

        TournamentPersister.getAllTournaments().then(function(tournaments) {
            var tournamentsGroupedBy = _.groupBy(tournaments, function(value) {
                return value.day;
            });

            // logger.debug(tournamentsGroupedBy);
            res.render('manage_tournament', {
                tournaments: tournamentsGroupedBy,
                msgStatus: msgStatus,
                msgError: msgError
            });
        });
    },

    /**
     *     GET - Show the page to add a new tournament
     *     @param  {Object} req - Express's request object. Not used
     *     @param  {Object} res - Express's response object. Res renders the view to add a niew tournament.
     */
     newTournament: function(req, res) {
        res.render('new_tournament');
    },


    /**
     *     GET - Show the page to add a new group to a tournament
     *     @param  {Object} req - Express's request object. Req contains the tournament id.
     *     @param  {Object} res - Express's response object. Res render the view to add a new group.
     */
     newGroup: function(req, res) {
        res.render('create_group', {
            tournamentId: req.params.tournamentId,
        });
    },

    /**
     *     GET - Change the tournament status to Poule
     *     @param  {Object} req - Express's request object. Req contains the tournament id and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the edit page of a tournament
     */
     setPoule: function(req, res) {

        TournamentPersister.setStatus(req.params.tournamentId, 'Poule').then(function() {
            req.session.msgStatus = "Le Tournoi est à présent en phase Poule";
            res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
        });
    },

    /**
     * POST - Send mail to a group with the infos about the tournament
     * @param  {Object} req Express's request object. Req contains the tournament id and the group id.
     * @param  {Object} res Express's request object. Res redirects to the edit page of a tournament 
     */
     sendMailToGroup: function(req, res) {
        var text = "Bonjour,\n Vos matchs auront lieu sur le terrain à l'adresse suivante :\n";
        var textHtml = "<p>Bonjour, <br/> Vos matchs auront lieu sur le terrain à l'adresse suivante :<br/>";
        var textMoreInfo = "Bonjour,\n nous vous invitons à vous rendre au QG du tournoi afin de finaliser votre inscription";
        var textMoreInfoHtml = "<p>Bonjour,<br/> nous vous invitons à vous rendre au QG du tournoi afin de finaliser votre inscription</p>";
        var textTL = "Bonjour,\n vous avez été désigné comme chef d'équipe, pourriez-vous vous rendre au QG pour plus d'informations";
        var textTLHtml = "<p>Bonjour, <br/> vous avez été désigné comme chef d'équipe, pourriez-vous vous rendre au QG pour plus d'informations</p>"
        Promise.props({
            pairs: TournamentPersister.findPairsInGroup(req.params.tournamentId, req.params.groupId),
            group: TournamentPersister.getGroupeById(req.params.tournamentId, req.params.groupId),
            responsibleId: TournamentPersister.getStaffMembersResponsibleForTournament(req.params.tournamentId)
        }).then(function(result) {
            var mails = [];
            var noPayment = [];
            _.each(result.pairs, function(pair) {
                for (var pos = 0; pos < pair["players"].length; pos++) {
                    if (pair.payment == null) {
                        noPayment.push(pair["players"][pos]["email"]);
                    } else {
                        mails.push(pair["players"][pos]["email"]);
                    }
                }
            });

            if (mails.length > 0) {
                Promise.props({
                    court: CourtPersister.findCourtById(result.group.court),
                    responsible: StaffPersister.findStaff(result.responsibleId)
                }).then(function(result) {
                    console.log("responsible : " + result.responsible);
                    text += result.court.courtAddress;
                    textHtml += result.court.courtAddress + "</p>";
                    if (result.responsible != null) {
                        text += "\n Pour plus d'informations contactez le responsable :\n";
                        textHtml += "<br/><p>Pour plus d'informations contactez le responsable :<br/>";
                        if (result.responsible["name"] != null) {
                            text += result.responsible["name"] + "\n";
                            textHtml += result.responsible["name"] + "<br/>";
                        }
                        if (result.responsible["mail"] != null) {
                            text += result.responsible["mail"] + "\n";
                            textHtml += result.responsible["mail"] + "<br/>";
                        }
                        if (result.responsible["phoneNumber"] != null) {
                            text += result.responsible["phoneNumber"];
                            textHtml += result.responsible["phoneNumber"] + "</p>";
                        }
                    }

                    utilsController.sendMail(mails, text, textHtml, 'Information tournoi');
                });
}
if (noPayment.length > 0) {
    utilsController.sendMail(noPayment, textMoreInfo, textMoreInfoHtml, 'Information paiement tournoi');
}
PlayerPersister.findPlayer(result.group.leader).then(function(tleader) {
    var tleaderMail = [];
    tleaderMail.push(tleader.email);
    utilsController.sendMail(tleaderMail, textTL, textTLHtml, 'Information tournoi');
})
res.send({
    redirect: '/staff/tournaments/edit/' + req.params.tournamentId
});
});
},

    /**
     * Delete a group from the tournament
     * @param  {Object} req - Express's request object. Req contains the tournament id, the group id and a possible message status or error.
     * @param  {Object} res - Express's response object. Res redirects to the edit page of the tournament.
     */
     delGroup: function(req, res) {
        var tournamentId = req.params.tournamentId;
        var groupId = req.params.groupId;

        TournamentPersister.removeGroup(tournamentId, groupId).then(function() {
            req.session.msgStatus = "Groupe correctement supprimé";
            res.redirect('/staff/tournaments/edit/' + tournamentId);
        }).catch(function(err) {
            req.session.msgError = "Impossible de supprimer le groupe, réessayez plus tard";
            res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
        });
    },

    /**
     *     GET - Change the tournament status to Conception
     *     @param  {Object} req - Express's request object. Req contains the tournament id and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the edit page of the tournament.
     */
     setConception: function(req, res) {

        TournamentPersister.setStatus(req.params.tournamentId, 'Conception').then(function() {
            req.session.msgStatus = "Le Tournoi est à présent en phase Conception";
            res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
        });

    },

    /**
     *     POST - Create a new tournament
     *     @param  {Object} req - Express's request object. Req contains the tournament's attributes to add
     *     @param  {Object} res - Express's response object. Res redirects to the tournament section.
     */
     createTournament: function(req, res) {
        var params = {};
        params.day = req.body.day;
        params.type = req.body.type;
        params.category = req.body.category;
        params.ageMin = req.body.ageMin;
        if (req.body.ageMax) {
            params.ageMax = req.body.ageMax;
        }
        TournamentPersister.createTournament(params).then(function() {
            res.redirect('/staff/tournaments');
        });
    },

    /**
     *     GET - Create a the family tournament
     *     @param  {Object} req - Express's request object
     *     @param  {Object} res - Express's response object
     */
     createFamilyTournament: function(req, res) {
        TournamentPersister.familyTournamentExists().then(function(tournament) {

            if (tournament.length == 0) {
                TournamentPersister.createFamilyTournament().then(function() {
                    res.redirect('/staff/tournaments');
                });
            } else {
                req.session.msgError = 'Le tournoi des familles existe deja';
                res.redirect('/staff/tournaments');
            }
        })

    },

    /**
     *     POST - Edit a tournament
     *     @param  {Object} req - Express's request object. Req contains tournament id and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res renders the view to edit a tournament.
     */
     editTournament: function(req, res) {

        var msgStatus = req.msgStatus;
        var msgError = req.msgError;

        TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
            res.render('edit_tournament', {
                tournament: tournament,
                msgStatus: msgStatus,
                msgError: msgError
            });
        });

    },

    /**
     *     GET - Delete a tournament
     *     @param  {Object} req - Express's request object. Req contains the tournament id and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the tournament section.
     */
     delTournament: function(req, res) {
        TournamentPersister.removeTournamentById(req.params.tournamentId);
        req.session.msgStatus = "Tournoi supprimé avec succès";
        res.redirect('/staff/tournaments');
    },


    /**
     *     POST - create a new group
     *     @param  {Object} req - Express's request object. Req contains the group params, the tournament id and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the edit page of the tournament.
     */
     createGroup: function(req, res) {
        var params = req.params.groupParams;
        var tournamentId = req.params.tournamentId;
        var obj = JSON.parse(params);
        var pairsId = [];


        Promise.props({
            tLeader: PlayerPersister.findPlayerByName(obj["tLeader"])
        }).then(function(result) {
            //retrieve pairsId
            _.each(obj["pairs"], function(pair) {
                logger.debug(pair);
            });
            //retrieve team leader
            logger.debug(result.tLeader._id);
            //retrieve wining pairs
            logger.debug(obj["nbPairs"]);
            //retrieve courtId
            logger.debug(obj["courtId"]);

            var toPersist = JSON.parse(JSON.stringify({
                court: obj["courtId"],
                leader: result.tLeader._id,
                pairs: obj["pairs"],
                maxWinners: obj["nbPairs"]
            }));

            logger.debug(toPersist);

            return TournamentPersister.createGroup(toPersist);
        }).then(function(toPersist) {
            return TournamentPersister.addGroup(req.params.tournamentId, toPersist[0]);
        }).then(function() {
            req.session.msgStatus = "Groupe ajouté avec succès";
            res.send({
                redirect: '/staff/tournaments/edit/' + req.params.tournamentId
            });
        });
    },

    /**
     *     GET - show the page to edit a group
     *     @param  {Object} req - Express's request object. Req contains the tournament id.
     *     @param  {Object} res - Express's response object. Res render the view to manage group.
     */

     editGroup: function(req, res) {
        res.render('manage_group', {
            tournamentId: req.params.tournamentId,
        });
    },


    /**
     * GET - Show the goup's infos
     * @param  {Object} req - Express's request object. Req contains the tournament id and the group id.
     * @param  {Object} res - Express's response object. Res sends the group's infos.
     */
     getGroupeInfo: function(req, res) {
        Promise.props({
            group: TournamentPersister.getGroupeById(req.params.tournamentId, req.params.groupId),
            pairs: TournamentPersister.findPairsInGroup(req.params.tournamentId, req.params.groupId)
        }).then(function(result) {
            var formattedPairs = _.map(result.pairs, function(pair) {
                return {
                    name: pair.players[0].firstName + " " + pair.players[0].lastName + " - " + pair.players[1].firstName + " " + pair.players[1].lastName,
                    id: pair._id,
                    wish: pair.comment
                }
            });
            formattedPairs.sort(function(a, b) {
                return b["wish"].length - a["wish"].length;
            });
            Promise.props({
                tLeader: PlayerPersister.findPlayer(result.group["leader"]),
                court: CourtPersister.findCourtById(result.group["court"])
            }).then(function(infos) {
                var teamLeader = infos.tLeader.firstName + " " + infos.tLeader.lastName;
                var courtInfos = {
                    address: infos.court.courtAddress,
                    id: infos.court._id,
                    wish: infos.court.specialInstructions
                };
                var groupInfos = {
                    pairs: formattedPairs,
                    tLeader: teamLeader,
                    court: courtInfos,
                    maxWin: result.group.maxWinners
                };
                res.send(groupInfos);
            });
        });
},

    /**
     * POST - Save changes on a group defined by id
     * @param  {Object} req Express's request object. Req contains the tournament id, the group's params to update and a possible message status. 
     * @param  {Object} res Express's request object. Res redirects to the edit page of the tournament.
     */
     updateGroup: function(req, res) {
        var params = req.params.groupParams;
        var tournamentId = req.params.tournamentId;
        var obj = JSON.parse(params);
        var pairsId = [];

        Promise.props({
            tLeader: PlayerPersister.findPlayerByName(obj["tLeader"])
        }).then(function(result) {
            //retrieve pairsId
            _.each(obj["pairs"], function(pair) {
                logger.debug(pair);
            });
            //retrieve team leader
            logger.debug(result.tLeader._id);
            //retrieve wining pairs
            logger.debug(obj["nbPairs"]);
            //retrieve courtId
            logger.debug(obj["courtId"]);

            var toPersist = JSON.parse(JSON.stringify({
                court: obj["courtId"],
                leader: result.tLeader._id,
                pairs: obj["pairs"],
                maxWinners: obj["nbPairs"]
            }));

            logger.debug(toPersist);

            TournamentPersister.updateGroup(tournamentId, obj["groupId"], toPersist).then(function() {
                req.session.msgStatus = "Groupe modifié avec succès";
                res.send({
                    redirect: '/staff/tournaments/edit/' + req.params.tournamentId
                });
            });
        });
    },

    /**
     * GET - Find the available pairs
     * @param  {Object} req Express's request object. Req contains the tournament id.
     * @param  {Object} res Express's request object. Res sends the formatted pairs.
     */
     getPairsAvailable: function(req, res) {
        TournamentPersister.findPairsNotInGroup(req.params.tournamentId).then(function(pairs) {
            // logger.debug(pairs);
            var formattedPairs = _.map(pairs, function(pair) {
                return {
                    name: pair.players[0].firstName + " " + pair.players[0].lastName + " - " + pair.players[1].firstName + " " + pair.players[1].lastName,
                    id: pair._id,
                    wish: pair.comment
                }
            });
            //logger.debug(formattedPairs);
            formattedPairs.sort(function(a, b) {
                return b["wish"].length - a["wish"].length;
            });
            res.send(formattedPairs);
        }).catch(function(err) {
            logger.debug(err);
        });

    },

    /**
     * GET - Find the available courts
     * @param  {Object} req Express's request object. Req contains the tournament id.
     * @param  {Object} res Express's request object. Res sends the formatted courts.
     */
     getCourtsAvailable: function(req, res) {
        TournamentPersister.findCourtNotAlreadyUsed(req.params.tournamentId).then(function(courts) {
            var formattedCourts = _.map(courts, function(court) {
                return {
                    address: court.courtAddress,
                    id: court._id,
                    wish: court.specialInstructions
                }
            });
            // logger.debug(formattedCourts);
            formattedCourts.sort(function(a, b) {
                return b["wish"].length - a["wish"].length;
            });
            res.send(formattedCourts);
        }).catch(function(err) {
            logger.debug(err);
        });

    },

    /**
     *     GET - show the page with the results of matches for a group
     *     @param  {Object} req - Express's request object. Req contains the tournament id and the group id.
     *     @param  {Object} res - Express's response object. Res render the view with the results.
     */
     getEncodeResults: function(req, res) {

        var msgError = req.msgError;

        Promise.props({
            matches: TournamentPersister.findAllMatches(req.params.tournamentId, req.params.groupId),
            pairs: TournamentPersister.findPairsInGroup(req.params.tournamentId, req.params.groupId)
        }).then(function(results) {

            // logger.debug(pairs);
            var formattedPairs = _.map(results.pairs, function(pair) {
                return {
                    name: pair.players[0].firstName + " " + pair.players[0].lastName + " - " + pair.players[1].firstName + " " + pair.players[1].lastName,
                    id: pair._id
                }
            });
            res.render('encode_results', {
                pairs: formattedPairs,
                tournamentId: req.params.tournamentId,
                groupId: req.params.groupId,
                matches: results.matches,
                msgError: msgError
            });
        });


    },

    /**
     *     POST - encode a match for a group
     *     @param  {Object} req - Express's request object. Req contains the tournament id, the group id, the match's attributes to encode and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the edit page of the tournament.
     */
     doEncodeResults: function(req, res) {

        if (req.body.pair1_score < 0 || req.body.pair2_score < 0) {
            req.session.msgError = "Vous avez inséré des données éronnées";
            res.redirect('/staff/tournaments/edit/' + req.params.tournamentId + '/group/encode/' + req.params.groupId);
        } else {

            TournamentPersister.addGameToGroup(req.params.tournamentId, req.params.groupId, req.body.pair1, parseInt(req.body.pair1_score), req.body.pair2, parseInt(req.body.pair2_score)).then(function() {
                req.session.msgStatus = "Résultat encodé avec succès";
                res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);

            }).catch(function(err) {
                logger.debug(err);
            });

        }


    },

    /**
     *     GET - delete a match of a group
     *     @param  {Object} req - Express's request object. Req contains the tournament id, the group id, the match id and a possible message status or error. 
     *     @param  {Object} res - Express's response object. Res redirects to the edit page of the tournament.
     */
     doDeleteMatch: function(req, res) {
        TournamentPersister.deleteMatch(req.params.tournamentId, req.params.groupId, req.params.matchId).then(function() {
            req.session.msgStatus = "Résultat supprimé avec succès";
            res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);

        }).catch(function(err) {
            logger.debug(err);
        });
    },


    /**
     * GET - Show the management page of elimination 
     * @param  {Object} req - Express's request object. Req contains the tournament id and a possible message status or error.
     * @param  {Object} res - Express's response object. Res renders the view to encode elimination bracket or remains on the page
     */
     show_elimination: function(req, res) {
        CourtPersister.findAllCourts().then(function(courts) {
            return TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
                if (tournament.status == 'Poule' || tournament.status == 'Assignment' || tournament.status == 'Elimination') {
                    var params;
                    return TournamentPersister.getWinningPairs(req.params.tournamentId).then(function(winningPairs) {
                        var promises = [];
                        _.each(winningPairs, function(groupWinningPairs) {
                            _.each(groupWinningPairs, function(winningPair) {
                                promises.push(PairPersister.findPair(winningPair.pairId).then(function(pair) {
                                    return pair.populate('players').execPopulate();
                                }).then(function(populatedPair) {
                                    winningPair.pair = populatedPair;
                                    _.map(tournament.knockOffTournament, function(element) {
                                        if (element.fighterA.type == 'Pair' && populatedPair._id.equals(element.fighterA.id)) {
                                            winningPair.generatorId = element.fighterA.generatorId;
                                        } else if (element.fighterB.type == 'Pair' && populatedPair._id.equals(element.fighterB.id)) {
                                            winningPair.generatorId = element.fighterB.generatorId;
                                        }
                                    });
                                }));
                            });
});

params = {
    groupPairs: winningPairs,
    pairs: _.flatten(winningPairs),
    tournamentId: req.params.tournamentId,

};

if (tournament.status == 'Assignment')
    params.isAssignemnt = true;
else if (tournament.status == 'Poule' || tournament.status == 'Elimination')
    params.isElim = true;

return Promise.all(promises).then(function() {
    if (tournament.status == 'Poule') {
        return TournamentPersister.setStatus(req.params.tournamentId, 'Elimination');
    }
}).then(function() {
    if (tournament.status == 'Assignment') {
        return BracketBuilder.promiseBuildBracket(tournament.nbOfKnockOffPairs, courts, tournament.knockOffTournament, false);
    }
}).then(function(bracket) {
    params.bracket = bracket;
    if (req.msgError)
        params.msgError = req.msgError;

    params.nbOfPairs = _.size(params.pairs);

    res.render('encode_elimination_bracket', params);
})
});
} else if (tournament.status == 'KnockOff' || tournament.status == 'Terminé') {
    BracketBuilder.promiseBuildBracket(tournament.nbOfKnockOffPairs, courts, tournament.knockOffTournament, true).then(function(bracket) {
        var params = {
            tournamentId: req.params.tournamentId,
            bracket: bracket

        };
        if (req.msgError)
            params.msgError = req.msgError;

        params.isEnded = tournament.status == "Terminé";
                        //logger.debug(params.isEnded + " " + tournament.status);

                        res.render('show_elimination_bracket', params);
                    });
}
});
});
},


    /**
     * POST - Encode the elimination phase and save it
     * @param  {Object} req - Express's request object. Req contains the tournament id, the elimination's attributes encoded and a possible message status or error. 
     * @param  {Object} res - Express's response object. Res redirects to the elimination page.
     */
     encode_elimination: function(req, res) {
        req.session.msgError = undefined;
        TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
            if (tournament.status == 'Elimination') {
                tournament.nbOfKnockOffPairs = req.body.nbOfPairs;
                tournament.status = 'Assignment';
                return tournament.saveAsync().then(function() {
                    return TournamentPersister.generateKnockoffGames(tournament._id, tournament.nbOfKnockOffPairs);
                });
            } else if (tournament.status == 'Assignment') {
                //var uniq = _.compact(_.uniq(_.map(req.body, function(value, key){return value;})));

                //if (!_.every(uniq, function(elem) {logger.debug(elem);return elem > 0 && elem <= _.size(uniq);})) {
                //  req.session.msgError = "certains numeros encodés ne sont pas référencés";
                //  return;
                //}

                var done = true;

                //logger.debug(req.body);

                _.map(tournament.knockOffTournament, function(element, index) {
                    var match = tournament.knockOffTournament[index];
                    if (match.fighterA.type == 'Pair') {
                        match.fighterA.type = 'ToBeDetermined';
                        match.fighterA.id = match.fighterA.generatorId;
                    }
                    if (match.fighterB.type == 'Pair') {
                        match.fighterB.type = 'ToBeDetermined';
                        match.fighterB.id = match.fighterB.generatorId;
                    }

                    match.court = null;

                    _.map(req.body, function(value, key) {
                        if (key == 'courts') {
                            _.map(value, function(courtId, matchId) {
                                if (matchId.slice(2) == match.id) {
                                    tournament.knockOffTournament[index].court = courtId;
                                }
                            });
                        } else if (match.fighterA.type == 'ToBeDetermined' && match.fighterA.generatorId == value) {
                            tournament.knockOffTournament[index].fighterA.id = key;
                            tournament.knockOffTournament[index].fighterA.type = 'Pair';
                        } else if (match.fighterB.type == 'ToBeDetermined' && match.fighterB.generatorId == value) {
                            tournament.knockOffTournament[index].fighterB.id = key;
                            tournament.knockOffTournament[index].fighterB.type = 'Pair';
                        }
                    });

if (match.fighterA.type == 'ToBeDetermined' || match.fighterB.type == 'ToBeDetermined' || match.court == null)
    done = false;
});

if (done)
    tournament.status = 'KnockOff';

return tournament.saveAsync();
} else if (tournament.status == 'KnockOff') {
    _.each(req.body.points, function(fighters, matchId) {
        if (req.session.msgError)
            return;

        if (_.size(fighters) == 0)
            return;

        if (_.size(fighters) != 2) {
            req.session.msgError = "Un match n'avait pas les points des deux opposants remplis";
            return;
        }

        var match = _.find(tournament.knockOffTournament, function(history) {
            return history.id == matchId.slice(2);
        });
        if (!match) {
            req.session.msgError = "Un match était introuvable";
            return;
        }
                    //logger.debug(match);
                    var matchFighterIds = [match.fighterA.generatorId, match.fighterB.generatorId];
                    var points = [];
                    _.map(fighters, function(pointsOfFighterForMatch, fighterId) {
                        //logger.debug("fighterId : " + fighterId.slice(2) + " points : " + pointsOfFighterForMatch);
                        if (!_.include(matchFighterIds, parseInt(fighterId.slice(2)))) {
                            req.session.msgError = "Un des opposant était introuvable pour le match renseigné";
                            return;
                        }

                        if (pointsOfFighterForMatch.length == 0)
                            return;

                        points.push({
                            value: pointsOfFighterForMatch.split(","),
                            fighterId: fighterId.slice(2)
                        });
                    });

                    if (points.length == 0)
                        return;

                    if (req.session.msgError)
                        return;

                    var nbOfSets = points[0].value.length;

                    if (nbOfSets != points[1].value.length) {
                        req.session.msgError = "Le nombre de set joués diffère entre deux joueurs";
                        return;
                    }

                    _.each(points, function(point) {
                        for (var i = 0; i < nbOfSets; i++) {
                            if (point.value[i] < 0 || point.value[i] > 7) {
                                req.session.msgError = "Certains points dépassent la limite possible (0 à 7)";
                                return;
                            }
                        }
                    });

                    if (req.session.msgError)
                        return;

                    _.each(points, function(point) {
                        if (match.fighterA.generatorId == point.fighterId) {
                            match.fighterA.score = [];
                            _.each(point.value, function(elem) {
                                match.fighterA.score.push(parseInt(elem));
                            });
                        } else if (match.fighterB.generatorId == point.fighterId) {
                            match.fighterB.score = [];
                            _.each(point.value, function(elem) {
                                match.fighterB.score.push(parseInt(elem));
                            });
                        }
                    });


                    var winPoints = 0;
                    for (var i = 0; i < nbOfSets; i++) {
                        //logger.debug(points[0].value[i] + " " + points[1].value[i] + " " + winPoints);
                        if (points[0].value[i] > points[1].value[i])
                            winPoints++;
                        else if (points[0].value[i] < points[1].value[i])
                            winPoints--;
                        else {
                            req.session.msgError = "Un set ne peut pas renvoyer d'égalité";
                            return;
                        }
                    }

                    var winnerId;
                    if (winPoints > 0) {
                        winnerId = points[0].fighterId;
                    } else if (winPoints < 0) {
                        winnerId = points[1].fighterId;
                    } else {
                        req.session.msgError = "Les points entrés impliquent une égalité";
                        return;
                    }

                    //logger.debug(winnerId + " " + match.fighterA.generatorId + " " + match.fighterB.generatorId);

                    if (match.fighterA.generatorId == winnerId) {
                        if (match.fighterA.type == 'Game') {
                            match.winner = _.find(tournament.knockOffTournament, function(history) {
                                return history.id == match.fighterA.generatorId;
                            }).winner;
                        } else {
                            match.winner = match.fighterA.id;
                        }
                    } else if (match.fighterB.generatorId == winnerId) {
                        if (match.fighterB.type == 'Game') {
                            match.winner = _.find(tournament.knockOffTournament, function(history) {
                                return history.id == match.fighterB.generatorId;
                            }).winner;
                        } else {
                            match.winner = match.fighterB.id;
                        }
                    }

                    if (match.isFinale)
                        tournament.status = 'Terminé';
                });

return tournament.saveAsync();
}
}).then(function() {
    res.redirect("/staff/tournaments/edit/" + req.params.tournamentId + "/elimination");
});
},

    /**
     * GET - Get valid opponents for a pair
     * @param  {Object} req - Express's request object. Req contains the tournament id, the group id and the pair id.
     * @param  {Object} res - Express's response object. Res sends the valid opponents.
     */
     getValidOpponents: function(req, res) {
        // logger.debug(req.params.pairId);
        TournamentPersister.findValidOpponents(req.params.tournamentId, req.params.groupId, req.params.pairId).then(function(validOpponents) {
            logger.debug('Pairs the pair havent played with yet:');
            // logger.debug(validOpponents);
            res.send(validOpponents);
        }).catch(function(err) {
            logger.debug(err);
        });

    },

    /**
     * GET - Create a printable pdf with the group's informations 
     * @param  {Object} req - Express's request object. Req contains the tournament id and the group id.
     * @param  {Object} res - Express's response object. Res used to create the pdf.
     */
     viewGroupPrintable: function(req, res) {
        // create a document and pipe to a blob
        var doc = new PDFDocument();
        doc.pipe(res);

        return TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
            return tournament.populate('staffResponsible').execPopulate();
        }).then(function(tournamentPopulated) {
            doc.fontSize(25).text(tournamentPopulated.category + " " + tournamentPopulated.type +
                " (" + tournamentPopulated.ageMin + " - " + tournamentPopulated.ageMax + ")", {
                    align: "center"
                }).moveDown();

            doc.fontSize(20).text("Responsable(s)", {
                align: "center"
            }).moveDown();

            doc.fontSize(15);

            if (_.size(tournamentPopulated.staffResponsible) > 0) {
                _.each(tournamentPopulated.staffResponsible, function(staff) {
                    doc.text("Nom : " + staff.name + " GSM : " + staff.phoneNumber);
                });
            } else {
                doc.text("Il n'y a aucun responsable spécifique pour ce tournoi.");
            }

            doc.moveDown();

            return TournamentPersister.getGroupInTournament(req.params.tournamentId, req.params.groupId);
        }).then(function(groupPopulated) {

            doc.fontSize(20).text("Group Information", {
                align: "center"
            }).moveDown();

            var court = groupPopulated.court;
            doc.fontSize(15).text("Court Address : " + court.courtAddress);

            doc.text("Owner address : " + court.ownerAddress[0].street + " " + court.ownerAddress[0].box + ", " +
                court.ownerAddress[0].zipCode + " " + court.ownerAddress[0].city);

            doc.moveDown();

            if (typeof groupPopulated.leader !== 'undefined') {
                doc.text("Leader : " + groupPopulated.leader.firstName + " " + groupPopulated.leader.lastName);
            }

            doc.text("Number of winners : " + groupPopulated.maxWinners);

            doc.moveDown();

            return TournamentPersister.findPairsInGroup(req.params.tournamentId, req.params.groupId);
        }).then(function(pairsInGroup) {

            doc.fontSize(20).text("Matches", {
                align: "center"
            }).moveDown();

            doc.fontSize(10);

            var x = 1;
            for (var i = 0; i < pairsInGroup.length; i++) {
                for (var y = i + 1; y < pairsInGroup.length; y++) {
                    var pair1 = pairsInGroup[i];
                    var pair2 = pairsInGroup[y];
                    var pairTxt1 = pair1.players[0].firstName + " " + pair1.players[0].lastName + " - " + pair1.players[1].firstName + " " + pair1.players[1].lastName;
                    var pairTxt2 = pair2.players[0].firstName + " " + pair2.players[0].lastName + " - " + pair2.players[1].firstName + " " + pair2.players[1].lastName;
                    doc.text("Match #" + x + " : " + pairTxt1 + " vs. " + pairTxt2 + " : ");
                    x++;
                }
            }

            doc.moveDown();

        }).then(function() {

            doc.fontSize(20).text("Tournament Rules", {
                align: "center"
            }).moveDown();

            // and some justified text wrapped into columns
            doc.font('Times-Roman', 13)
            .text(lorem({
                count: 5,
                units: "paragraphs"
            }), {
                columns: 3,
                columnGap: 15,
                height: 200,
                align: 'justify',
                ellipsis: true
            });

        }).then(function() {
            // end and display the document in the iframe to the right
            doc.end();
        });
    },

    /**
     * GET - Render the template displaying the rain mode option
     * @param  {Object} Req - the Express's request object. Not used
     * @param  {Object} Res - the Express's response object. Res render the view to the rain .
     * @return {Void}
     */
     getRain: function(req, res) {
        res.render('rain');
    },

    /**
     * GET - Redirect to the rain mode management page
     * @param  {Object} Req - the Express's request object. Not used.
     * @param  {Object} Res - the Express's response object. Res redirect to the rain mode page.
     * @return {Void}
     */
     getRainMode: function(req, res) {
        res.redirect('manage_rainmode');
    },

    /**
     * GET - Render the template displaying all groups that can be united
     * @param  {Object} Req - the Express's request object. Not used.
     * @param  {Object} Res - the Express's response object. Res render the view to manage tournaments if it rains.
     * @return {Void}
     */
    getManageRain: function(req, res) {

        var msgStatus = req.msgStatus;

        TournamentPersister.getAllTournamentsWithLeaders().then(function(tournaments) {
            res.render('manage_rain', {
                msgStatus: msgStatus,
                tournaments: tournaments
            });
        });
    },

    /**
     * POST - Unite two groups of a tournament in order to reassign them easily to a covered court. Redirects to same page.
     * @param  {Object} Req - the Express's request object. Req contains the tournament id, the groups's infos.
     * @param  {Object} Res - the Express's response object. Res redirects to the rain mode page.
     * @return {Void}
     */
     doUniteGroups: function(req, res) {
        TournamentPersister.getGroupeById(req.params.tournamentId, req.body.groupA).then(function(groupA) {
            TournamentPersister.getGroupeById(req.params.tournamentId, req.body.groupB).then(function(groupB) {
                TournamentPersister.unionGroup(req.params.tournamentId, groupA, groupB).then(function(argument) {
                    TournamentPersister.removeGroup(req.params.tournamentId, groupB._id).then(function(argument) {
                        req.session.msgStatus = 'Les groupes ont été fusionnés avec succès !';
                        res.redirect('/staff/tournaments/manage_rainmode');
                    });

                });
            });
        });

    },

    /**
     * GET - Render the template displaying all groups of each tournament and the covered courts on which they can be assigned
     * @param  {Object} Req - the Express's request object. Not used.
     * @param  {Object} Res - the Express's response object. Res render the view of reassignment.
     * @return {Void}
     */
    getManageRainReassign: function(req, res) {

        var msgStatus = req.msgStatus;

        CourtPersister.findCourts('Club').then(function(indoorCourts) {
            TournamentPersister.getAllTournamentsWithLeaders().then(function(tournaments) {
                res.render('reassign', {
                    msgStatus: msgStatus,
                    courts: indoorCourts,
                    tournaments: tournaments
                });
            });

        });
    },

    /**
     * POST - Reassign a group to another court
     * @param  {Object} Req - the Express's request object. Req contains the tournament id, the group id and the court id.
     * @param  {Object} Res - the Express's response object. Res redirect to the reassignment page.
     * @return {Void}
     */
     reassignGroupCourt: function(req, res) {
        var tournamentId = req.params.tournamentId;
        var groupId = req.params.groupId;
        var courtId = req.body[groupId];

        TournamentPersister.getGroupeById(tournamentId, groupId).then(function(group) {
            TournamentPersister.updateGroup(req.params.tournamentId, req.params.groupId, {
                court: courtId,
                leader: group.leader,
                gamesHistory: group.gamesHistory,
                maxWinners: group.maxWinners,
                playersLevelInterval: group.playersLevelInterval,
                pairs: group.pairs
            }).then(function(argument) {
                req.session.msgStatus = 'Le groupe a bien été assigné au terrain choisi.';
                res.redirect('/staff/tournaments/manage_rainmode/reassign');
            });
        })
    },

    /**
     * GET - Redirect to confirmation of court reassignment success
     * @param  {Object} Req - the Express's request object. Req contains a possible message status or error.
     * @param  {Object} Res - the Express's response object. Res redirects to the staff section.
     * @return {Void}
     */
     getDoneReassign: function(req, res) {
        req.session.msgStatus = "Les groupes ont été répartis avec succès !";
        res.redirect('/staff');
    }

};