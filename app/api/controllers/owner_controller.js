var datalayer = require('./../../datalayer');
var WebService = require("../../helpers/WebServices");
var PlayerPersister = datalayer.persisters.PlayerPersister;
var Court = datalayer.models.Court;
var CourtPersister = datalayer.persisters.CourtPersister;
var logger = require('winston');

/**
 * The owner's section controller
 * @module app/api/controllers/owner_controller
 * @type {Object}
 */
module.exports = {

    /**
     *     GET - Show the view with a confirmation message.
     *     @param  {Object} req - Express's request object. Not used.
     *     @param  {Object} res - Express's response object. Res renders the view with the registration's confirmation
     */
    getRegistered: function(req, res) {
        res.render('registered_owner');
    },

    /**
     *     GET - Show the view with the form to register a new owner with his court
     *     @param  {Object} req - Express's request object. Req contains a possible message status or error.
     *     @param  {Object} res - Express's response object. Res renders the view to register a owner
     */
    getRegister: function(req, res) {
        var val = req.session.val;
        var msgStatus = req.msgStatus;  
        var errors = req.msgError;
        res.render('register_owner', {
            msgStatus: msgStatus,
            errors: errors,
            val: val,
        });
    },

    /**
     *     POST - Add new owner and his court. The curt must be disposable at least one day.
     *     If the form is incomplete or the court aren't disposable a msgErro is returned.
     *     @param  {Object} req - Express's request object. Req contains the owner's attributs to add and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the register owner page
     */
    doAdd: function(req, res) {
        // var surfacesArray = Court.schema.path('courtSurface').enumValues;
        // var ownerTypesArray = Court.schema.path('typeOwner').enumValues;
        req.session.msgError = [];
        var saturday = (req.body.saturdayFree == 'free');
        var sunday = (req.body.sundayFree == 'free');

        if (!saturday && !sunday) {
            //Satuday AND sunday are false, then we discard the registration
            req.session.msgError.push('Disponibilité. Il faut que le terrain soit disponible minimum un jour');
        }
        if (req.body.title == '') {
            req.session.msgError.push('Titre');
        }
        if (req.body.first_name == '') {
            req.session.msgError.push('Prénom');
        }
        if (req.body.last_name == '') {
            req.session.msgError.push('Nom');
        }
        if (req.body.street == '') {
            req.session.msgError.push('Rue');
        }
        if (req.body.streetNumber == '') {
            req.session.msgError.push('Numéro de rue');
        }
        if (req.body.zipCode == '') {
            req.session.msgError.push('Code postal');
        }
        if (req.body.city == '') {
            req.session.msgError.push('Ville');
        }
        if (req.body.gsm == '') {
            req.session.msgError.push('Numéro de téléphone');
        }
        if (req.body.email == '') {
            req.session.msgError.push('Addresse mail');
        }
        if (req.body.surface == '') {
            req.session.msgError.push('Surface');
        }
        if (req.body.ownerType == undefined) {
            req.session.msgError.push('Type de propriétaire');
        }
        if (req.body.saturdayFree == undefined && req.body.sundayFree == undefined) {
            req.session.msgError.push('Jour de disponibilité');
        }
        if (req.body.instructions == '') {
            // Are instructions mandatory?
        }

        var ownerAddress = req.body.street + ' ' + req.body.streetNumber + ', ' + req.body.city + ' ' + req.body.zipCode;
        var courtAdress;
        if (req.body.sameAddress == 'on') {
            //owner Adress != court
            if (req.body.streetCourt == '') {
                req.session.msgError.push('Rue du court');
            }
            if (req.body.streetNumberCourt == '' || req.body.streetNumberCourt < 1) {
                req.session.msgError.push('Numéro de rue du court');
            }
            if (req.body.cityCourt == '') {
                req.session.msgError.push('Ville du court');
            }
            if (req.body.zipCodeCourt == '' || req.body.zipCodeCourt < 1) {
                req.session.msgError.push('Code postal court');
            }
            courtAdress = req.body.streetCourt + ' ' + req.body.streetNumberCourt + ', ' + req.body.cityCourt + ' - ' + req.body.zipCodeCourt;
        } else {
            courtAdress = ownerAddress;
        }
        if (req.session.msgError.length == 0) {
            WebService.getLatitudeLongitude(courtAdress).then(function(location) {
                var creationYear = new Date().getFullYear();

                var promise = CourtPersister.createCourt({
                    courtNumber: req.body.courtNumber,
                    courtAddress: courtAdress,
                    courtSurface: req.body.surface,
                    typeOwner: req.body.ownerType,
                    specialInstructions: req.body.instructions,
                    ownerAddress: {
                        street: req.body.street,
                        box: req.body.streetNumber,
                        zipCode: req.body.zipCode,
                        city: req.body.city
                    },
                    latitude: location.lat,
                    longitude: location.lng,
                    editions: [{
                        year: creationYear,
                        saturdayFree: saturday,
                        sundayFree: sunday,
                    }]
                });

                promise.then(function() {
                    res.redirect('/register-owner/done');
                }).catch(function(err) {
                    req.session.msgError.push('Il y a eu une erreur avec votre requête, veuillez réessayer plus tard.');
                    logger.debug(err);
                });
            }).catch(function (error) {
                req.session.val = [];
                req.session.val = req.body;
                req.session.msgError.push('Adresse introuvable.');
                res.redirect('/register-owner');
                logger.debug(err);                
            });
        } else {
            req.session.val = [];
            req.session.val = req.body;
            // req.session.first_name = req.body.first_name;
            res.redirect('/register-owner');
        }
    }
};
