var _ = require('underscore');
var datalayer = require('./../../datalayer');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var logger = require('winston');
var WebService = require("../../helpers/WebServices");

var CourtPersister = datalayer.persisters.CourtPersister;
var HistoryEntryPersister = datalayer.persisters.HistoryEntryPersister;

/**
 * The court's section controller.
 * @module app/api/controllers/court_controller
 * @type {Object}
 */
module.exports = {
    /**
     *     GET - Show detailed informations about the court selected by ID.
     *     @param  {Object} req - Express's request object. Req contains the court id
     *     @param  {Object} res - Express's response object. Res renders the view with a court's infos
     */
    getCourtById: function(req, res) {

        var courtId = req.params.courtId;
        CourtPersister.findCourtById(courtId).then(function(court) {
            logger.debug(court);
            var courtSurface = {
                'grass': court.courtSurface === 'Gazon',
                'synthetic': court.courtSurface === 'Synthétique',
                'clay': court.courtSurface === 'Brique pilée',
                'beton': court.courtSurface === 'Béton'
            };

            res.render('court_details', {
                court: court,
                ownerAddress: court.ownerAddress[0],
                courtSurface: courtSurface,
                editions: court.editions.reverse()
            })
        }).catch(function(err) {
            logger.debug('getCourtById: ' + err);
        })
    },

    /**
     *     GET - Show detail a list with all court.
     *     @param  {Object} req - Express's request object. Req contains a possible message status or error.
     *     @param  {Object} res - Express's response object. Res renders the view of the courts's list
     */
    getCourtsList: function(req, res) {

        var msgStatus = req.msgStatus;

        CourtPersister.findAllCourts().then(function(courts) {
            res.render('court_list', {
                courts: courts,
                msgStatus: msgStatus
            })
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     *     GET - Send a JSON lists with some courts, filtered by criteria.
     *     @param  {Object} req - Express's request object. Req contains the search criteria
     *     @param  {Object} res - Express's response object. Res sends the courts found
     */
    getSearchWithCriteria: function(req, res) {
        var criteria = req.params.criteria;

        CourtPersister.findCourts(criteria).then(function(courts) {
            logger.debug(courts);
            res.send(courts);
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     *     GET - Send a JSON lists with all courts.
     *     @param  {Object} req - Express's request object. Not used
     *     @param  {Object} res - Express's response object. Res sends the courts found
     */
    getSearchWithoutCriteria: function(req, res) {
        CourtPersister.findAllCourts().then(function(courts) {
            res.send(courts);
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     *     POST - Add a comment on a edition of a court.
     *     @param  {Object} req - Express's request object. Req contains the court id, the edition id and the comment to added
     *     @param  {Object} res - Express's response object. Res redirects to the court page
     */
    postComment: function(req, res) {
        var courtId = req.params.courtId;
        var editionId = req.body.action;

        CourtPersister.createComment(courtId, editionId, {
            who: req.session.user.username,
            commentary: req.body.textarea1,
            createdAt: new Date()
        }).then(function(courts) {
            res.redirect('/court/' + courtId);
        }).catch(function(err) {
            logger.debug('postComment' + err);
        });
    },

    /**
     *     GET - Show the court-edit page in the purpose to modify this court.
     *     @param  {Object} req - Express's request object. Req contains the court id and a possible message status or error
     *     @param  {Object} res - Express's response object. Res renders view to manage courts
     */
    getManageCourt: function(req, res) {

        var msgStatus = req.msgStatus;
        var msgError = req.msgError;

        var id = req.params.courtId;
        CourtPersister.findCourtById(id).then(function(court) {

            // Get history log
            HistoryEntryPersister.getHistoryEntriesById(id).then(function(entriesArray) {
                logger.debug(JSON.stringify(entriesArray));
                res.render('manage_court', {
                    court: court,
                    history: entriesArray,
                    errors: msgError
                });
            }).catch(function(err) {
                logger.debug(err);
            });
        }).catch(function(err) {
            logger.debug('getManageCourt' + err);
        });
    },

    /**
     *     POST - Add a new edition on a court.
     *     @param  {Object} req - Express's request object. Req contains the edition's attributs to add and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the courts list page
     */
    addEdition: function(req, res) {
        req.session.msgError = [];
        req.session.msgStatus = "";

        var id = req.params.courtId;
        var year = req.body.year;
        var saturdayFree = (req.body.saturdayFree == 'free');
        var sundayFree = (req.body.sundayFree == 'free');
        var courtConditions = req.body.courtConditions;
        var user = req.session.user.name;

        if (req.session.msgError.length == 0) {
            CourtPersister.createEdition(id, {
                year: year,
                courtConditions: courtConditions,
                saturdayFree: saturdayFree,
                sundayFree: sundayFree,
                encodedBy: user,
                encodedAt: new Date()
            }).then(function() {
                req.session.msgStatus = 'Edition Ajoutée avec succes';
                res.redirect('/courts_list');
            });
        } else {
            var id = req.params.id;
            req.session.msgStatus = 'Il y a eu une erreur avec votre requête';
            res.redirect('/court/edit/' + id);
        }
    },

    /**
     *     POST - Modify a court, only fields belong to a court.
     *     Register modifications into the history.
     *
     *     @param  {Object} req - Express's request object. Req contains the court's attributes to modify and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the courts list page or remains on the page.
     */
    editCourt: function(req, res) {
        req.session.msgError = [];
        req.session.msgStatus = "";
        var id = req.params.courtId;
        var courtNumber = req.body.courtNumber;
        var courtAddress = req.body.courtAddress;
        var courtSurface = req.body.courtSurface;
        var typeOwner = req.body.typeOwner;
        var specialInstructions = req.body.specialInstructions;

        if (courtNumber == '' || courtNumber < '0') {
            req.session.msgError.push('Numéro du court');
        }
        if (courtAddress == '') {
            req.session.msgError.push('Adresse');
        }
        if (courtSurface == '') {
            req.session.msgError.push('Surface du court');
        }
        if (typeOwner == '') {
            req.session.msgError.push('Surface de court');
        }

        if (req.session.msgError.length == 0) {
            var modifiedFields = [];

            CourtPersister.findCourtById(id).then(function(oldCourt) {

                if (oldCourt.courtNumber != courtNumber) {
                    modifiedFields.push({
                        "name": "courtNumber",
                        "value": oldCourt.courtNumber
                    });
                }

                if (oldCourt.courtAddress !== courtAddress) {
                    modifiedFields.push({
                        "name": "courtAddress",
                        "value": oldCourt.courtAddress
                    });
                }

                if (oldCourt.courtSurface !== courtSurface) {
                    modifiedFields.push({
                        "name": "courtSurface",
                        "value": oldCourt.courtSurface
                    });
                }

                if (oldCourt.typeOwner !== typeOwner) {
                    modifiedFields.push({
                        "name": "typeOwner",
                        "value": oldCourt.typeOwner
                    });
                }
                WebService.getLatitudeLongitude(courtAddress).then(function(location) {
                    CourtPersister.editCourt(id, {
                        courtNumber: courtNumber,
                        courtAddress: courtAddress,
                        courtSurface: courtSurface,
                        typeOwner: typeOwner,
                        specialInstructions: specialInstructions,
                        latitude: location.lat,
                        longitude: location.lng,
                    }).then(function() {

                        // Log to history
                        if (modifiedFields.length > 0) {
                            var promises = [];
                            for (var field in modifiedFields) {
                                var promise = HistoryEntryPersister.createHistoryEntry({
                                    date: new Date(),
                                    author: req.session.user.name,
                                    updatedId: id,
                                    updatedCollection: 'Court',
                                    updatedField: modifiedFields[field].name,
                                    oldValue: modifiedFields[field].value
                                });
                                promises.push(promise);
                            }
                            Promise.all(promises).then(function(argument) {
                                logger.debug("Saved modifications to history.");
                            });
                        }

                        req.session.msgStatus = 'Court modifié avec succes';
                        res.redirect('/courts_list');
                    }).catch(function(err) {
                        req.session.msgError.push('Il y a eu une erreur avec votre requête, veuillez réessayer plus tard.');
                        logger.debug(err);
                    });
                }).catch(function(err) {
                    console.log()
                });

            }).catch(function(err) {
                logger.debug(err);
            });
        } else {
            res.redirect('/court/edit/' + id);
        }

    },

    /**
     *     POST - Modify an edition of a court, only fields belong to a edition (execpt year).
     *     Register modifications into the history.
     *
     *     @param  {Object} req - Express's request object. Req contains the edition's attributes of a court to modify and a possible message status or error.
     *     @param  {Object} res - Express's response object. Res redirects to the courts list page or remains on the page.
     */
    editCourtEdition: function(req, res) {
        req.session.msgError = [];
        req.session.msgStatus = "";
        var courtId = req.params.courtId;
        var editionId = req.body.action;

        var saturdayFree = (req.body.saturdayFree == 'free');
        var sundayFree = (req.body.sundayFree == 'free');
        var courtConditions = req.body.courtConditions;
        var user = req.session.user.name;

        if (req.session.msgError.length == 0) {
            var modifiedFields = [];

            CourtPersister.findEditionById(courtId, editionId).then(function(oldEdition) {

                if (oldEdition.courtConditions !== courtConditions) {
                    modifiedFields.push({
                        "name": "courtNumber",
                        "value": oldEdition.courtNumber
                    });
                }

                if (oldEdition.saturdayFree !== saturdayFree) {
                    modifiedFields.push({
                        "name": "courtAddress",
                        "value": oldEdition.courtAddress
                    });
                }

                if (oldEdition.sundayFree !== sundayFree) {
                    modifiedFields.push({
                        "name": "courtSurface",
                        "value": oldEdition.courtSurface
                    });
                }

                CourtPersister.editEdition(courtId, editionId, {
                    courtConditions: courtConditions,
                    saturdayFree: saturdayFree,
                    sundayFree: sundayFree,
                    encodedBy: user,
                    encodedAt: new Date()
                }).then(function() {

                    // Log to history
                    if (modifiedFields.length > 0) {
                        var promises = [];
                        for (var field in modifiedFields) {
                            var promise = HistoryEntryPersister.createHistoryEntry({
                                date: new Date(),
                                author: req.session.user.name,
                                updatedId: id,
                                updatedCollection: 'Court.Edition',
                                updatedField: modifiedFields[field].name,
                                oldValue: modifiedFields[field].value
                            });
                            promises.push(promise);
                        }
                        Promise.all(promises).then(function(argument) {
                            console.log("Saved modifications to history.");
                        });
                    };

                    req.session.msgStatus = 'Edition modifié avec succes';
                    res.redirect('/courts_list');
                }).catch(function(err) {
                    req.session.msgError.push('Il y a eu une erreur avec votre requête, veuillez réessayer plus tard.');
                    console.log(err);
                });

            }).catch(function(err) {
                console.log(err);
            });
        } else {
            var id = req.params.id;
            res.redirect('/court/edit/' + id);
        }

    }
};
