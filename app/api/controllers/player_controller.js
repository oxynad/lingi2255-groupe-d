var datalayer = require('./../../datalayer');
var nodemailer = require('nodemailer');
var dateFormat = require('dateformat');
var mongoose = require('mongoose');
var md5 = require('md5');
var _ = require('underscore');
var config = require('./../../../config');
var PlayerPersister = datalayer.persisters.PlayerPersister;
var PairPersister = datalayer.persisters.PairPersister;
var ExtraPersister = datalayer.persisters.ExtraPersister;
var utilsController = require('./utils_controller');
var moment = require('moment');
moment.locale('fr');
var Promise = require('bluebird');
var logger = require('winston');

/**
 * The application's player's controller
 * @module app/api/controllers/player_controller
 * @type {Object}
 */
module.exports = {

    /**
     * GET - Show the page informing the player that he is registered
     * @param  {Object} req - Express's request object, not used
     * @param  {Object} res - Express's response object, send information about registration
     */
    getRegistered: function(req, res) {
        ExtraPersister.findExtras().then(function(extras) {
            res.render('registered', {
                alreadyRegistered: req.session.alreadyRegistered,
                extras_to_registered: req.session.extras_to_registered,
                totalPrice: req.session.totalPrice,
                pairId: req.session.pairId,
                extras: extras
            });
        });
    },

    /**
     * GET - Display the page informing the player that he successfully paid via Paypal
     * @param  {Object} req - Express's request object, not used
     * @param  {Object} res - Express's response object, just create the page
     */
    getPayed: function(req, res) {
        res.render('payed');
    },

    /**
     * GET - Display the page informing the player that has not yet paid and has chosen to pay
     * by cash at the HQ
     * @param  {Object} req - Express's request object, not used
     * @param  {Object} res - Express's response object, just create the page
     */
    getToPayReminder: function(req, res) {
        res.render('to_pay_reminder');
    },

    /**
     * GET - Show the player's registration page
     * @param  {Object} req - Express's request object, get msgErrors back from player.doAdd controller
     * @param  {Object} res - Express's response object, send msgErrors and precomplete form
     */
    getRegister: function(req, res) {
        ExtraPersister.findExtras().then(function(extras) {
            var extras_content = [];
            /* Get the lists of rankings (AFT) and extras*/
            var rankings = PlayerPersister.findAllRankingPossible();
            for (var i = 0; i < extras.length; i++) {
                extras_content.push({
                    price: extras[i].price,
                    title: extras[i].extraTitle,
                    id: extras[i]._id
                });
            }

            /* Get the error messages back from the player.doAdd controller*/
            var msgError1 = req.session.msgError1;
            var msgError2 = req.session.msgError2;

            delete req.session.msgError1;
            delete req.session.msgError2;

            /* Pre-complete the fields if an error happened*/
            res.render('register_player', {
                rankings: rankings,
                msgError1: msgError1,
                msgError2: msgError2,
                extras: extras_content,

                title1: req.session.title1,
                first_name1: req.session.first_name1,
                last_name1: req.session.last_name1,
                street1: req.session.street1,
                streetNumber1: req.session.streetNumber1,
                box1: req.session.box1,
                zipCode1: req.session.zipCode1,
                city1: req.session.city1,
                gsm1: req.session.gsm1,
                birthDate1: req.session.birthDate1,
                email1: req.session.email1,
                aftRanking1: req.session.aftRanking1,

                title2: req.session.title2,
                first_name2: req.session.first_name2,
                last_name2: req.session.last_name2,
                street2: req.session.street2,
                streetNumber2: req.session.streetNumber2,
                box2: req.session.box2,
                zipCode2: req.session.zipCode2,
                city2: req.session.city2,
                gsm2: req.session.gsm2,
                birthDate2: req.session.birthDate2,
                email2: req.session.email2,
                aftRanking2: req.session.aftRanking2
            });
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     * POST - Adds the 2 players and their pair in the database, sends the confirmation mails
     * @param  {Object} req - Express's request object, get all the data from the form
     * @param  {Object} res - Express's response object, redirect to /registered if registration ok, if not back to /register
     */
    doAdd: function(req, res) {

        /* Load the characteristics of the extras then... */
        ExtraPersister.findExtras().then(function(extras) {
            /* Retrieve the data from the register form */
            var title1 = req.body.title1;
            var first_name1 = req.body.first_name1;
            var last_name1 = req.body.last_name1;
            var street1 = req.body.street1;
            var streetNumber1 = req.body.streetNumber1;
            var box1 = req.body.box1 || 0;
            var zipCode1 = req.body.zipCode1;
            var city1 = req.body.city1;
            var gsm1 = req.body.gsm1;
            var birthDate1 = req.body.birthDate1;
            var email1 = req.body.email1;
            var aftRanking1 = req.body.aftRanking1;

            var title2 = req.body.title2;
            var first_name2 = req.body.first_name2;
            var last_name2 = req.body.last_name2;
            var street2 = req.body.street2;
            var streetNumber2 = req.body.streetNumber2;
            var box2 = req.body.box2 || 0; 
            var zipCode2 = req.body.zipCode2;
            var city2 = req.body.city2;
            var gsm2 = req.body.gsm2;
            var birthDate2 = req.body.birthDate2;
            var email2 = req.body.email2;
            var aftRanking2 = req.body.aftRanking2;
            var comment = req.body.comments;    

            birthDate1 = new Date(birthDate1);
            birthDate2 = new Date(birthDate2);

            var comments = [];
            comments.push(comment);

            /* Extra Management */
            var pairsExtras = [];   /* List of the extras of the pair with their respective qty */
            var totalPrice = 40; /* Total of the money due by the pair, start at the price of the tournament */
            var extrasSelected = req.body.extra; /* Retrieve the extras of the pair */

            var qty;
            var i = 0; /* Used to count the extras and access it directly in extras[i] */
            var extras_to_registered = []; /* Extras sent to the registered page */

            /* Iterate through each extras from the register form, save them in pairsExtras and extras_to_registered
                and add to the totalPrice*/
            _.each(extrasSelected, function(qty, id) {
                if (qty > 0) {
                    var extraChosen = extras[i];

                    pairsExtras.push({
                        quantity: qty,
                        extra: mongoose.Types.ObjectId(extraChosen._id)
                    });

                    totalPrice += extraChosen.price * qty;

                    extras_to_registered.push({
                        qty: qty,
                        extras: extras[i]
                    });
                }
                i++;

            });

            /* /Extra Management */

            var alreadyInPoule = req.body.alreadyInPoule || false;

            req.session.msgError1 = [];
            req.session.msgError2 = [];
            req.session.alreadyRegistered = '';

            /* If a field is empty, add it to the list of errors */
            if (title1 == '') req.session.msgError1.push('Titre');
            if (title2 == '') req.session.msgError2.push('Titre');
            if (first_name1 == '') req.session.msgError1.push('Prénom');
            if (first_name2 == '') req.session.msgError2.push('Prénom');
            if (last_name1 == '') req.session.msgError1.push('Nom');
            if (last_name2 == '') req.session.msgError2.push('Nom');
            if (street1 == '') req.session.msgError1.push('Rue');
            if (street2 == '') req.session.msgError2.push('Rue');
            if (streetNumber1 == '') req.session.msgError1.push('Numéro de rue');
            if (streetNumber2 == '') req.session.msgError2.push('Numéro de rue');
            if (zipCode1 == '') req.session.msgError1.push('Code postal');
            if (zipCode2 == '') req.session.msgError2.push('Code postal');
            if (city1 == '') req.session.msgError1.push('Ville');
            if (city2 == '') req.session.msgError2.push('Ville');
            if (gsm1 == '') req.session.msgError1.push('Numéro de téléphone');
            if (gsm2 == '') req.session.msgError2.push('Numéro de téléphone');
            if (birthDate1 == '') req.session.msgError1.push('Date de naissance');
            if (birthDate2 == '') req.session.msgError2.push('Date de naissance');
            if (email1 == '') req.session.msgError1.push('Addresse mail');
            if (email2 == '') req.session.msgError2.push('Addresse mail');
            if (aftRanking1 == '') req.session.msgError1.push('Classement AFT');
            if (aftRanking2 == '') req.session.msgError2.push('Classement AFT');     

            if (req.session.msgError1.length == 0 && req.session.msgError2.length == 0) {
                /* Bag of values randomly taken to generate the confirmation key of 62 characters */
                var valuesTab = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
                var key1 = "", key2 = "";

                for (var i = 0; i < 15; i++) {
                    key1 += valuesTab[Math.floor(Math.random() * 61)];
                    key2 += valuesTab[Math.floor(Math.random() * 61)];
                }

                var promises = [];

                /* Transform the data into objects to pass it to the persister functions */
                var params_player1 = { title: title1, firstName: first_name1, lastName: last_name1, street: street1,
                            streetNumber: streetNumber1, box: box1, zipCode: zipCode1, city: city1, gsm: gsm1,
                            birthdate: birthDate1, email: email1, aftRanking: aftRanking1, alreadyInPoule: alreadyInPoule,
                            keyEmailConfirm: key1
                        };
                var params_player1_already_registered = { title: title1, street: street1, streetNumber: streetNumber1,
                            box: box1, zipCode: zipCode1, city: city1, gsm: gsm1, email: email1, aftRanking: aftRanking1,
                            alreadyInPoule: alreadyInPoule, keyEmailConfirm: key1 };
                var params_player2 = { title: title2, firstName: first_name2, lastName: last_name2, street: street2,
                            streetNumber: streetNumber2, box: box2, zipCode: zipCode2, city: city2, gsm: gsm2, 
                            birthdate: birthDate2, email: email2, aftRanking: aftRanking2, alreadyInPoule: alreadyInPoule,
                            keyEmailConfirm: key2
                        };
                var params_player2_already_registered = { title: title2, street: street2, streetNumber: streetNumber2,
                            box: box2, zipCode: zipCode2, city: city2, gsm: gsm2, email: email2, aftRanking: aftRanking2,
                            alreadyInPoule: alreadyInPoule, keyEmailConfirm: key2
                        };

                /* Search the player by Name, FirstName and Birthdate, if it finds something, edit it
                    if not, create a new player */
                promises.push(PlayerPersister.checkPlayerExists(last_name1, first_name1, birthDate1).then(function(player1) {
                    if (player1) {
                        req.session.alreadyRegistered = last_name1 + ' ' + first_name1;
                        logger.debug("p1 existe");
                        return PlayerPersister.editPlayer(player1._id, params_player1_already_registered);

                    } else {
                        logger.debug("p1 n'existe pas");
                        return PlayerPersister.createPlayer(params_player1);
                    }
                }));
                promises.push(PlayerPersister.checkPlayerExists(last_name2, first_name2, birthDate2).then(function(player2) {
                    if (player2) {
                        if (req.session.alreadyRegistered == '') {
                            req.session.alreadyRegistered = last_name2 + ' ' + first_name2;
                        } else {
                            req.session.alreadyRegistered += ' et ' + last_name2 + ' ' + first_name2;
                        }
                        logger.debug("p2 existe deja");
                        return PlayerPersister.editPlayer(player2._id, params_player2_already_registered);
                    } else {
                        logger.debug("p2 n'existe pas");
                        return PlayerPersister.createPlayer(params_player2);
                    }
                }));

                /* Execute the creation/editing of the players */
                Promise.all(promises).then(function(res) {
                    /*If update, Promise returns a table in 2D
                        If create, Promise returns a table in 1D */
                    var p1 = res[0][0];
                    var p2 = res[1][0];

                    if (typeof p1 == 'undefined') {
                        p1 = res[0];
                    }
                    if (typeof p2 == 'undefined') {
                        p2 = res[1];
                    }

                    var urlRoot = config.server.url; /* Get the urlRoot to make the confirm link */

                    /* Format the date for the email */
                    var date1 = moment(birthDate1);
                    var date2 = moment(birthDate2);
                    date1 = date1.format("D/M/YYYY");
                    date2 = date2.format("D/M/YYYY");

                    /* Create content of the mails */
                    var mailContentPlainText1 = 'Bonjour,\n' + 'Vous recevez ce mail pour vous confirmer que nous avons bien reçu vos informations ' + 'pour l\'inscription au tournoi Le Charles de Lorraine.\n' + 'Veuillez confirmer votre inscription via le lien suivant:' + urlRoot + '/register/confirm/' + key1 + 'Voici un récapitulatif de vos informations:\n' + 'Joueur 1\n' + title1 + ' ' + last_name1 + ' ' + first_name1 + '\nAdresse: ' + street1 + ' n°' + streetNumber1 + ' ' + box1 + '\n' + zipCode1 + ', ' + city1 + '\n' + gsm1 + '\nDate de naissance: ' + date1 + '\nrang AFT: ' + aftRanking1;
                    var mailContentPlainText2 = 'Bonjour,\n' + 'Vous recevez ce mail pour vous confirmer que nous avons bien reçu vos informations ' + 'pour l\'inscription au tournoi Le Charles de Lorraine.\n' + 'Veuillez confirmer votre inscription via le lien suivant:' + urlRoot + '/register/confirm/' + key2 + 'Voici un récapitulatif de vos informations:\n' + 'Joueur 2\n' + title2 + ' ' + last_name2 + ' ' + first_name2 + '\nAdresse: ' + street2 + ' n°' + streetNumber2 + ' ' + box2 + '\n' + zipCode2 + ', ' + city2 + '\n' + gsm2 + '\nDate de naissance: ' + date2 + '\nrang AFT: ' + aftRanking2;
                    var mailContentHTML1 = '<p>Bonjour,</p>' + '<p>Vous recevez ce mail pour vous confirmer que nous avons bien reçu vos informations ' + 'pour l\'inscription au tournoi Le Charles de Lorraine.</p>' + '<a>' + urlRoot + '/register/confirm/' + key1 + '</a>' + '<p>Voici un récapitulatif de vos informations:</p>' + '<p>Joueur 1</p>' + '<ul>' + '<li>' + title1 + ' ' + last_name1 + ' ' + first_name1 + '</li>' + '<li>Adresse: ' + street1 + ' n°' + streetNumber1 + ' ' + box1 + '<br\\>' + zipCode1 + ', ' + city1 + '</li>' + '<li>' + gsm1 + '</li>' + '<li>Date de naissance: ' + date1 + '</li>' + '<li>rang AFT: ' + aftRanking1 + '</li>' + '</ul>';
                    var mailContentHTML2 = '<p>Bonjour,</p>' + '<p>Vous recevez ce mail pour vous confirmer que nous avons bien reçu vos informations ' + 'pour l\'inscription au tournoi Le Charles de Lorraine.</p>' + '<a>' + urlRoot + '/register/confirm/' + key2 + '</a>' + '<p>Voici un récapitulatif de vos informations:</p>' + '<p>Joueur 2</p>' + '<ul>' + '<li>' + title2 + ' ' + last_name2 + ' ' + first_name2 + '</li>' + '<li>Adresse: ' + street2 + ' n°' + streetNumber2 + ' ' + box2 + '<br\\>' + zipCode2 + ', ' + city2 + '</li>' + '<li>' + gsm2 + '</li>' + '<li>Date de naissance: ' + date2 + '</li>' + '<li>rang AFT: ' + aftRanking2 + '</li>' + '</ul>';

                    /* Init list of receivers */
                    var emailReceiver1 = [];
                    emailReceiver1.push(email1);
                    var emailReceiver2 = [];
                    emailReceiver2.push(email2);

                    /* Send confirmation emails */
                    utilsController.sendMail(emailReceiver1, mailContentPlainText1, mailContentHTML1, 
                        'Confirmation pour l\'inscription au tournoi');
                    utilsController.sendMail(emailReceiver2, mailContentPlainText2, mailContentHTML2, 
                        'Confirmation pour l\'inscription au tournoi');

                    return PairPersister.createPair({
                        players: [p1._id, p2._id],
                        extra: pairsExtras,
                        montant: totalPrice,
                        comment: comment
                    });

                }).then(function(pairs) {
                    req.session.pairId = pairs[0]._id;
                    req.session.totalPrice = totalPrice;
                    req.session.extras_to_registered = extras_to_registered;
                    res.redirect('/registered/');
                }).catch(function(err) {
                    req.session.msgError1.push('There was an error with your request, please try again later.');
                    logger.debug(err);
                    logger.error(err);
                    res.redirect('/register');
                });
            } else {

                /*
                Precomplete html form with correct information
                */
                req.session.title1 = title1;
                req.session.first_name1 = first_name1;
                req.session.last_name1 = last_name1;
                req.session.street1 = street1;
                req.session.streetNumber1 = streetNumber1;
                req.session.box1 = box1;
                req.session.zipCode1 = zipCode1;
                req.session.city1 = city1;
                req.session.gsm1 = gsm1;
                req.session.birthDate1 = birthDate1;
                req.session.email1 = email1;
                req.session.aftRanking1 = aftRanking1;

                req.session.title2 = title2;
                req.session.first_name2 = first_name2;
                req.session.last_name2 = last_name2;
                req.session.street2 = street2;
                req.session.streetNumber2 = streetNumber2;
                req.session.box2 = box2;
                req.session.zipCode2 = zipCode2;
                req.session.city2 = city2;
                req.session.gsm2 = gsm2;
                req.session.birthDate2 = birthDate2;
                req.session.email2 = email2;
                req.session.aftRanking2 = aftRanking2;
                res.redirect('/register');
            }
        });

    },
    
    /**
     * GET - Display the page informing the player that he has successfully confirmed his email
     * or that he went to a wrong URL or has already confirmed his email
     * @param  {Object} req - Express's request object, get key of the confirmation email from the URL
     * @param  {Object} res - Express's response object, display if key correct or not
     */
    confirmEmail: function(req, res) {
        var correctEmail = "";
        /* If  found a player with the confirmKey and has not been already confirmed
            then put the player's email into correctEmail that is sent via the param keyCorrect
            */
        PlayerPersister.findPlayerByKeyEmailConfirm(req.params.key).then(function(player) {
            if (!_.isEmpty(player) && !player.emailConfirmed) {
                correctEmail = player.email;
                player.emailConfirmed = true;
                PlayerPersister.editPlayer(player._id, player);
            }

            res.render('register_player_confirm', {
                keyCorrect: correctEmail
            });
        });
    }
};