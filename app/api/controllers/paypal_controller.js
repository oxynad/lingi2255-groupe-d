var paypal = require('paypal-rest-sdk');
var config = require('./../../../config');
var datalayer = require('./../../datalayer');
var PairPersister = datalayer.persisters.PairPersister;
var ExtraPersister = datalayer.persisters.ExtraPersister;
var _ = require('underscore');
var mongoose = require('mongoose');
var Extra = datalayer.models.Extra;
var logger = require('winston');

/**
 * The application's paypal controller
 * @module app/api/controllers/paypal_controller
 * @type {Object}
 */
module.exports = {

    /**
     * GET - Create Paypal payment from the player
     * @param  {Object} req - Express's request object. Contains the pair id that needs to pay.
     * @param  {Object} res - Express's response object. Redirects to the paypal payment page.
     */
    create: function(req, res) {
        var item_list = [];
        var extrasOfPair = [];
        var totalPayment = 0;
        /* Find the pair by the pair's id in the link */
        PairPersister.findPair(req.params.pairId).then(function(pair){
            /* If the pair does not exist, return page 404 */
            if(typeof pair === 'undefined' || pair == null){
                res.redirect("/404");
                logger.debug("-- Pair " + req.params.pairId + " not found --");
                return;
            }
            /* Init list of items for paypal */
            item_list = [{
                            "name": "Inscription tournoi",
                            "sku": "Joueur 1.",
                            "price": "20.00",
                            "currency": "EUR",
                            "quantity": 1
                        },{
                            "name": "Inscription tournoi",
                            "sku": "Joueur 2.",
                            "price": "20.00",
                            "currency": "EUR",
                            "quantity": 1
                        }];
            totalPayment += 40;
            var promises = [];
            extrasOfPair = pair.extra;

            /* Iterate through the list of the extras and search their attributes */
            _.each(pair.extra, function(extra) {
                promises.push(Extra.findByIdAsync(extra.extra));
            });
            return Promise.all(promises);
        }).then(function(extras) {
                if(typeof extras === 'undefined'){
                    res.redirect("/404");
                    return;
                }
                /* Add each extra in the list of items */
                var i = 0;

                extras.forEach(function(extra){
                    var extra_unit = {
                        "name": extra.extraTitle,
                        "price": "" + extra.price,
                        "sku" : extra.extraTitle,
                        "currency": "EUR",
                        "quantity": extrasOfPair[i].quantity
                    };
                    totalPayment += extrasOfPair[i].quantity * extra.price;
                    i++;
                    item_list.push(extra_unit);
                });
                // pure Paypal payment
                var payment = {
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"
                    },
                    "redirect_urls": {
                        "return_url": config.server.url + "/payment-execute",
                        "cancel_url": config.server.url + "/payment-cancel"
                    },
                    "transactions": [{
                        "item_list": {
                            "items": item_list
                        },
                        "amount": {
                            "total": totalPayment,
                            "currency": "EUR"
                        },
                        "description": "Inscription ASMAE"
                    }]
                };
            // CB via paypal payment
            // var payment = {
            //     "intent": "sale",
            //     "payer": {
            //         "payment_method": "credit_card",
            //         "funding_instruments": [{
            //             "credit_card": {
            //                 "number": "5500005555555559",
            //                 "type": "mastercard",
            //                 "expire_month": 12,
            //                 "expire_year": 2018,
            //                 "cvv2": 111,
            //                 "first_name": "Joe",
            //                 "last_name": "Shopper"
            //             }
            //         }]
            //     },
            //     "transactions": [{
            //         "amount": {
            //             "total": "5.00",
            //             "currency": "EUR"
            //         },
            //         "description": "My awesome payment"
            //     }]
            // };

                paypal.payment.create(payment, function(error, payment) {
                    if (error) {
                        logger.debug(error);
                    } else {
                        if (payment.payer.payment_method === 'paypal') {
                            req.session.paymentId = payment.id;
                            var redirectUrl;
                            _.each(payment.links, function(link){
                                if (link.method === 'REDIRECT')
                                    redirectUrl = link.href;
                            });                    
                            res.redirect(redirectUrl);
                        }
                    }
                });
        });
    },

    /**
     * GET - Execute Paypal Payment
     * @param  {Object} req - Express's request object. Res contains the payment id, pair id and team id
     * @param  {Object} res - Express's response object. Req renders the view confirming the payment
     */
    execute: function(req, res) {
        var paymentId = req.session.paymentId;
        var pairId = req.session.pairId;
        delete req.session.teamId;
        delete req.session.paymentId;
        var payerId = req.param('PayerID');

        var details = {
            "payer_id": payerId
        };

        paypal.payment.execute(paymentId, details, function(error, payment) {
            if (error) {
                logger.debug(error);
            } else {
                PairPersister.updatePair(pairId, {payment: paymentId}).then(function(){
                    res.render('payed');
                });
            }
        });
    },

    /**
     * GET - Display a page saying that the payment has been canceled due to an error
     * @param  {Object} req - Express's request object. Not used
     * @param  {Object} res - Express's response object. Res renders the view of payment cancelled
     */
    cancel: function(req, res) {
        res.render('payment-cancelled');
    }
};
