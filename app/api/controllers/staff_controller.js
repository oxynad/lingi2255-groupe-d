var _ = require('underscore');
var datalayer = require('./../../datalayer');
var PlayerPersister = datalayer.persisters.PlayerPersister;
var ExtraPersister = datalayer.persisters.ExtraPersister;
var GroupPersister = datalayer.persisters.GroupPersister;
var TournamentPersister = datalayer.persisters.TournamentPersister;
var HistoryEntryPersister = datalayer.persisters.HistoryEntryPersister;
var PairPersister = datalayer.persisters.PairPersister;
var logger = require('winston');
var Promise = require('bluebird');
var mongoose = require('mongoose');

/**
 *     The staff's section controller.
 *     @module app/api/controllers/staff_controller
 *     @type {Object}
 */
module.exports = {

    /**
     *     GET - Show the staff's section page.
     *     @param  {Object} req - Express's request object. Req contains a possible message status or error.
     *     @param  {Object} res - Express's response object. Res renders the view showing all the players in the app and shows the possible message status or error aswell.
     *     @return {Void}
     */
    getStaff: function(req, res) {

        var msgStatus = req.msgStatus;

        PlayerPersister.findAllPlayers().then(function(players) {
            res.render('staff_section', {
                players: players,
                msgStatus: msgStatus
            });
        }).catch(function(err) {
            logger.debug(err);
        })
    },


    /**
     *     GET - Sends a JSON object containing all the players
     *     @param  {Object} req - Express's request object. not used
     *     @param  {Object} res - Express's response object. Res sends the players found
     *     @returns {Object} JSON object containing all the players
     */
    getSearch: function(req, res) {
        PlayerPersister.findAllPlayers().then(function(players) {
            res.send(players);
        }).catch(function(err) {
            logger.debug(err);
        });
    },


    /**
     *     GET - Sends a JSON object containing players by a specified criteria
     *     @param  {Object} req - Express's request object. Req contains the search criteria
     *     @param  {Object} res - Express's response object. Res sends the players found
     *     @returns {Object} JSON Object containing players by a specified criteria
     */
    getSearchWithCriteria: function(req, res) {
        var criteria = req.params.criteria;
        PlayerPersister.findPlayers(criteria).then(function(players) {
            // logger.debug(players);
            res.send(players);
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     *     GET - Show the extra's management page.
     *     @param  {Object} req - Express's request object. Req contains a possible message status or error.
     *     @param  {Object} res - Express's response object. Res renders the view with all extras
     *     @return {Void}
     */
    getManageExtras: function(req, res) {

        var msgStatus = req.msgStatus;
        var msgError = req.msgError;

        ExtraPersister.findExtras().then(function(extras) {
            res.render('manage_extras', {
                msgStatus: msgStatus,
                msgError: msgError,
                extras: extras
            });
        }).catch(function(err) {
            logger.debug(err);
        });
    },


    /**
     *     POST - Creates a new extra
     *     @param  {Object} req - Express's request object. Req contains the extra's price and title to create.
     *     @param  {Object} res - Express's response object. Res redirects to the staff extra's section and show a success message.
     *     @return {Void}
     */
    createExtra: function(req, res) {

        var price = req.body.price;
        var extraTitle = req.body.description;

        if (price < 0 || extraTitle === '') {
            req.session.msgError = "Vous avez inséré des données éronnées";
            res.redirect('/staff/extras');

        } else {
            ExtraPersister.createExtra({
                price: price,
                extraTitle: extraTitle
            }).then(function() {
                req.session.msgStatus = "Extra créé avec succès";
                res.redirect('/staff/extras');
            }).catch(function(err) {
                logger.debug(err);
            });

        }

    },


    /**
     *     POST - Edits an extra's information
     *     @param  {Object} req - Express's request object. Req contains the extra's id , price and title to modify.
     *     @param  {Object} res - Express's response object. Res redirects to the staff extra's section and shows a success message.
     *     @return {Void}
     */
    editExtra: function(req, res) {


        var extraId = req.params.id;
        var price = req.body.price;
        var extraTitle = req.body.description;

        if (price < 0 || extraTitle === '') {
            req.session.msgError = "Vous avez inséré des données éronnées";
            res.redirect('/staff/extras');
        } else {
            ExtraPersister.editExtra(extraId, {
                price: price,
                extraTitle: extraTitle
            }).then(function() {
                req.session.msgStatus = "Extra modifié avec succès";
                res.redirect('/staff/extras');
            }).catch(function(err) {
                logger.debug(err);
            });
        }
    },


    /**
     *     GET - Deletes an extra
     *     @param  {Object} req - Express's request object. Req contains the extra's id to delete.
     *     @param  {Object} res - Express's response object. Res redirects to the extras section.
     *     @return {Void}
     */
    deleteExtra: function(req, res) {
        var extraId = req.params.id;

        ExtraPersister.deleteExtra(extraId).then(function() {
            req.session.msgStatus = "Extra supprimé avec succès";
            res.redirect('/staff/extras');
        })
    },



    /**
     *     GET - Deletes a player
     *     @param  {Object} req - Express's request object. Req contains the player id to be deleted.
     *     @param  {Object} res - Express's response object. Res redirects to the staff section.
     *     @return {Void}
     */
    deletePlayer: function(req, res) {
        var playerId = req.params.id;

        PlayerPersister.deletePlayer(playerId).then(function() {
            req.session.msgStatus = "Joueur supprimé avec succès";
            res.redirect('/staff');
        })
    },


    /**
     *     GET - Shows the player's management page
     *     @param  {Object} req - Express's request object. Req contains possible message status or error and the player's id.
     *     @param  {Object} res - Express's response object. Res renders the view to manage players.
     *     @return {Void}
     */
    getManagePlayer: function(req, res) {

        var msgStatus = req.msgStatus;
        var msgError = req.msgError;

        var rankings = PlayerPersister.findAllRankingPossible();
        var id = req.params.id;
        PlayerPersister.findPlayer(id).then(function(player) {


            // Get history log
            HistoryEntryPersister.getHistoryEntriesById(id).then(function(entriesArray) {
                res.render('manage_player', {
                    msgError: msgError,
                    msgStatus: msgStatus,
                    player: player,
                    rankings: rankings,
                    history: entriesArray
                });
            }).catch(function(err) {
                logger.debug(err);
            });
        }).catch(function(err) {});

    },


    /**
     *     POST - Edits a player's informations
     *     @param  {Object} req - Express's request object. Req contains all the player info that will be updated.
     *     @param  {Object} res - Express's response object. Res redirects to the player edition page.
     *     @return {Void}
     */
    editPlayer: function(req, res) {


        req.session.msgError = [];
        req.session.msgStatus = "";
        var id = req.params.id;
        var firstname = req.body.firstname;
        var lastname = req.body.lastname;
        var street = req.body.street;
        var streetNumber = req.body.streetNumber;
        var box = req.body.box;
        var zipCode = req.body.zipCode;
        var city = req.body.city;
        var gsm = req.body.gsm;
        var birthdate = new Date(req.body.birthdate);
        var email = req.body.email;
        var aftRanking = req.body.aftRanking;

        if (firstname == '') {
            req.session.msgError.push('Prénom');
        }
        if (lastname == '') {
            req.session.msgError.push('Nom');
        }
        if (street == '') {
            req.session.msgError.push('Rue');
        }
        if (streetNumber == '' && req.body.streetNumber < 0) {
            req.session.msgError.push('Numéro de rue');
        }
        if (zipCode == '' && req.body.zipCode < 0) {
            req.session.msgError.push('Code postal');
        }
        if (city == '') {
            req.session.msgError.push('Ville');
        }
        if (gsm == '') {
            req.session.msgError.push('Numéro de téléphone');
        }
        if (birthdate == '') {
            req.session.msgError.push('Date de naissance');
        }
        if (email == '') {
            req.session.msgError.push('Addresse mail');
        }
        if (aftRanking == '') {
            req.session.msgError.push('Classement AFT');
        }

        if (req.session.msgError.length == 0) {

            var id = req.params.id;
            var firstname = req.body.firstname;
            var lastname = req.body.lastname;
            var street = req.body.street;
            var streetNumber = req.body.streetNumber;
            var box = req.body.box;
            var zipCode = req.body.zipCode;
            var city = req.body.city;
            var gsm = req.body.gsm;
            var birthdate = new Date(req.body.birthdate);
            var email = req.body.email;
            var aftRanking = req.body.aftRanking;

            var modifiedFields = [];
            // Logging to modification history
            PlayerPersister.findPlayer(id).then(function(oldPlayer) {

                if (oldPlayer.firstName != firstname) {
                    modifiedFields.push({
                        "name": "firstname",
                        "value": oldPlayer.firstName
                    });
                }

                if (oldPlayer.lastName != lastname) {
                    modifiedFields.push({
                        "name": "lastname",
                        "value": oldPlayer.lastName
                    });
                }

                if (oldPlayer.street != street) {
                    modifiedFields.push({
                        "name": "street",
                        "value": oldPlayer.street
                    });
                }

                if (oldPlayer.streetNumber != streetNumber) {
                    modifiedFields.push({
                        "name": "streetNumber",
                        "value": oldPlayer.streetNumber
                    });
                }

                if (oldPlayer.zipCode != zipCode) {
                    modifiedFields.push({
                        "name": "zipCode",
                        "value": oldPlayer.zipCode
                    });
                }

                if (oldPlayer.city != city) {
                    modifiedFields.push({
                        "name": "city",
                        "value": oldPlayer.city
                    });
                }

                if (oldPlayer.gsm != gsm) {
                    modifiedFields.push({
                        "name": "gsm",
                        "value": oldPlayer.gsm
                    });
                }

                if (oldPlayer.birthdate.toUTCString() != birthdate.toUTCString()) {
                    modifiedFields.push({
                        "name": "birthdate",
                        "value": oldPlayer.birthdate
                    });
                }

                if (oldPlayer.email != email) {
                    modifiedFields.push({
                        "name": "email",
                        "value": oldPlayer.email
                    });
                }

                if (oldPlayer.aftRanking != aftRanking) {
                    modifiedFields.push({
                        "name": "aftRanking",
                        "value": oldPlayer.aftRanking
                    });
                }

                PlayerPersister.editPlayer(id, {
                    //title: title,
                    firstName: firstname,
                    lastName: lastname,
                    street: street,
                    streetNumber: streetNumber,
                    box: box,
                    zipCode: zipCode,
                    city: city,
                    gsm: gsm,
                    birthdate: birthdate.toUTCString(),
                    email: email,
                    aftRanking: aftRanking,
                }).then(function() {
                    // Log changes to history
                    if (modifiedFields.length > 0) {
                        var promises = [];
                        for (var field in modifiedFields) {
                            logger.debug('OLD VALUE : ' + modifiedFields[field].value);
                            var promise = HistoryEntryPersister.createHistoryEntry({
                                date: new Date(),
                                author: req.session.user.name,
                                updatedId: id,
                                updatedCollection: 'Player',
                                updatedField: modifiedFields[field].name,
                                oldValue: modifiedFields[field].value
                            });
                            promises.push(promise);
                        }
                        Promise.all(promises).then(function(argument) {
                            logger.debug("Saved modifications to history.");
                        });
                    };
                    req.session.msgStatus = "le joueur a été modifié avec succès";
                    res.redirect('/staff/edit/' + id);

                }).catch(function(err) {
                    req.session.msgError.push('Il y a eu une erreur, veuillez réessayer.');
                    logger.debug(err);
                });

            }).catch(function(err) {
                req.session.msgError.push('Il y a eu une erreur, veuillez réessayer.');
                logger.debug(err);
            });

        } else {
            var id = req.params.id;
            res.redirect('/staff/edit/' + id);
        }
    },


    /**
     * GET - Shows the pair's management page
     * @param  {Object} req Express's request object. Req contains possible message status or error.
     * @param  {Object} res Express's request object. Res renders the view to manage pairs.
     * @return {Void}
     */
    getPair: function(req, res) {

        var msgStatus = req.msgStatus;
        var errors = req.msgError;

        Promise.props({
            //joueurs: PlayerPersister.findAllPlayers(),
            pairs: PairPersister.findPairs(),
            players: PairPersister.findPlayersNotInPair()
        }).then(function(result) {
            res.render('manage_pair', {
                msgStatus: msgStatus,
                errors: errors,
                //joueurs: result.joueurs,
                pairs: result.pairs,
                players: result.players
            });
        }).catch(function(err) {
            logger.debug(err);
        });
    },

    /**
     * GET - Sort and get pair by payment 
     * @param  {Objet} req Express's request object. Req contains the sort param
     * @param  {Objet} res Express's request object. Res sends a JSON with pairs and a other with players
     * @return {Object} Returns a JSON object containing the pairs sorted by the param query.
     */
    getPairPayment: function(req, res) {

        Promise.props({
            players: PlayerPersister.findAllPlayers(),
            pairs: PairPersister.getPairsby(),
        }).then(function(result) {
            var pairs = [];
            var players = [];
            if (req.params.sortParam === 'nPaid') {
                pairs = _.filter(result.pairs, function(pair) {
                    return pair.payment == null;
                });

                players = _.filter(result.players, function(player) {
                    if (_.contains(pairs.players), player._id) {
                        return player;
                    }
                });
            } else if (req.params.sortParam === 'paid') {
                pairs = _.filter(result.pairs, function(pair) {
                    return pair.payment != null;
                });

                players = _.filter(result.players, function(player) {
                    if (_.contains(pairs.players), player._id) {
                        return player;
                    }
                });
            } else if (req.params.sortParam === 'all') {
                pairs = result.pairs;
                players = result.players;
            }

            res.send({
                pairs: JSON.stringify(pairs),
                players: JSON.stringify(players)
            });
        }).catch(function(err) {
            console.log(err);
        });
    },

    /**
     * POST - edit a pair
     * @param  {Object} req Express's request object. Req contains infos about the pair to be modified.
     * @param  {Object} res Express's request object. Res redirects to the staff pair's section
     * @return {Void}
     */
    editPair: function(req, res) {
        var pairId = req.params.id;
        var payment = req.body.etatpayement;
        var montant = req.body.montant;
        var errors = [];

        var modifiedFields = [];

        if (montant === undefined || montant < 0) {
            errors.push('Montant négatif');
            req.session.msgError = errors;
            res.redirect('/staff/pair');

        } else {
            PairPersister.findPair(pairId).then(function(oldPair) {

                if (oldPair.payment !== payment) {
                    modifiedFields.push({
                        "name": "payment",
                        "value": oldPair.payment
                    });
                }

                if (oldPair.montant !== montant) {
                    modifiedFields.push({
                        "name": "montant",
                        "value": oldPair.montant
                    });
                }

                PairPersister.updatePair(pairId, {
                    payment: payment,
                    montant: montant
                }).then(function() {

                    // Log changes to history
                    if (modifiedFields.length > 0) {
                        var promises = [];
                        for (var field in modifiedFields) {
                            var promise = HistoryEntryPersister.createHistoryEntry({
                                date: new Date(),
                                author: req.session.user.name,
                                updatedId: pairId,
                                updatedCollection: 'Pair',
                                updatedField: modifiedFields[field].name,
                                oldValue: modifiedFields[field].value
                            });
                            promises.push(promise);
                        }
                        Promise.all(promises).then(function(argument) {
                            logger.debug("Saved modifications to history.");
                        });
                    }
                    req.session.msgStatus = "Paire modifiée avec succès";
                    console.log(req.session.msgStatus);
                    res.redirect('/staff/pair');
                }).catch(function(err) {
                    logger.debug(err);
                });

            }).catch(function(err) {
                logger.debug(err);
            });
        }
    },

    /**
     * GET - Delete a pair 
     * @param  {Objet} req Express's request object. Req contains the the pair id to be deleted.
     * @param  {Objet} res Express's request object. Res redirects to the staff's pair section.
     * @return {Void}
     */
    deletePair: function(req, res) {
        var pairId = req.params.id;

        PairPersister.deletePair(pairId).then(function() {
            req.session.msgStatus = "Paire supprimée avec succès";
            res.redirect('/staff/pair');
        })
    },

    /**
     * POST - Create a pair of players
     * @param  {Objet} req Express's request object. Req contains the pairs to be created.
     * @param  {Objet} res Express's request object. Res redirects to the staff pair's section.
     * @return {Void}
     */
    createPair: function(req, res) {

        req.session.msgError = [];

        var player1 = req.body.player1;
        var player2 = req.body.player2;
        var payment = req.body.payment;
        var montant = req.body.montant;

        if (player1 == undefined) {
            req.session.msgError.push('Joueur 1 non défini');
        }

        if (player2 == undefined) {
            req.session.msgError.push('Joueur 2 non défini');
        } else if (player2 == player1) {
            req.session.msgError.push('Joueur 2 identique au Joueur 1');
        };

        if (montant < 0) {
            req.session.msgError.push('montant négatif');
        }

        if (req.session.msgError.length == 0) {

            var players = [player1, player2];
            logger.debug(players);

            PairPersister.createPair({
                players: players,
                payment: payment,
                montant: montant,
            }).then(function() {
                req.session.msgStatus = "Paire créée avec succès";
                res.redirect('/staff/pair');
            }).catch(function(err) {
                logger.debug(err);
            });
        } else {
            res.redirect('/staff/pair');
        }
    },


    /**
     * GET - Shows the list of modification histories
     * @param  {Objet} req Express's request object. not used.
     * @param  {Objet} res Express's request object. Res renders the view.
     */
    getHistory: function(req, res) {

        HistoryEntryPersister.getAllHistoryEntries().then(function(entries) {
            res.render('history-log', {
                history: entries,
            });
        }).catch(function(err) {
            logger.debug(err);
        });

    },

};