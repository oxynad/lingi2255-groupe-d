var nodemailer = require('nodemailer');
var _ = require('underscore');
var logger = require('winston');
var config = require('../../../config');

//init transporter
var transporter = nodemailer.createTransport(config.mail);

/**
 * Some Utils
 *     @module app/api/controllers/utils_controller
 *     @type {Object}
 */
module.exports = {

    /**
     * Send an email to a specific person
     * @param {Array} receivers - array of email addresses (String)
     * @param {String} text - body in plaintext
     * @param {String} textHtml - body as html text
     * @param {String} subject - subject of email
     */
	sendMail : function(receivers, text, textHtml, subject) {
		var receiversList = '';

		_.each(receivers, function (receiver) {
			receiversList += receiver + ',';
		});

		receiversList = receiversList.slice(0,-1);

        logger.debug(receiversList);
        logger.debug(text);
        logger.debug(textHtml);
        logger.debug(subject);

		var mailOptions = {
          from: 'Asmae Tennis <asmaetennis@gmail.com>', // sender address
          to: receiversList, // list of receivers
          subject: subject, // Subject line
          text: text, // plaintext body
          html: textHtml // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(error, info){
          if(error){
            return logger.debug(error);
          }
          logger.debug('Message sent: ' + info.response);
        });
	}

};

