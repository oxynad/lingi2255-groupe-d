var passport = require('./../../passport/loginStrategy');

/**
 *     The application's authentication controller
 *     @module app/api/controllers/auth_controller
 *     @type {Object}
 */
module.exports = {

  /**
   *     POST - login the staff or the admin into the application.
   *     @param  {Object} req - Express's request object. Req contents the user's infos or a possible error message
   *     @param  {Object} res - Express's response object. Res redirects to the login page or to the staff section
   */
  doLogin: function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        req.session.error = "Echec d'authentification";
        return res.redirect('/login');
      }
      req.session.user = user;
      return res.redirect('/staff');
    })(req, res, next)
  },

  /**
   *     GET - show the login page.
   *     @param  {Object} req - Express's request object. Req contains a possible error message.
   *     @param  {Object} res - Express's response object. Res renders to the login page.
   */
  getLogin: function(req, res) {
    if (req.session.error) {
      res.render('login', {message: req.session.error});
      delete req.session.error;
    } else {
      res.render('login');
    }
  },

  /**
   *     GET - logs out the user and redirects him to the homepage.
   *     @param  {Object} req - Express's request object. Req destroy the session
   *     @param  {Object} res - Express's response object. Res redirects to the homepage.
   */
  getLogout: function(req, res) {
    req.session.destroy(function(err) {
      res.redirect('/');
    });
  }
};