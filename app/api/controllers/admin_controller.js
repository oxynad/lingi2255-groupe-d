var _ = require('underscore');
var datalayer = require('./../../datalayer');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var TournamentPersister = datalayer.persisters.TournamentPersister;
var CourtPersister = datalayer.persisters.CourtPersister;
var StaffPersister = datalayer.persisters.StaffPersister;

/**
 *     The admin's section controller.
 *     @module app/api/controllers/admin_controller
 *     @type {Object}
 */
module.exports = {

  /**
   *     GET - Show the admin's section page.
   *     @param  {Object} req - Express's request object. Req contains a possible message status or error
   *     @param  {Object} res - Express's response object. Res renders the admin's view 
   */
  getAdminPage: function(req, res) {
    StaffPersister.findAllStaffs().then(function(staffs) {
      var msgStatus = req.msgStatus;

      res.render('admin_section', {
        msgStatus: msgStatus,
        staffs: staffs
      });
    })
  },

  /**
   *     GET - Show the admin's edition page for a staff member.
   *     @param  {Object} req - Express's request object. Req contains a possible message status or error
   *     @param  {Object} res - Express's response object. Res renders the view to edit a staff member
   */
  getEditStaff: function(req, res) {
    StaffPersister.findStaff(req.params.staffId).then(function(staff) {
      res.render('edit_staff', {
        staff: staff,
        msgStatus: req.msgStatus,
        msgError: req.msgError
      });
    })
  },

  /**
   *     POST - Edit a staff member.
   *     @param  {Object} req - Express's request object. Req contains the staff's the attributes to update and a possible message status or error.
   *     @param  {Object} res - Express's response object. Res redirects to the admin section
   */
  doEditStaff: function(req, res) {
    var name = req.body.name;
    var gsm = req.body.gsm;
    var email = req.body.email;
    var id = req.params.staffId;

    StaffPersister.updateStaff(id, {
      name: name,
      mail: email,
      phoneNumber: gsm
    }).then(function() {
      req.session.msgStatus = 'Membre du staff modifié avec succès.';
      res.redirect('/admin');
    });
  },

  /**
   *     GET - Delete the specified staff member.
   *     @param  {Object} req - Express's request object. Req contains the staff id and a possible message status or error.
   *     @param  {Object} res - Express's response object. Res redirects to the admin section.
   */
  deleteStaff: function(req, res) {
    var staffId = req.params.staffId;

    StaffPersister.deleteStaff(staffId).then(function() {
      req.session.msgStatus = "Membre du staff supprimé avec succès.";
      res.redirect('/admin');
    });

  },


  /**
   *     GET - Show the admin's page to add a staff member.
   *     @param  {Object} req - Express's request object. Not used
   *     @param  {Object} res - Express's response object. Res renders the view to add a new staff member.
   */
  getAddStaff: function(req, res) {

    TournamentPersister.getAllTournaments().then(function(tournaments) {
      CourtPersister.findAllCourts().then(function(courts) {

        var concatenatedCategories = _.map(tournaments, function(tournament) {
          return {
            categoryId: tournament._id,
            categoryName: tournament.category + ' ' + tournament.type
          };
        });

        res.render('admin_addStaff', {
          categories: concatenatedCategories,
          courts: courts
        });
      });
    });
  },

  /**
   *     POST - add a staff member
   *     @param  {Object} req - Express's request object. Req contains the attributes for the new staff member
   *     @param  {Object} res - Express's response object. Res redirects to the admin section
   */
  doAddStaff: function(req, res) {

    var username = req.body.username;
    var name = req.body.first_name + ' ' + req.body.last_name;
    var gsm = req.body.gsm;
    var email = req.body.email;
    var courtId = req.body.terrain;
    var password = req.body.password;
    var categoriesKeys = _.keys(req.body.categories);
    var moderationLevel = "Staff";

    StaffPersister.createStaff({
      username: username,
      name: name,
      mail: email,
      phoneNumber: gsm,
      moderationLevel: moderationLevel,
      password: password
    }).then(function(staff) {

      var promises = [];
      if (categoriesKeys) {
        _.each(categoriesKeys, function(id) {
          promises.push(TournamentPersister.setResponsible(id, staff[0]._id));
        });
      }
      if (courtId) {
        promises.push(CourtPersister.setResponsible(courtId, staff[0]._id));
      }
      Promise.all(promises).then(function() {
        req.session.msgStatus = "Membre du staff ajouté avec succès.";
        res.redirect('/admin');
      });
    });
  }
};