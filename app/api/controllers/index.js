var authController = require('./auth_controller');
var playerController  =require('./player_controller');
var staffController = require('./staff_controller');
var courtController = require('./court_controller');
var paypalController = require('./paypal_controller');
var ownerController = require('./owner_controller');
var tournamentController = require('./tournament_controller');
var adminController = require('./admin_controller');
var utilsController = require('./utils_controller');

module.exports = {
  auth: authController,
  player: playerController,
  staff: staffController,
  court: courtController,
  paypal: paypalController,
  owner: ownerController,
  admin: adminController,
  tournament: tournamentController,
  utils: utilsController
};