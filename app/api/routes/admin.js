var _ = require('underscore');
var routes = require('./../controllers');

/**
 * The application admin's routes
 * @module app/api/routes/admin
 * @type {Object}
 */
module.exports = function(app) {

  /*
  Middleware to check if user is authorized
  */
  function restrict(req, res, next) {
    if (req.session.user && req.session.user.moderationLevel === 'Admin') {
      next();
    } else {
      req.session.error = 'Accès non autorisé! Vous n\'êtes pas un Administrateur';
      res.redirect('/login');
    }
  }

  app.get('/admin', restrict, routes.admin.getAdminPage);
  
  // Add staff
  app.get('/admin/addStaff', restrict,  routes.admin.getAddStaff);

  // Add staff
  app.post('/admin/addStaff', restrict, routes.admin.doAddStaff);

  app.get('/admin/edit/:staffId',restrict,  routes.admin.getEditStaff);
  app.get('/admin/delete/:staffId',restrict,  routes.admin.deleteStaff);


  app.post('/admin/edit/:staffId', restrict, routes.admin.doEditStaff);

};