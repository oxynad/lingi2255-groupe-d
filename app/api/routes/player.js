var _ = require('underscore');
var routes = require('./../controllers');

/**
 * The application player's routes
 * @module app/api/routes/player
 * @type {Object}
 */
module.exports = function(app) {

	/* Displayed when the player has successfully registered */
	app.get('/registered', routes.player.getRegistered)
		.use(function(req, res, next) {
			if (typeof(req.session.msgError1) == 'undefined') {
				req.session.msgError1 = [];
			}
			if (typeof(req.session.msgError2) == 'undefined') {
				req.session.msgError2 = [];
			}
			next();
		});

	/* Display register player form */
	app.get('/register', routes.player.getRegister);
	/* Register the player */
	app.post('/register/add', routes.player.doAdd);
	/* Display reminder to pay when chosen by cash, after registration */
	app.get('/to_pay_reminder', routes.player.getToPayReminder);
	/* Display confirmation registration paid */
	app.get('/payed', routes.player.getPayed);
	/* Confirm email of the player */
	app.get('/register/confirm/:key', routes.player.confirmEmail)
};