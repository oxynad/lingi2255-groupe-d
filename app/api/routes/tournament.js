var routes = require('./../controllers');

var _ = require('underscore');
var datalayer = require('./../../datalayer');
var TournamentPersister = datalayer.persisters.TournamentPersister;

/**
 * The application tournament's routes
 * @module app/api/routes/tournament
 * @type {Object}
 */
module.exports = function(app) {

  /*
    Middleware to check if user is authorized
    */
  function restrictStaff(req, res, next) {
    if (req.session.user) {
      next();
    } else {
      req.session.error = 'Accès non autorisé! Vous n\'êtes pas un Administrateur ou un membre du Staff';
      res.redirect('/login');
    }
  }

   /*
  Middleware to check if user is authorized
  */
  function restrictAdmin(req, res, next) {
    if (req.session.user && req.session.user.moderationLevel === 'Admin') {
      next();
    } else {
      req.session.error = 'Accès non autorisé! Vous n\'êtes pas un Administrateur';
      res.redirect('/login');
    }
  }

  /*
   Middleware to check if tournament is in Conception
   */
  function checkIfConception(req, res, next) {

    TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
      if (tournament.status !== 'Conception') {
        req.session.msgError = "Le tournoi est a déja commencé, vous ne pouvez plus ajouter ou modifier les groupes.";
        res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
      } else {
        next();
      }
    });
  }

  /*
  Middleware to check if tournament is in Poule
  */
  function checkIfPoule(req, res, next) {

    TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
      if (tournament.status !== 'Poule') {
        req.session.msgError = "Le tournoi n'a pas encore commencé, vous ne pouvez donc pas encore encoder de résultats.";
        res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
      } else {
        next();
      }
    });
  }

  /*
   Middleware to check if tournament is rdy for Elimination
   */
  function checkRdyForElimination(req, res, next) {
    TournamentPersister.getTournament(req.params.tournamentId).then(function(tournament) {
      if (tournament.status === 'Conception') {
        req.session.msgError = "Le tournoi n'a pas encore commencé.  Vous ne pouvez donc pas encore entammer les knockoff.";
        res.redirect('/staff/tournaments/edit/' + req.params.tournamentId);
      } else {
        next();
      }
    })
  }

  app.get('/staff/tournaments', restrictStaff, routes.tournament.manageTournaments);
  app.get('/staff/tournaments/new', restrictAdmin, routes.tournament.newTournament);
  app.post('/staff/tournaments/create', restrictAdmin, routes.tournament.createTournament);
  app.get('/staff/tournaments/create-tdf', restrictAdmin, routes.tournament.createFamilyTournament);
  app.get('/staff/tournaments/edit/:tournamentId', restrictStaff, routes.tournament.editTournament);
  app.get('/staff/tournaments/del/:tournamentId', restrictAdmin,  routes.tournament.delTournament);

  app.get('/staff/tournaments/edit/:tournamentId/group/new', restrictStaff, checkIfConception, routes.tournament.newGroup);
  app.get('/staff/tournaments/edit/:tournamentId/group/delete/:groupId', restrictStaff, checkIfConception, routes.tournament.delGroup);
  app.post('/staff/tournaments/edit/:tournamentId/group/create/:groupParams', restrictStaff, routes.tournament.createGroup);
  app.get('/staff/tournaments/edit/:tournamentId/group/edit/:groupId', restrictStaff, checkIfConception, routes.tournament.editGroup);
  app.post('/staff/tournaments/edit/:tournamentId/group/update/:groupParams', restrictStaff, routes.tournament.updateGroup);
  app.get('/staff/tournaments/edit/:tournamentId/pairsAvailable', restrictStaff, routes.tournament.getPairsAvailable);
  app.get('/staff/tournaments/edit/:tournamentId/courtsAvailable', restrictStaff, routes.tournament.getCourtsAvailable);
  app.get('/staff/tournaments/edit/:tournamentId/group/groupInfo/:groupId', restrictStaff, routes.tournament.getGroupeInfo);
  app.post('/staff/tournaments/edit/:tournamentId/group/:groupId/sendMail', restrictStaff, routes.tournament.sendMailToGroup);

  app.get('/staff/tournaments/edit/:tournamentId/group/encode/:groupId', restrictStaff, checkIfPoule, routes.tournament.getEncodeResults);
  app.get('/staff/tournaments/edit/:tournamentId/group/encode/:groupId/getValidOpponents/:pairId', restrictStaff, routes.tournament.getValidOpponents);
  app.post('/staff/tournaments/edit/:tournamentId/group/encode/:groupId/', restrictStaff, routes.tournament.doEncodeResults);
  app.post('/staff/tournaments/edit/:tournamentId/group/encode/:groupId/delete-match/:matchId', restrictStaff, routes.tournament.doDeleteMatch);

  app.get('/staff/tournaments/edit/:tournamentId/change/conception', restrictStaff, routes.tournament.setConception);
  app.get('/staff/tournaments/edit/:tournamentId/change/poule', restrictStaff, routes.tournament.setPoule);

  app.get('/staff/tournaments/edit/:tournamentId/elimination', restrictStaff, checkRdyForElimination, routes.tournament.show_elimination);
  app.post('/staff/tournaments/edit/:tournamentId/elimination', restrictStaff, routes.tournament.encode_elimination);

  app.get('/staff/tournaments/edit/:tournamentId/group/view/:groupId/', restrictStaff, routes.tournament.viewGroupPrintable);

  app.get('/staff/tournaments/rain', restrictStaff, routes.tournament.getRain);
  app.get('/staff/tournaments/rainmode', restrictStaff, routes.tournament.getRainMode);
  app.get('/staff/tournaments/manage_rainmode', restrictStaff, routes.tournament.getManageRain);
  app.post('/staff/tournaments/manage_rainmode/:tournamentId/', restrictStaff, routes.tournament.doUniteGroups);

  app.get('/staff/tournaments/manage_rainmode/reassign', restrictStaff, routes.tournament.getManageRainReassign);
  app.get('/staff/tournaments/manage_rainmode/reassign/done', restrictStaff, routes.tournament.getDoneReassign);
  
  app.post('/staff/tournaments/manage_rainmode/reassign/:tournamentId/:groupId', restrictStaff, routes.tournament.reassignGroupCourt);  

};