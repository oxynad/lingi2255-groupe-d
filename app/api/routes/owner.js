var _ = require('underscore');
var routes = require('./../controllers');

/**
 * The application owner's routes
 * @module app/api/routes/owner
 * @type {Object}
 */
module.exports = function(app) {

	app.use(function(req, res, next) {
		if (typeof(req.session.msgError) == 'undefined') {
			req.session.msgError = [];
		}
		next();
	});

	// Owner's registration
	app.get('/register-owner', routes.owner.getRegister);

	// Confirmation
	app.get('/register-owner/done', routes.owner.getRegistered);

	// Add new owner
	app.post('/register-owner/add', routes.owner.doAdd);
};