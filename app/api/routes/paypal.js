var paypal = require('paypal-rest-sdk');
var routes = require('./../controllers');
var config = {};

/**
 * The application paypal's routes
 * @module app/api/routes/paypal
 * @type {Object}
 */
module.exports = function(app, c) {
  config = c;
  paypal.configure(c.api);

  app.get('/payment-create/:pairId', routes.paypal.create);
  app.get('/payment-execute', routes.paypal.execute);
  app.get('/payment-cancel', routes.paypal.cancel);
};

