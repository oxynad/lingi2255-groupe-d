var routes = require('./../controllers');

/**
 * The application staff's routes
 * @module app/api/routes/staff
 * @type {Object}
 */
module.exports = function(app) {

  /*
    Middleware to check if user is authorized
    */
    function restrict(req, res, next) {
      if (req.session.user) {
        next();
      } else {
        req.session.error = 'Accès non autorisé! Vous n\'êtes pas un membre du Staff';
        res.redirect('/login');
      }
    }

  app.get('/staff', restrict, routes.staff.getStaff);

  app.get('/staff/search/', restrict, routes.staff.getSearch);
  app.get('/staff/search/:criteria', restrict, routes.staff.getSearchWithCriteria);

  app.get('/staff/extras', restrict, routes.staff.getManageExtras);
  app.post('/staff/extras/create', restrict, routes.staff.createExtra);
  app.post('/staff/extras/edit/:id', restrict, routes.staff.editExtra);
  app.get('/staff/extras/del/:id', restrict, routes.staff.deleteExtra);

  app.get('/staff/del/:id', restrict, routes.staff.deletePlayer);
  app.get('/staff/edit/:id', restrict, routes.staff.getManagePlayer);
  app.post('/staff/edit/:id', restrict, routes.staff.editPlayer);

  app.get('/staff/pair', restrict, routes.staff.getPair);
  app.post('/staff/pair/edit/:id', restrict, routes.staff.editPair);
  app.post('/staff/pair/create', restrict, routes.staff.createPair);
  app.get('/staff/pair/del/:id', restrict, routes.staff.deletePair);
  app.get('/staff/pair/sort/:sortParam', restrict, routes.staff.getPairPayment);

  app.get('/staff/history', restrict, routes.staff.getHistory);

};