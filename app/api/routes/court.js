var _ = require('underscore');
var routes = require('./../controllers');

/**
 * The application court's routes
 * @module app/api/routes/court
 * @type {Object}
 */
module.exports = function(app) {

	/*
	Middleware to check if user is authorized
	*/
	function restrict(req, res, next) {
		if (req.session.user) {
			next();
		} else {
			req.session.error = 'Accès non autorisé! Vous n\'êtes pas un Administrateur ou un membre du Staff';
			res.redirect('/login');
		}
	}

  app.get('/courts_list',restrict, routes.court.getCourtsList); //add restrict after the dev
  app.get('/courts_list/search/:criteria',restrict, routes.court.getSearchWithCriteria);
  app.get('/courts_list/search/',restrict, routes.court.getSearchWithoutCriteria);
  app.get('/court/:courtId',restrict, routes.court.getCourtById);
  
  /* Edit a court (Get form + save Modifications) : */
  app.get('/court/edit/:courtId', restrict, routes.court.getManageCourt);
  app.post('/court/edit/:courtId', restrict, routes.court.editCourt);
  app.post('/court/edition/:courtId', restrict, routes.court.editCourtEdition);
  app.post('/court/addEdition/:courtId', restrict, routes.court.addEdition);
  app.post('/court/:courtId/postComment',restrict, routes.court.postComment);

  // TODO app.post('/court/:courtId/postEdition',restrict, routes.court.postEdition);
};