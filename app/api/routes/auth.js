var _ = require('underscore');
var routes = require('./../controllers');

/**
 * The application auth's routes
 * @module app/api/routes/auth
 * @type {Object}
 */
module.exports = function(app) {


  /*
    Login to ASMAE staff section.
   */
  app.post('/login', routes.auth.doLogin);

  /*
    Route to display the login section
   */
  app.get('/login', routes.auth.getLogin);

  /*
    Logout of the staff section
   */
  app.get('/logout', routes.auth.getLogout);

}