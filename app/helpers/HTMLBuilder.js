/*
<!-- Original:  Tom McComb (mrtuba@tampabay.rr.com) -->
<!-- Web Site:  http://home.tampabay.rr.com/mrtuba -->
*/

var _ = require("underscore");
var Promise = require("bluebird");
var PairPersister = require("../datalayer/persistence/PairPersister");
var logger = require('winston');

var fct = function GeneratePlayoffs(TeamCount, Courts, Games, IsFullDB) {
    var TeamSet = new Array();
    var GameSet = new Array();
    var MaxLevel = 1;
    var OldTeam;
    var nextGame = 1;
    var Parent = new Array(4);
    var ParentNum;
    TeamSet[1] = 1;
    if (TeamCount < 2) {
        return new Promise(function(resolve) {
            resolve("You cannot have playoffs without at least two teams.");
        });
    }
    MaxLevel++;
    var HalfCount = 3;
    for (var ThisTeam = 2; ThisTeam <= TeamCount; ThisTeam++) {
        if (ThisTeam == HalfCount) {
            HalfCount = (2 * HalfCount) - 1;
            MaxLevel++;
        }
        OldTeam = HalfCount - ThisTeam;
        if (ThisTeam == 2) {
            TeamSet[2] = 1;
            GameSet[1] = newGame(1, 2, true, true);
        }
        else {
            ParentNum = TeamSet[OldTeam];
            Parent = GameSet[ParentNum];
            if (Parent[1] == OldTeam) {
                GameSet[ParentNum] = newGame(nextGame, Parent[2], false, Parent[4]);
            }
            else {
                GameSet[ParentNum] = newGame(Parent[1], nextGame, Parent[3], false);
            }
            GameSet[nextGame] = newGame(OldTeam, ThisTeam, true, true);
        }
        TeamSet[ThisTeam] = nextGame;
        TeamSet[OldTeam] = nextGame;
        nextGame++;
    }

    //logger.debug(GameSet);
    var doc = {str : ""};
    doc.str += "<main id='tournament'>";
    if (typeof Games !== 'undefined') {
        return attachDBInfo(GameSet, Games).then(function() {
            return new Promise(function(resolve) {
                constructViaBinaryTree(GameSet, MaxLevel, doc, Courts, IsFullDB);
                doc.str += ("</main>");
                resolve(doc.str);
            });
        });
    } else {
        return new Promise(function(resolve) {
            constructViaBinaryTree(GameSet, MaxLevel, doc, Courts, IsFullDB);
            doc.str += ("</main>");
            resolve(doc.str);
        });
    }
};

function attachDBInfo(GameSet, Games) {
    var allPromises = _.map(GameSet, function(Game, Index) {
        if (!Game) return;
        var gameHistory = _.find(Games, function(game) {
            return game.id == Index;
        });

        if (!gameHistory) return;

        var promises = [];

        var obj = {
            fighterA: {},
            fighterB: {},
            winner: undefined
        };

        obj.fighterA.generatorId = gameHistory.fighterA.generatorId;
        obj.fighterA.score = gameHistory.fighterA.score;
        if (gameHistory.fighterA.type == 'Pair')
            promises.push(PairPersister.findPair(gameHistory.fighterA.id).then(function(team){
                return team.populate('players').execPopulate();
            }).then(function(team) {
                //logger.debug("FIGHTER A " + team);
                obj.fighterA.pair = team;
            }));

        obj.fighterB.generatorId = gameHistory.fighterB.generatorId;
        obj.fighterB.score = gameHistory.fighterB.score;
        if (gameHistory.fighterB.type == 'Pair')
            promises.push(PairPersister.findPair(gameHistory.fighterB.id).then(function(team){
                return team.populate('players').execPopulate();
            }).then(function(team) {
                //logger.debug("FIGHTER B " + team);
                obj.fighterB.pair = team;
            }));

        if (gameHistory.winner)
            promises.push(PairPersister.findPair(gameHistory.winner).then(function(team){
                return team.populate('players').execPopulate();
            }).then(function(team) {
                //logger.debug("WINNER " + team);
                obj.winner = team;
            }));

        if (gameHistory.court)
            obj.court = gameHistory.court;

        return Promise.all(promises).then(function() {
            //logger.debug("ASSIGN ");
            //logger.debug(obj);
            //logger.debug(Game);
            Game[5] = obj;
        });
    });

    return Promise.all(allPromises);
}

function getLeft(GameSet, Pointer) {
    return GameSet[Pointer][1];
}

function isLeft(GameSet, Pointer) {
    return !GameSet[Pointer][3];
}

function getRight(GameSet, Pointer) {
    return GameSet[Pointer][2];
}

function isRight(GameSet, Pointer) {
    return !GameSet[Pointer][4];
}

function fillEmpty(DesiredLevel, CurrentLevel, doc) {
    //logger.debug("FILL EMPTY ---");
    var LevelToFill = CurrentLevel - DesiredLevel;
    var LoopSize = Math.pow(2, LevelToFill);
    for(var i = 0 ; i < LoopSize ; i++) {
        //logger.debug("PRINTING EMPTY ONE GAME");
        doc.str += '<li class="spacer">&nbsp;</li>';
        doc.str += '<li class="game game-top">&nbsp;</li>';
        doc.str += '<li class="game game-spacer">&nbsp;</li>';
        doc.str += '<li class="game game-bottom">&nbsp;</li>';
    }
}

function getRealPair(GameSet, CurrentPointer, isPairLookedIsA) {
    var cur;
    if (isPairLookedIsA) {
        cur = GameSet[CurrentPointer][5].fighterA;
        if (typeof cur.pair === 'undefined') {
            return GameSet[cur.generatorId][5].winner;
        }
        return cur.pair;
    } else {
        cur = GameSet[CurrentPointer][5].fighterB;
        if (typeof cur.pair === 'undefined') {
            return GameSet[cur.generatorId][5].winner;
        }
        return cur.pair;
    }
}

function drawLevel(GameSet, CurrentPointer, DesiredLevel, CurrentLevel, doc, Courts, IsFullDB) {
    //logger.debug(GameSet[CurrentPointer]);
    //logger.debug(DesiredLevel + "  " + CurrentLevel);

    if (isLeft(GameSet, CurrentPointer)) {
        if (DesiredLevel < CurrentLevel) {
            //logger.debug("GOING LEFT");
            drawLevel(GameSet, getLeft(GameSet, CurrentPointer), DesiredLevel, CurrentLevel - 1, doc, Courts, IsFullDB);
        }
    } else if (DesiredLevel < CurrentLevel) {
        fillEmpty(DesiredLevel, CurrentLevel - 1, doc);
    }

    if (DesiredLevel == CurrentLevel) {
        //logger.debug("PRINTING GAME");
        doc.str += '<li class="spacer">&nbsp;</li>';
        doc.str += '<li class="game game-top ';
        var teamId;
        var strToAdd;
        var strPoint;
        var fA, fB;
        if (!isLeft(GameSet, CurrentPointer)) {
            teamId = getLeft(GameSet, CurrentPointer);
            strToAdd = 'Team #' + teamId;
            if (GameSet[CurrentPointer][5] && IsFullDB) {
                strPoint = "points[g_"+CurrentPointer+"][f_"+teamId+"]";
                if (GameSet[CurrentPointer][5].fighterA.generatorId == teamId) {
                    strToAdd = GameSet[CurrentPointer][5].fighterA.pair.players[0].lastName
                        + " - " + GameSet[CurrentPointer][5].fighterA.pair.players[1].lastName;
                } else if (GameSet[CurrentPointer][5].fighterB.generatorId == teamId) {
                    strToAdd = GameSet[CurrentPointer][5].fighterB.pair.players[0].lastName
                        + " - " + GameSet[CurrentPointer][5].fighterB.pair.players[1].lastName;
                }

                if (typeof GameSet[CurrentPointer][5].winner !== 'undefined') {
                    strToAdd += "<span>";
                    if (GameSet[CurrentPointer][5].fighterA.generatorId == teamId) {
                        _.each(GameSet[CurrentPointer][5].fighterA.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fA = getRealPair(GameSet, CurrentPointer, true);
                        if (typeof fA !== 'undefined' && fA._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    } else if (GameSet[CurrentPointer][5].fighterB.generatorId == teamId) {
                        _.each(GameSet[CurrentPointer][5].fighterB.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fB = getRealPair(GameSet, CurrentPointer, false);
                        if (typeof fB !== 'undefined' && fB._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    }
                    strToAdd += "</span>";
                } else if (!isRight(GameSet, CurrentPointer) || typeof GameSet[getRight(GameSet, CurrentPointer)][5].winner !== 'undefined') {
                    strToAdd += "<span><div class='input-field'><input type='text' name=\""+strPoint+"\" /></span>";
                }
            }
            doc.str += '">' + strToAdd;
        } else {
            var leftGameId = getLeft(GameSet, CurrentPointer);
            if (GameSet[leftGameId][5] && IsFullDB && (typeof GameSet[leftGameId][5].winner !== 'undefined')) {
                strToAdd = GameSet[leftGameId][5].winner.players[0].lastName
                    + " - " + GameSet[leftGameId][5].winner.players[1].lastName;

                strPoint = "points[g_"+CurrentPointer+"][f_"+leftGameId+"]";

                if (typeof GameSet[CurrentPointer][5].winner !== 'undefined') {
                    strToAdd += "<span>";
                    if (GameSet[CurrentPointer][5].fighterA.generatorId == leftGameId) {
                        _.each(GameSet[CurrentPointer][5].fighterA.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fA = getRealPair(GameSet, CurrentPointer, true);
                        if (typeof fA !== 'undefined' && fA._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    } else if (GameSet[CurrentPointer][5].fighterB.generatorId == leftGameId) {
                        _.each(GameSet[CurrentPointer][5].fighterB.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fB = getRealPair(GameSet, CurrentPointer, false);
                        if (typeof fB !== 'undefined' && fB._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    }
                    strToAdd += "</span>";
                } else if (!isRight(GameSet, CurrentPointer) || typeof GameSet[getRight(GameSet, CurrentPointer)][5].winner !== 'undefined') {
                    strToAdd += "<span><div class='input-field'><input type='text' name=\""+strPoint+"\" /></span>";
                }

                doc.str += '">' + strToAdd;
            } else {
                doc.str += '">' + '&nbsp;';
            }
        }
        doc.str += '</li>';
        doc.str += '<li class="game game-spacer">';
        if (GameSet[CurrentPointer][5] && IsFullDB) {
            _.each(Courts, function (court) {
                if (GameSet[CurrentPointer][5].court.equals(court._id)) {
                    doc.str += court.courtAddress;
                }
            });
            //doc.str += '&nbsp;';
        } else if (GameSet[CurrentPointer][5] && !IsFullDB) {
            doc.str += '<select name="courts[g_'+ CurrentPointer +']" class="browser-default">';
            if (typeof GameSet[CurrentPointer][5].court === 'undefined' || GameSet[CurrentPointer][5].court == null) {
                doc.str += '<option value="" disabled="" selected="">Choose the court</option>';
                _.each(Courts, function (court) {
                    doc.str += '<option value="' + court._id + '">' + court.courtAddress + '</option>';
                });
            } else if (typeof GameSet[CurrentPointer][5].court !== 'undefined' && GameSet[CurrentPointer][5].court != null) {
                _.each(Courts, function (court) {
                    if (GameSet[CurrentPointer][5].court.equals(court._id)) {
                        doc.str += '<option value="' + court._id + '" selected="">' + court.courtAddress + '</option>';
                    } else {
                        doc.str += '<option value="' + court._id + '">' + court.courtAddress + '</option>';
                    }
                });
            }
            doc.str += '</select>';
        }
        doc.str += '</li>';
        doc.str += '<li class="game game-bottom ';
        if (!isRight(GameSet, CurrentPointer)) {
            teamId = getRight(GameSet, CurrentPointer);
            strToAdd = 'Team #' + teamId;
            if (GameSet[CurrentPointer][5] && IsFullDB) {
                strPoint = "points[g_"+CurrentPointer+"][f_"+teamId+"]";
                //logger.debug(teamId + " " + CurrentPointer);
                //logger.debug(GameSet[CurrentPointer][5]);
                if (GameSet[CurrentPointer][5].fighterA.generatorId == teamId) {
                    strToAdd = GameSet[CurrentPointer][5].fighterA.pair.players[0].lastName
                        + " - " + GameSet[CurrentPointer][5].fighterA.pair.players[1].lastName;
                } else if (GameSet[CurrentPointer][5].fighterB.generatorId == teamId) {
                    strToAdd = GameSet[CurrentPointer][5].fighterB.pair.players[0].lastName
                        + " - " + GameSet[CurrentPointer][5].fighterB.pair.players[1].lastName;
                }

                if (typeof GameSet[CurrentPointer][5].winner !== 'undefined') {
                    strToAdd += "<span>";
                    if (GameSet[CurrentPointer][5].fighterA.generatorId == teamId) {
                        _.each(GameSet[CurrentPointer][5].fighterA.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fA = getRealPair(GameSet, CurrentPointer, true);
                        if (typeof fA !== 'undefined' && fA._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    } else if (GameSet[CurrentPointer][5].fighterB.generatorId == teamId) {
                        _.each(GameSet[CurrentPointer][5].fighterB.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fB = getRealPair(GameSet, CurrentPointer, false);
                        if (typeof fB !== 'undefined' && fB._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    }
                    strToAdd += "</span>";
                } else if (!isLeft(GameSet, CurrentPointer) || typeof GameSet[getLeft(GameSet, CurrentPointer)][5].winner !== 'undefined') {
                    strToAdd += "<span><div class='input-field'><input type='text' name=\""+strPoint+"\" /></span>";
                }
            }
            doc.str += '">' + strToAdd;
        } else {
            var rightGameId = getRight(GameSet, CurrentPointer);
            if (GameSet[rightGameId][5] && IsFullDB && typeof GameSet[rightGameId][5].winner !== 'undefined') {
                strToAdd = GameSet[rightGameId][5].winner.players[0].lastName
                    + " - " + GameSet[rightGameId][5].winner.players[1].lastName;

                strPoint = "points[g_"+CurrentPointer+"][f_"+rightGameId+"]";

                if (typeof GameSet[CurrentPointer][5].winner !== 'undefined') {
                    strToAdd += "<span>";
                    if (GameSet[CurrentPointer][5].fighterA.generatorId == rightGameId) {
                        _.each(GameSet[CurrentPointer][5].fighterA.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fA = getRealPair(GameSet, CurrentPointer, true);
                        if (typeof fA !== 'undefined' && fA._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    } else if (GameSet[CurrentPointer][5].fighterB.generatorId == rightGameId) {
                        _.each(GameSet[CurrentPointer][5].fighterB.score, function(score) {
                            strToAdd += score + " ";
                        });
                        fB = getRealPair(GameSet, CurrentPointer, false);
                        if (typeof fB !== 'undefined' && fB._id.equals(GameSet[CurrentPointer][5].winner._id)) {
                            doc.str += "winner";
                        }
                    }
                    strToAdd += "</span>";
                } else if (!isLeft(GameSet, CurrentPointer) || typeof GameSet[getLeft(GameSet, CurrentPointer)][5].winner !== 'undefined') {
                    strToAdd += "<span><div class='input-field'><input type='text' name=\""+strPoint+"\" /></span>";
                }

                doc.str += '">' + strToAdd;
            } else {
                doc.str += '">' + '&nbsp;';
            }
        }
        doc.str += '</li>';
    }

    if (isRight(GameSet, CurrentPointer)) {
        if (DesiredLevel < CurrentLevel) {
            //logger.debug("GOING RIGHT");
            drawLevel(GameSet, getRight(GameSet, CurrentPointer), DesiredLevel, CurrentLevel - 1, doc, Courts, IsFullDB);
        }
    } else if (DesiredLevel < CurrentLevel) {
        fillEmpty(DesiredLevel, CurrentLevel - 1, doc);
    }
    //logger.debug("GOING UP");
}

function constructViaBinaryTree(GameSet, MaxLevel, doc, Courts, IsFullDB) {
    for(var CurrentLevel = 1 ; CurrentLevel < MaxLevel ; CurrentLevel++ ) {
        //logger.debug("DRAW START --- " + CurrentLevel + " --- " + (MaxLevel - 1));
        doc.str += '<ul class="round round-'+CurrentLevel+'">';
        drawLevel(GameSet, 1, CurrentLevel, MaxLevel - 1, doc, Courts, IsFullDB);
        doc.str += '<li class="spacer">&nbsp;</li>';
        doc.str += '</ul>';
    }
}

function newGame(First, Second, FirstIsTeam, SecondIsTeam) {
    var Game = new Array(4);
    Game[1] = First;
    Game[2] = Second;
    Game[3] = FirstIsTeam;
    Game[4] = SecondIsTeam;
    return Game;
}

module.exports = {
    promiseBuildBracket : fct
};