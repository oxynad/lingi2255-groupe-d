/*
 <!-- Original:  Tom McComb (mrtuba@tampabay.rr.com) -->
 <!-- Web Site:  http://home.tampabay.rr.com/mrtuba -->

 <!-- This script and many more are available free online at -->
 <!-- The JavaScript Source!! http://javascript.internet.com -->
 */

var fct = function GeneratePlayoffs(TeamCount) {
    var TeamSet = new Array();
    var GameSet = new Array();
    var MaxLevel = 1;
    var OldTeam;
    var nextGame = 1;
    var Parent = new Array(4);
    var ParentNum;
    TeamSet[1] = 1;
    if (TeamCount < 2) {
        return new Promise(function(resolve) {
            resolve("You cannot have playoffs without at least two teams.");
        });
    }
    MaxLevel++;
    var HalfCount = 3;
    for (var ThisTeam = 2; ThisTeam <= TeamCount; ThisTeam++) {
        if (ThisTeam == HalfCount) {
            HalfCount = (2 * HalfCount) - 1;
            MaxLevel++;
        }
        OldTeam = HalfCount - ThisTeam;
        if (ThisTeam == 2) {
            TeamSet[2] = 1;
            GameSet[1] = newGame(1, 2, true, true);
        }
        else {
            ParentNum = TeamSet[OldTeam];
            Parent = GameSet[ParentNum];
            if (Parent[1] == OldTeam) {
                GameSet[ParentNum] = newGame(nextGame, Parent[2], false, Parent[4]);
            }
            else {
                GameSet[ParentNum] = newGame(Parent[1], nextGame, Parent[3], false);
            }
            GameSet[nextGame] = newGame(OldTeam, ThisTeam, true, true);
        }
        TeamSet[ThisTeam] = nextGame;
        TeamSet[OldTeam] = nextGame;
        nextGame++;
    }

    return GameSet;
};

function newGame(First, Second, FirstIsTeam, SecondIsTeam) {
    var Game = new Array(4);
    Game[1] = First;
    Game[2] = Second;
    Game[3] = FirstIsTeam;
    Game[4] = SecondIsTeam;
    return Game;
}

module.exports = fct;