var request = require('request');
var logger = require('winston');

module.exports = {

 	getLatitudeLongitude : function (address) {

 		return new Promise(function (fullfill, reject) {
			var url = encodeURI('http://maps.googleapis.com/maps/api/geocode/json?address=' + address + 'Belgique');
			logger.debug('request : '+url);
			var result = request(url, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					if (JSON.parse(body).status === 'ZERO_RESULTS') {
						// No locations match the given address
						reject(error);
					} else {
			    		var result = JSON.parse(body).results[0];
			    		fullfill(result.geometry.location);						
					}
				}
				reject(error);
 			}); 
		});
	}
}