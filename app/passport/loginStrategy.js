var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy;
var logger = require('winston');
var datalayer = require('../datalayer');
var StaffPersister = datalayer.persisters.StaffPersister;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  StaffPersister.findStaff(id).then(function(staff) {
    done(err, staff);
  })
});


passport.use(new LocalStrategy(
  function(username, password, done) {
    StaffPersister.staffLogin(username, password).then(function(user) {
      if (!user) {
        return done(null, false, {
          message: 'Incorrect Login.'
        });
      } else {
        return done(null, user);
      }
    }).catch(function(err) {
      logger.debug('Error');

      return done(err);
    })

  }
));

module.exports = passport;