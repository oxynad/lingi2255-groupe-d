var Promise = require('bluebird');
var mongoose = require('mongoose');
var Pair = mongoose.model('Pair');
var Player = mongoose.model('Player');
var _ = require('underscore');
var PlayerPersister = require('./PlayerPersister');
var Extra = require('../models/Extra');
var ExtraPersister = require('./ExtraPersister');
var logger = require('winston');

/**
 * The Pair persistence class
 * @module app/datalayer/persistence/PairPersister
 * @type {Object}
 */
module.exports = {

    /**
     * Create a new pair
     * @param {Object} attrs - specific attributes of the newly created pair
     * @returns {Promise} Promise that this pair has been created and saved
     */
    createPair: function(attrs) {
        return Pair(attrs).saveAsync();
    },

    /**
     * Find all existent pairs
     * @returns {Promise} Promise giving you a hold on an array of pairs
     */
    findAllPairs: function() {
        return Pair.findAsync({});
    },

    /**
     * Retrieve all pairs and the players's infos
     * @return {Promise} Promise giving you a hold on an array of pairs
     */
    findPairs: function() {
        return Pair.find({}).populate('players');
    },

    /**
     * Find specific pair
     * @param {ObjectId|String} pairId - the id of the pair to retrieve
     * @returns {Promise} Promise giving you a hold on the specific pair
     */
    findPair: function(pairId) {
        return Pair.findOneAsync({
            _id: pairId
        });
    },

    /**
     * Find pairs and sort them
     * @returns {Promise} Promise giving you a hold on an array of pairs sorted
     */
    getPairsby: function() {
        return Pair.find({}).sort({
            "payment": -1
        }).populate('players');
    },

    /**
     * Delete a specific pair
     * @param {ObjectId|String} pairId - the id of the pair to delete
     * @returns {Promise} Promise that this specific pair is deleted
     */
    deletePair: function(pairId) {
        return Pair.findByIdAsync(pairId).then(function(doc) {
            return doc.removeAsync();
        });
    },

    /**
     * Update a specific pair
     * @param {ObjectId|String} pairId - the id of the specific pair
     * @param {Object} attrsToUpdate - the attributes to update
     * @returns {Promise} Promise that this pair has been updated and saved
     */
    updatePair: function(pairId, attrsToUpdate) {
        return Pair.findByIdAndUpdateAsync(pairId, attrsToUpdate);
    },

    /**
     * Find all extras of a specific pair
     * @param {ObjectId|String} pairId - the id of the pair
     * @returns {Promise} Promise giving u a hold on the array of extras the pair has subscribed for
     */
    findExtras: function(pairId) {
        Pair.findOneAsync({
            _id: pairId
        }).then(function(pair) {
            var result = [];
            _.each(pair.extra, function(extra) {
                result.push(Extra.findByIdAsync(extra.type));
            });
            return Promise.all(result);
        });
    },

    /**
     * Find all the players not in a pair.
     * @returns {Promise} Promise with the players not in a pair.
     */
    findPlayersNotInPair: function() {

        return new Promise(function(resolve) {
            module.exports.findPairs().then(function(pairs) {
                var playerInPairs = _.flatten(_.map(pairs, function(pair) {
                    return pair.players;
                }));

                Player.find({
                    _id: {
                        "$nin": playerInPairs
                    }
                }).exec(function(err, players) {
                    resolve(players);
                });
            })
        });
    },

    /**
     * Populate the doc to add some statics pairs (mainly for testing & developping purposes)
     * @returns {Promise} Promise that a set of pairs has been added
     */
    populate: function() {

        var that = this;
        return PlayerPersister.findAllPlayers().then(function(players) {

            var promises = [];

            // Paire de femmes
            promises.push(that.createPair({
                players: [players[0]._id, players[1]._id],
                comment: 'Jouer sur Bruxelles si possible'
            }));
            // Paire d'hommes
            promises.push(that.createPair({
                players: [players[2]._id, players[3]._id],
            }));

            // Paire d'hommes
            promises.push(that.createPair({
                players: [players[4]._id, players[5]._id]
            }));
            // Paire de femmes
            promises.push(that.createPair({
                players: [players[6]._id, players[7]._id]
            }));
            // Paire d'hommes
            promises.push(that.createPair({
                players: [players[8]._id, players[9]._id],
                comment: 'On aimerait pouvoir jouer dans l\'équipe d\'Igor et Grégory'

            }));
            // Paire d'hommes
            promises.push(that.createPair({
                players: [players[10]._id, players[11]._id]
            }));

            // TDF
            promises.push(that.createPair({
                players: [players[12]._id, players[13]._id]
            }));

            //TDF
            promises.push(that.createPair({
                players: [players[14]._id, players[15]._id]
            }));

            //TDF
            promises.push(that.createPair({
                players: [players[16]._id, players[17]._id]
            }));


            return Promise.all(promises);
        })
    }
};