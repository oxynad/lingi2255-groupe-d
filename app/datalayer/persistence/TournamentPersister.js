var Promise = require('bluebird');
var mongoose = Promise.promisifyAll(require("mongoose"));
var Tournament = mongoose.model('Tournament');
var Court = mongoose.model('Court');
var Pair = mongoose.model('Pair');
var PairPersister = require('./PairPersister');
var CourtPersister = require('./CourtPersister');
var StaffPersister = require('./StaffPersister');
var _ = require('underscore');
var GroupGameHistory = mongoose.model('GroupGameHistory');
var Group = mongoose.model('Group');
var Player = mongoose.model('Player');
var async = require('async');
var logger = require('winston');
var moment = require('moment');
var config = require('../../../config');


function updateGroup(tournamentId, groupId, newValue) {
  return Tournament.findById(tournamentId).then(function(tournament) {
    var group = tournament.groups.id(groupId);
    group.court = newValue["court"];
    group.leader = newValue["leader"];
    group.gamesHistory = newValue["gamesHistory"];
    group.pairs = newValue["pairs"];
    group.maxWinners = newValue["maxWinners"];
    tournament.save();
  });
}

/**
 * The Tournament Persistence Class
 * @module app/datalayer/persistence/TournamentPersister
 * @type {Object}
 */
module.exports = {

  /**
   * Create a new tournament
   * @param {Object} attrs - specifics attributes to assign to the newly formed tournament
   * @returns {Promise} Promise of saving the new tournament and returning it.
   */
  createTournament: function(attrs) {
    return Tournament(attrs).saveAsync();
  },

  /**
   * Create a family tournament
   * @returns {Promise} Promise of saving the new family tournament and returning it.
   */
  createFamilyTournament: function() {
    var familyTournament = new Tournament();
    familyTournament.category = 'Familles';
    familyTournament.day = 'Saturday';
    familyTournament.type = 'mixte';
    familyTournament.ageMin = 0;
    familyTournament.ageMax = 0;
    return familyTournament.saveAsync();
  },

  /**
   * Checks if the family tournament already exists
   * @returns {Promise} Promise of saving the new family tournament and returning it.
   */
  familyTournamentExists: function() {
    return Tournament.findAsync({
      category: 'Familles'
    }).then(function(tournament) {
      return tournament;
    })
  },


  /**
   * Remove a tournament
   * @param {ObjectId | String} tournamentId - The id of the tournament to be deleted
   * @return {Promise} Promise of deleting the specified tournament
   */
  removeTournamentById: function(tournamentId) {
    return Tournament.findByIdAndRemoveAsync(tournamentId);
  },

  /**
   * Add a reponsible to a tournament
   * @param {ObjectId | String} tournamentId - id of the tournament to which u want to add a new responsible
   * @param {ObjectId | String} staffMemberId - id the member of the staff to add as a responsible to this tournament
   * @returns {Promise} Promise that the associated staff member will be a responsible in the associated tournament
   */
  setResponsible: function(tournamentId, staffMemberId) {
    return Tournament.updateAsync({
      _id: tournamentId
    }, {
      $push: {
        staffResponsible: staffMemberId
      }
    });
  },

  /**
   * Retrieve all the existing tournaments
   * @returns {Promise} Promise giving u access to an array of Tournament
   */
  getAllTournaments: function() {
    return Tournament.findAsync({});
  },

  /**
   * Retrieve all the existing tournaments that have a leader.
   * @return {Promise} Promise giving u access to an array of Tournament found
   */
  getAllTournamentsWithLeaders: function() {
    return Tournament.find({}).populate('groups.leader');
  },

  /**
   * Retrieve a specific tournament
   * @param {ObjectId | String} tournamentId - the id of the tournament to retrieve
   * @returns {Promise} Promise giving u access to the tournament requested
   */
  getTournament: function(tournamentId) {
    return Tournament.findByIdAsync(tournamentId);
  },

  /**
   * Retrieve a list of tournaments taking place at a specific date
   * @param {String} day - The day at which the tournaments u want to retrieve take place
   * @returns {Promise} Promise giving access to a list of the tournaments queried
   */
  getTournamentsOnSpecificDay: function(day) {
    return Tournament.findAsync({
      day: day
    });
  },

  /**
   * Find the staff members responsible for the tournament who has this id
   * @param  {Numer} tournamentId - the tournament's id
   * @return A promise with the staff members found
   */
  getStaffMembersResponsibleForTournament: function(tournamentId) {
    return Tournament.findByIdAsync(tournamentId).then(function(tournament) {
      return tournament.staffResponsible;
    })
  },

  /**
   * Find tournaments whose this staff member is responsible
   * @param  {ObjectId|String} staffId - the staff's id
   * @return A promise with the tournaments found
   */
  getAllTournamentsWithStaffResponsible: function(staffId) {
    return Tournament.findAsync({
      staffResponsible: {
        $in: [staffId]
      }
    });
  },

  /**
   * Create a new group
   * @param {Object} attrs - the specific attributes to assign to the newly formed group
   * @returns {Promise} Promise giving u access to the newly saved group
   */
  createGroup: function(attrs) {
    return Group(attrs).saveAsync();
  },

  /**
   * Retrieve a specific group
   * @param {ObjectId | String} tournamentId - the id of the tournament in which the group is nested
   * @param {ObjectId|String} groupId - the id of the group u want to retrieve
   * @returns {Promise} Promise giving u a hold on the group specified
   */
  getGroupeById: function(tournamentId, groupId) {
    return Tournament.findByIdAsync(tournamentId).then(function(tournament) {
      return tournament.groups.id(groupId);
    });
  },

  /**
   * Add a group to a tournament
   * @param {ObjectId|String} tournamentId - the id of the tournament the group should be added to
   * @param {ObjectId|String} newGroup - the group to add to the tournament
   * @returns {Promise}
   */
  addGroup: function(tournamentId, newGroup) {
    return Tournament.findByIdAndUpdateAsync(tournamentId, {
      $push: {
        groups: newGroup
      }
    }, {
      new: true
    });
  },

  /**
   * Update the attributes of a group
   * @param {ObjectId|String} tournamentId - id of the tournament in which the group is stored
   * @param {ObjectId|String} groupId - id of the group to update
   * @param {Object} newValue - object containing the new values of the following attributes : 'court', 'leader', pairs', 'maxWinners'
   * @returns {Promise} Promise that the group has been updated
   */
  updateGroup: function(tournamentId, groupId, newValue) {
    return updateGroup(tournamentId, groupId, newValue);
  },

  /**
   * Delete a group from a tournament in the database
   * @param  {ObjectId|String} tournamentId - the tournament's id
   * @param  {ObjectId|String} groupId      - the group's id
   * @return A promise with the group deleted
   */
  removeGroup: function(tournamentId, groupId) {
    return Tournament.findByIdAndUpdateAsync(tournamentId, {
      $pull: {
        groups: {
          _id: groupId
        }
      }
    }, {
      new: true
    });
  },


  /**
   * Unite two groups of a tournament
   * @param  {ObjectId|String} tournamentId - the tournament's id
   * @param  {Object} groupA       - the groupA's infos
   * @param  {Object} groupB       - the groupB's infos
   * @return {Promise} A promise with the new group updated
   */
  unionGroup: function (tournamentId, groupA, groupB) {

    var newGamesHistory = groupA.gamesHistory.concat(groupB.gamesHistory);
    var newPairs = groupA.pairs.concat(groupB.pairs);

    return updateGroup(tournamentId, groupA._id, {
      court: null,
      leader: groupA.leader,
      pairs: newPairs,
      gamesHistory: newGamesHistory,
      maxWinners: (groupA.maxWinners + groupB.maxWinners)
    });
  },

  /**
   * Set the year of the tournamet in the database
   * @param {ObjectId|String} tournamentId - the tournament's id
   * @param {Number} year         - the year of the tournament
   * @return A promise with the year set up
   */
  setYear: function(tournamentId, year) {
    return Tournament.findByIdAndUpdate(tournamentId, {
      year: year
    });
  },

  /**
   * Set the Category of the tournament in the database
   * @param {ObjectId|String} tournamentId - the tournament's id
   * @param {String} category     - the category of the tournament
   * @return A promise with the category set up
   */
  setCategory: function(tournamentId, category) {
    return Tournament.findByIdAndUpdate(tournamentId, {
      category: category
    });
  },

  /**
   * Set the Type of the tournament in the database
   * @param {ObjectId|String} tournamentId - the tournament's id
   * @param {String} type     - the type of the tournament
   * @return A promise with the type set up
   */
  setType: function(tournamentId, type) {
    return Tournament.findByIdAndUpdate(tournamentId, {
      type: type
    });
  },

  /**
   * Change the status of a tournament
   * @param {Object|String} tournamentId - the id of the tournament to update
   * @param {String} status - the newly status to set to the tournament
   * @returns {Promise} Promise returning u the updated tournament
   */
  setStatus: function(tournamentId, status) {
    return Tournament.findByIdAndUpdateAsync(tournamentId, {
      status: status
    });
  },

  /**
   * Gives u a hold to an array of pairs that match criterias to be inside a tournament
   * (and thus, not already in a group of that exact tournament and match the requirement of the tournament on age & type of gender)
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @return {Promise} Promise that contains a hold on an array of pairs that match the requirements
   */
  findPairsNotInGroup: function(tournamentId) {
    var that = this;

    return that.getTournament(tournamentId).then(function(tournament) {
      return that.getTournamentsOnSpecificDay(tournament.day).then(function(validTournaments) {
        var pairsInGroups = _.flatten(_.map(validTournaments, function(validTournament) {
          var groups = _.map(validTournament.groups, function(group) {
            return _.map(group.pairs, function(id) {
              return id;
            });
            // return pairs;
          });
          return groups;
        }));


        return Pair.find({
          _id: {
            '$nin': pairsInGroups
          }
        }).populate('players').execAsync().then(function(pairs) {


          // logger.debug(pairs);
          var dateMin = new Date(tournament.ageMin, 1, 1);
          var dateMax = new Date(tournament.ageMax, 1, 1);

          return _.reject(pairs, function(pair) {

            if (tournament.category === 'Familles') {

              birthdate1 = moment(pair.players[0].birthdate);
              birthdate2 = moment(pair.players[1].birthdate);
              console.log(birthdate1.format());
              console.log(birthdate2.format());

              if(birthdate1 < birthdate2){
                if(birthdate2.diff(moment(), 'years') >= (Math.abs(config.tournoiDesFamilles.ageMaxEnfant) * -1) && birthdate1.diff(moment(), 'years') <= (Math.abs(config.tournoiDesFamilles.ageMinAdulte) * -1)){
                  return false;
                }
              }else{
                if(birthdate1.diff(moment(), 'years') >= (Math.abs(config.tournoiDesFamilles.ageMaxEnfant) * -1) && birthdate2.diff(moment(), 'years') <= (Math.abs(config.tournoiDesFamilles.ageMinAdulte) * -1)){
                  return false;
                }
              }
             console.log(birthdate1.diff(moment(), 'years'));
             console.log(birthdate2.diff(moment(), 'years'));

             return true;



            } else {

              if (tournament.ageMax == 0) {
                if (pair.players[0].birthdate > dateMin || pair.players[1].birthdate > dateMin)
                  return true;
              } else if (tournament.ageMax > 0) {
                if (pair.players[0].birthdate < dateMin || pair.players[1].birthdate < dateMin)
                  return true;
                if (pair.players[0].birthdate > dateMax || pair.players[1].birthdate > dateMax)
                  return true;
              }

              if (tournament.type != 'mixte') {
                var gender = tournament.type == 'homme' ? 'Mr' : 'Mrs';

                if (pair.players[0].title != gender || pair.players[1].title != gender) {
                  return true;
                }
              }

            }



            return false;
          });
        });

      });
    });
  },

  /**
   * Retrieve an array of pairs contained inside a specific group
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {ObjectId|String} groupId - the id of the group
   * @returns {Promise} Promise that gives u an hold to an array of pairs contained in the specified group
   */
  findPairsInGroup: function(tournamentId, groupId) {
    var that = this;

    return that.getTournament(tournamentId).then(function(tournament) {
      var group = tournament.groups.id(groupId);
      return Pair.findAsync({
        '_id': {
          $in: group.pairs
        }
      });
    }).then(function(pairs) {
      var promises = [];
      _.each(pairs, function(elem) {
        promises.push(elem.populate('players').execPopulate());
      });
      return Promise.all(promises);
    });
  },

  /**
   * Add a game to a specific group
   * @param {ObjectId|String} tournamentId - the id of the tournament from which the group came from
   * @param {ObjectId|String} groupId - the id of the group from which the game came from
   * @param {ObjectId|String} pairId1 - the id of the 1st pair involved in the game
   * @param {Number} scorePair1 - the score of the 1st pair involved in the game
   * @param {ObjectId|String} pairId2 - the id of the 2nd pair involved in the game
   * @param {Number} scorePair2 - the score of the 2nd pair involved in the game
   * @returns {Promise} Promise that the game is added to the specified group
   */
  addGameToGroup: function(tournamentId, groupId, pairId1, scorePair1, pairId2, scorePair2) {
    return Tournament.findOne({
      _id: tournamentId
    }).execAsync().then(function(tournament) {

      logger.debug(tournament);

      var index = _.findIndex(tournament.groups, function(group) {
        return group._id == groupId;
      });

      var game = new GroupGameHistory();
      game.pairs.push({
        pair: pairId1,
        score: scorePair1
      });
      game.pairs.push({
        pair: pairId2,
        score: scorePair2
      });

      tournament.groups[index].gamesHistory.push(game);
      return tournament.saveAsync();
    });
  },

  /**
   * Get a specific group in a tournament, populating the courts & leaders of each group.
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {ObjectId|String} groupId - the id of the group
   * @returns {Promise} Promise giving u the group populated
   */
  getGroupInTournament: function(tournamentId, groupId) {
    var that = this;

    return that.getTournament(tournamentId).then(function(tournament) {
      return tournament.populate('groups.court').populate('groups.leader').execPopulate();
    }).then(function(tournament) {
      return new Promise(function(resolve) {
        var group = tournament.groups.id(groupId);
        resolve(group);
      });
    });
  },

  /**
   * Delete a specific match
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {ObjectId|String} groupId - the id of the group
   * @param {ObjectId|String} matchId - the id of the match
   * @returns {Promise} Promise that this specific match is deleted from the specified group inside this specific tournament
   */
  deleteMatch: function(tournamentId, groupId, matchId) {
    return Tournament.findByIdAsync(tournamentId).then(function(tournament) {
      var group = tournament.groups.id(groupId);
      var opts = [{
        path: 'gamesHistory.pairs',
        model: 'GroupGameHistory'
      }];
      return Tournament.populateAsync(group, opts).then(function(group) {
        group.gamesHistory.pull(matchId);
        return tournament.saveAsync();
      });
    })
  },

  /**
   * Retrieve an array of games inside a specific group
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {ObjectId|String} groupId - the id of the group
   * @returns {Promise} Promise that retrieve u an array of the games inside this specified group, inside this specific tournament
   */
  findAllMatches: function(tournamentId, groupId) {
    return Tournament.findByIdAsync(tournamentId).then(function(tournament) {
      var group = tournament.groups.id(groupId);

      var opts = [{
        path: 'gamesHistory.pairs',
        model: 'GroupGameHistory'
      }, {
        path: 'gamesHistory.pairs.pair',
        model: 'Pair'
      }];

      return Tournament.populateAsync(group, opts).then(function(group) {
        return Player.populateAsync(group.gamesHistory, {
          path: 'pairs.pair.players',
          model: 'Player'
        });
      });
    });
  },

  /**
   * Retrieve an array of pairs that a pair didnt played yet against in the morning
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {ObjectId|String} groupId - the id of the group
   * @param {ObjectId|String} pairId - the id of the pair
   * @returns {Promise} Promise retrieving an array of opponents (pairs) that the specified pair didnt played against yet
   */
  findValidOpponents: function(tournamentId, groupId, pairId) {
    var that = this;

    return Tournament.findOne({
      _id: tournamentId
    }).execAsync().then(function(tournament) {
      // logger.debug(tournament);
      var group = tournament.groups.id(groupId);

      var opts = [{
        path: 'gamesHistory.pairs',
        model: 'GroupGameHistory'
      }];

      return Tournament.populateAsync(group, opts).then(function(validOpponents) {
        // logger.debug(validOpponents);
        var pairs = validOpponents.pairs;


        // logger.debug(validOpponents.gamesHistory);

        // No games played yet
        if (validOpponents.gamesHistory.length == 0) {
          return that.findPairsInGroup(tournamentId, groupId).then(function(pairs) {
            return _.reject(pairs, function(pair) {
              return pair._id.toString() === pairId;
            });
          });
        } else {
          return new Promise(function(resolve, reject) {
            async.eachSeries(validOpponents.gamesHistory, function(game, callback) {
              var plucked = _.pluck(game.pairs, 'pair');
              logger.debug(plucked);
              if (_.find(plucked, function(id) {
                  return pairId == id.toString();
                })) {
                plucked = _.reject(plucked, function(id) {
                  return id.toString() === pairId;
                });
                pairs = _.reject(pairs, function(id) {
                  return id.toString() === plucked.toString();
                });
                callback();
              } else {
                callback();
              }
            }, function done() {
              // logger.debug('Printing remaining pairs');
              pairs = _.reject(pairs, function(id) {
                return id.toString() == pairId;
              });
              // logger.debug(pairs);
              resolve(Pair.find({
                _id: {
                  "$in": pairs
                }
              }).populate('players').execAsync());
            });
          });
        }
      })
    })
  },

  /**
   * Get the array of courts not already assigned in the morning for a specific tournament
   * @param {ObjectId|String} tournamentId - the id the tournament
   * @returns {Promise} Promise returning an array of Courts not already used that day for a specific tournament
   */
  findCourtNotAlreadyUsed: function(tournamentId) {
    var that = this;
    return new Promise(function(resolve) {

      that.getTournament(tournamentId).then(function(tournament) {
        that.getTournamentsOnSpecificDay(tournament.day).then(function(validTournaments) {
          var courtsInGroups = _.flatten(_.map(validTournaments, function(validTournament) {
            var groups = _.map(validTournament.groups, function(group) {
              // logger.debug(group);
              return group.court;
              // return pairs;
            });
            return groups;
          }));

          Court.find({
            _id: {
              "$nin": courtsInGroups
            }
          }).exec(function(err, courts) {
            resolve(courts);
          });
        })

      })
    });
  },

  /**
   * Generate the knockoff games tournament in DB for a specific tournament
   * @param {ObjectId|String} tournamentId - the id of the tournament
   * @param {Number} numberOfWinningTeams - the number of teams that advance in the afternoon matches
   * @returns {Promise} Promise of the updated tournament (and thus, the knockoffTournament attribute updated)
   */
  generateKnockoffGames: function(tournamentId, numberOfWinningTeams) {
    var games = require("../../helpers/BracketGameGenerator")(numberOfWinningTeams);
    //logger.debug(games);
    var knockOffGames = [];
    _.each(games, function(element, index) {
      if (!element) return;
      knockOffGames.push({
        id: "" + index,
        fighterA: {
          id: "" + element[1],
          generatorId: element[1],
          type: (element[3]) ? 'ToBeDetermined' : 'Game'
        },
        fighterB: {
          id: "" + element[2],
          generatorId: element[2],
          type: (element[4]) ? 'ToBeDetermined' : 'Game'
        },
        isFinale: index == 1
      });
    });

    //logger.debug(knockOffGames);

    return Tournament.findOneAndUpdateAsync({
      '_id': tournamentId,
    }, {
      $set: {
        'knockOffTournament': knockOffGames
      }
    });
  },

  /**
   * Function to get a hold on an array of the winnings pairs of a tournament after the matches of the morning are done.
   * Those winnings pair are fetched from each group to a maximum defined in the group instance in db.
   * @param {ObjectId|String} tournamentId - id of the tournament from which u need the winning pairs.
   * @return {Promise} an array of arrays of pairs, the first array is the group array, each one containing an array of winning pairs of that group
   */
  getWinningPairs: function(tournamentId) {
    var that = this;
    return new Promise(function(resolve) {
      that.getTournament(tournamentId).then(function(tournament) {
        var groupOfWinningPairs = _.map(tournament.groups, function(group) {
          var scores = _.map(group.pairs, function(pair) {
            var won = 0;
            var pointWon = 0;
            var pointLost = 0;

            _.each(group.gamesHistory, function(game) {
              var result = _.findIndex(game.pairs, function(gamePair) {
                return gamePair.pair.equals(pair);
              });

              if (result != -1) {
                var myPair = game.pairs[result];
                var opponent = game.pairs[1 - result];
                pointWon += myPair.score;
                pointLost += opponent.score;
                if (myPair.score > opponent.score) {
                  won++;
                }
              }
            });

            var result = {};
            result.pairId = pair;
            result.wons = won;
            result.pointsWon = pointWon;
            result.pointsLost = pointLost;

            return result;
          });

          scores.sort(function(first, second) {
            var wonFirst = first.wons;
            var wonSecond = second.wons;
            if (wonFirst !== wonSecond) {
              return wonSecond - wonFirst;
            }

            var pointsWonFirst = first.pointsWon;
            var pointsWonSecond = second.pointsWon;
            if (pointsWonFirst !== pointsWonSecond) {
              return pointsWonSecond - pointsWonFirst;
            }

            return second.pointsLost - first.pointsLost;
          });

          var winners = [];

          for (var i = 0; i < group.maxWinners && i < group.pairs.length; i++) {
            winners.push(scores[i]);
          }

          return winners;
        });
        resolve(groupOfWinningPairs);
      });
    });
  },

  /**
   * Function that populate the Tournament for development purposes
   * @returns {Promise} Promise that the collection Tournament will be populated with some new elements
   */
  populate: function() {
    var that = this;
    return Promise.props({
      pairs: PairPersister.findAllPairs(),
      courts: CourtPersister.findAllCourts(),
      responsible: StaffPersister.findAllStaffs()
    }).then(function(result) {

      var promises = [];


      promises.push(that.createTournament({
        status: 'Poule',
        category: "Pré-minimes",
        day: "Saturday",
        type: "homme",
        ageMin: 1999,
        ageMax: 2000,
        staffResponsible: result.responsible[0],
        groups: [{
          court: result.courts[0]._id,
          pairs: [result.pairs[8]._id, result.pairs[4]._id, result.pairs[5]._id],
          maxWinners: 2,
          leader: result.pairs[4].players[0]
        }, {
          court: result.courts[1]._id,
          pairs: [result.pairs[6]._id, result.pairs[7]._id],
          maxWinners: 1,
          leader: result.pairs[4].players[0]
        }]
      }));

      promises.push(that.createTournament({
        category: "Scolaire",
        day: "Sunday",
        type: "femme",
        ageMin: 2002,
        ageMax: 2003,
        groups: [{
          court: result.courts[1]._id,
          pairs: [result.pairs[3]._id],
          leader: result.pairs[0].players[0]
        }]
      }));

      return Promise.all(promises);
    });
  }
};