var Promise = require('bluebird');
var mongoose = require('mongoose');
var Court = mongoose.model('Court');
var Schema = mongoose.Schema;
var _ = require('underscore');
var logger = require('winston');

/**
 *     The court's persistence class.
 *     @module app/datalayer/persistence/CourtPersister
 *     @type {Object}
 */
module.exports = {

  /**
   *     Creates a court in the database
   *     @param  {Object} attrs - the court's attributes
   *     @returns {Promise} A promise with the court created
   */
  createCourt: function(attrs) {
    return Court(attrs).saveAsync();
  },  

   /**
   *     Finds all the courts in the database
   *     @returns {Promise} A promise with all the courts
   */
  findAllCourts: function() {
    return Court.findAsync({});
  },

   /**
   *     Finds a court with the specified id
   *     @param  {Number} courtId - the court's id
   *     @returns {Promise} A promise with the court found
   */
  findCourtById: function(courtId) {
    return Court.findByIdAsync(courtId);
  },

  /**
   * Finds a Edition of a court with the specified ids
   *     @param  {Number} courtId - the court's id
   *     @param  {Number} editionId - the editionId's id
   *     @returns A promise with the edition found
   * 
   */
  findEditionById: function(courtId, editionId) {
        return Court.findOne({
          _id: courtId
        }).execAsync().then(function(court) {
          var editionIndex = _.findIndex(court.editions, function(edition) {
            return edition.id.toString() === editionId;
          });
          return court.editions[editionIndex];
        });
  },
   /**
   *     Adds a staff member responsible for the specified court
   *     @param  {Number} courtId - the court's id
   *     @param  {Number} staffMemberId - the staff member's id
   *     @returns {Promise} A promise with the court's staff member responsible added
   */
  setResponsible: function(courtId, staffMemberId) {

    return Court.updateAsync({
      _id: courtId
    }, {
      $push: {
        staffResponsible: staffMemberId
      }
    });

  },

  /**
   *     Gets all the courts whom the staff member is responsible
   *     @param  {Number} staffId - the staff member's id
   *     @returns {Promise} A promise with the courts
   */
  getAllCourtsWithStaffResponsible: function(staffId) {
    return Court.findAsync({
      staffResponsible: {
        $in: [staffId]
      }
    });
  },

  /**
   *     Finds the courts with a specific criteria on all the court's attributes
   *     @param  {String} criteria - the criteria used for the request
   *     @returns {Promise} A promise with the courts found
   */
  findCourts: function(criteria) {
    logger.debug('CRITERIA : ' + criteria);
    return new Promise(function(fullfill, reject) {
      var regex = new RegExp('^(.+)?' + criteria + '(.+)?$', "i");

      var query = {
        '$or': [{
          'courtAddress': regex
        }, {
          'courtSurface': regex
        }, {
          'typeOwner': regex
        }]
      };

      Court.find(query).exec(function(err, res) {
        if (err) {
          reject(err);
        } else {
          fullfill(res);
        }
      });
    });
  },

  /**
   *     Finds the courts with a specific criteria on all the court's attributes
   *     @param  {Number} courtId - the court's id to find
   *     @param  {Object} updateAttrs - the attributes of the court to update
   *     @returns {Promise} A promise with the court updated
   */
  editCourt: function(courtId, updateAttrs) {
    return Court.findByIdAndUpdateAsync(courtId, updateAttrs);
  },
  /**
   *     Finds the edition (of a court) with a specific criteria on all the court's attributes.
   *     @param  {Number} courtId - the court's id to find.
   *     @param {Number} editionId - the edition's id to find.
   *     @param  {Object} newValue - the attributes of the court to update.
   *     @returns A promise with the edition updated.
   */
  editEdition: function(courtId, editionId, newValue) {
    return Court.findByIdAsync(courtId).then(function(court) {
      var edition = court.editions.id(editionId);
      edition.courtConditions = newValue.courtConditions;
      edition.saturdayFree = newValue.saturdayFree;
      edition.sundayFree = newValue.sundayFree;
      edition.encodedBy = newValue.encodedBy;
      edition.encodedAt = newValue.encodedAt;
      court.saveAsync()
    });
  },
  /**
   *     Create a new edtion (for a court) in the database.
   *     @param  {Number} courtId - the court's id to find.
   *     @param {Number} editionId - the edition's id to find.
   *     @param  {Object} edition - the fields of the edition to push on the court.
   *     @returns A promise with the edition inserted.
   */
  createEdition: function(courtId, edition){
    return Court.findOne({
      _id: courtId
    }).execAsync().then(function(court) {
      court.editions.push(edition);
      return court.saveAsync();
    })
  },

  /**
   *     Create a comment (for a court, on a edition) in the database.
   *     @param  {Number} courtId - the court's id to find.
   *     @param {Number} editionId - the edition's id to find.
   *     @param  {Object} comment - fields of the comment to push on the edition.
   *     @returns A promise with the comment inserted.
   */
  createComment: function(courtId, editionId, comment) {
    return Court.findOne({
      _id: courtId
    }).execAsync().then(function(court) {
      logger.debug("court : " + courtId);
      var editionIndex = _.findIndex(court.editions, function(edition) {
        return edition.id.toString() === editionId;
      });
      logger.debug("edition Index : " + editionIndex);

      logger.debug(court.editions);
      court.editions[editionIndex].commentaries.push(comment);
      return court.saveAsync();
    })
  },
  /**
   * Function to insert-mock values in the database.
   * @return {Object} - A promise with all values inserted.
   */
  populate: function() {
    var promises = [];
    var courtT = new Court;
    courtT.id = 1;
    courtT.courtNumber = 9;
    courtT.courtAddress = 'Rue de la ramée 7, 1348 Louvain-la-Neuve';
    courtT.courtSurface = 'Gazon';
    courtT.typeOwner = 'Privé';
    courtT.specialInstructions = 'Passer par la gauche pour accèder au terrain';
    courtT.ownerAddress = {
      street: 'Avenue Jan Verdoodt',
      box: 1,
      zipCode: 1090,
      city: 'Bruxelles'
    };
    courtT.latitude = '50.673569';
    courtT.longitude = '4.6169025';
    courtT.editions = {
      year: 2014,
      saturdayFree: true,
      sundayFree: true,
      encodedAt: new Date,
      encodedBy: 'Dan',
      courtConditions: 'Bon',
      commentaries: [{
        who: 'Dan',
        commentary: 'Ligne blanche à refaire!',
        createdAt: new Date
      }, {
        who: 'Greg',
        commentary: 'C\'est prévu pour l\'hiver',
        createdAt: new Date
      }]
    };
    courtT.owner = {
      title: 'M',
      firstName: 'Harry',
      lastName: 'Potter',
      street: 'Place Des Sports',
      streetNumber: '11',
      box: Number,
      zipCode: 1348,
      city: 'Louvain-la-Neuve',
      email: 'tennis@uclsport.be',
    }

    var courSec = new Court;
    courSec.id = 2;
    courSec.courtNumber = 2;
    courSec.courtAddress = 'Rue royale 67, 1000 Bruxelles';
    courSec.courtSurface = 'Brique pilée';
    courSec.typeOwner = 'Club';
    courSec.specialInstructions = '';
    courSec.ownerAddress = {
      street: 'Rue royale',
      box: 67,
      zipCode: 1000,
      city: 'Bruxelles'
    };
    courSec.latitude = '50.8495931';
    courSec.longitude = '4.364084099999999';
    courSec.editions = [{
      year: 2014,
      saturdayFree: true,
      sundayFree: false,
      encodedAt: new Date,
      encodedBy: 'Dan',
      courtConditions: 'Excellent',
      commentaries: [{
        who: 'Dan',
        commentary: 'Rien à redire',
        createdAt: new Date
      }]
    }, {
      year: 2015,
      saturdayFree: false,
      sundayFree: true,
      encodedAt: new Date,
      encodedBy: 'Greg',
      courtConditions: 'Bon',
      commentaries: [{
        who: 'Greg',
        commentary: 'Les lignes ne sont plus tout à fait visibles',
        createdAt: new Date
      }]
    }];

    promises.push(this.createCourt(courtT));
    promises.push(this.createCourt(courSec));

    return Promise.all(promises);
  }
};