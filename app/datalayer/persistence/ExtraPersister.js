var mongoose = require('mongoose');
var Extra = mongoose.model('Extra');

/**
 * The Extra persistence class
 * @module app/datalayer/persistence/ExtraPersister
 * @type {Object}
 */
module.exports = {

  /**
   * Create a new Extra
   * @param {Object} attrs - specific attributes of the newly created extra
   * @returns {Promise} Promise that this extra has been created and saved
     */
  createExtra: function(attrs) {
    return Extra(attrs).saveAsync();
  },

  /**
   * Find all existent extras
   * @returns {Promise} Promise giving u a hold on an array of extras
     */
  findExtras: function() {
    return Extra.findAsync();
  },

  /**
   * Edit a specific extra
   * @param {ObjectId|String} extraId - the id of the specific extra
   * @param {Object} updateAttrs - the attributes to update
     * @returns {Promise} Promise that this extra has been updated and saved
     */
  editExtra: function(extraId, updateAttrs) {
    return Extra.findByIdAndUpdateAsync(extraId, updateAttrs);
  },

  /**
   * Delete a specific extra
   * @param {ObjectId|String} extraId - the id of the extra to delete
   * @returns {Promise} Promise that this specific extra is deleted
     */
  deleteExtra: function(extraId){
    return Extra.findByIdAndRemoveAsync(extraId);
  },

  /**
   * Populate the doc to add some statics extras (mainly for testing & developping purposes)
   * @returns {Promise} Promise that a extra has been added
     */
  populate: function() {
    var promises = [];

    promises.push(this.createExtra({extraTitle:"BBQ", price: 20}));

    return Promise.all(promises);
  }
};