var Promise = require('bluebird');
var mongoose = require('mongoose');
var HistoryEntry = mongoose.model('HistoryEntry');
var Schema = mongoose.Schema;

/**
 * The HistoryEntry persistence class
 * @module app/datalayer/persistence/HistoryEntryPersister
 * @type {Object}
 */
module.exports = HistoryEntryPersister = {

  /**
   * Create a new HistoryEntry
   * @param  {Object} attrs - the object containing the attributes to bet set on the newly persisted document
   * @return {Promise} Promise of saving the new history entry and returning it.
   */
  createHistoryEntry: function(attrs){
    return HistoryEntry(attrs).saveAsync();
  },

  /**
   * Find all the history entries present in the database
   * @return {Promise} the history entries found in the database
   */
  getAllHistoryEntries: function () {
    return HistoryEntry.findAsync({});
  },

  /**
   * Find a history entry present in the database
   * @param  {ObjectId|String} objectId - id of the history entry
   * @return {Promise} the history entry found in the database
   */
  getHistoryEntriesById: function (objectId) {
  	return new Promise(function (fullfill, reject) {
  		HistoryEntry.find(
  			{ 
  				updatedId: objectId 
  			}, 
  			null, 
  			{ 
  				limit: 10, 
  				sort: { 
  					'date': -1 
  				}
  			}).exec(function (err, res) {
  			if (err) {
  				reject(err);
  			} else {
  				fullfill(res);
  			}
  		});
  	});
  }
};