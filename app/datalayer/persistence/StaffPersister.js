var mongoose = require('mongoose');
var Staff = mongoose.model('Staff');

/**
 * The Staff Persistence Class
 * @module app/datalayer/persistence/StaffPersister
 * @type {Object}
 */
module.exports = {

  /**
   * Creates a new staff member
   * @param {Object} attrs - specific attributes to assign to the newly created staff member
   * @returns {Promise} Promise of this staff member to be added & saved
     */
  createStaff: function(attrs) {
    return Staff(attrs).saveAsync();
  },

  /**
   * Find a staff member by his unique id
   * @param {ObjectId|String} staffId - the id of the staff member to retrieve
   * @returns {Promise} Promise giving u access to the staff member u were looking for (if any)
     */
  findStaff: function(staffId) {
    return Staff.findByIdAsync(staffId);
  },

  /**
   * Get an array of all existants staff members
   * @returns {Promise} Promise giving u a hold to an array of staff members (with level 'Staff')
     */
  findAllStaffs: function() {
    return Staff.findAsync({
      moderationLevel: 'Staff'
    });
  },

  /**
   * Log in a staff member with a specific username & password
   * @param {String} username - the username of the staff member to log in
   * @param {String} password - the password of the staff member to log in
   * @returns {Promise} Promise getting u the staff member matching the username & password, if any
     */
  staffLogin: function(username, password) {
    return Staff.findOneAsync({
      username: username,
      password: password
    });
  },

  /**
   * Update a staff member infos
   * @param {ObjectId|String} staffId - the id of the staff member to update
   * @param {Object} attrs - the attributes to update
   * @returns {Promise} Promise that this specific staff member will be updated
     */
  updateStaff: function(staffId, attrs) {
    return Staff.findByIdAndUpdateAsync(staffId, attrs);
  },

  /**
   * Remove a staff member from the system
   * @param {ObjectId|String} staffId - the id of the staff member to delete
   * @returns {Promise} Promise that this staff member has been deleted
     */
  deleteStaff: function(staffId){
    return Staff.findByIdAsync(staffId).then(function(doc){
      return doc.removeAsync();
    });
  },

  /**
   * Function to populate the docs of the mongoDB with a admin
   */
  populate: function() {
    var promises = [];

    that = this;
    promises.push(that.createStaff({
      username: 'admin',
      password: 'password',
      name: 'Dan Martens',
      moderationLevel: 'Admin'
    }));

    promises.push(this.createStaff({
      username: 'greg',
      password: 'lol',
      name: 'Grégory Schot',
      mail: 'gregory.schot@gmail.com',
      phoneNumber: '0472/47/93/08',
      moderationLevel: 'Staff'
    }));

    return Promise.all(promises);
  }
};