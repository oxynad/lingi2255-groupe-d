var Promise = require('bluebird');
var mongoose = require('mongoose');
var Player = mongoose.model('Player');
var Schema = mongoose.Schema;
var logger = require('winston');

/**
 * The application's Player Persister
 * @module app/datalayer/persistence/PlayerPersister
 * @type {Object}
 */
module.exports = {

  /**
   * Create new player in the database
   * @param  {Object} attrs - list of attributes of a player
   * @return {Object} the player saved in the database
   */
  createPlayer: function(attrs) {
    return Player(attrs).saveAsync();
  },

  /**
   * Edit player in the database
   * @param  {Object} playerId - id of the player 
   * @param  {Object} attrs - list of the attributes to update in the database
   * @return {Promise} the player saved in the database
   */
  editPlayer: function(playerId, updateAttrs) {
    console.log('UPDATE ATTRIBUTES : ' + JSON.stringify(updateAttrs));
    return Player.findByIdAndUpdateAsync(playerId, updateAttrs);
  },

  /**
   * Remove player from the database
   * @param  {ObjectId} playerId - id of the player
   * @return {Promise} the player deleted the database
   */
  deletePlayer: function(playerId) {
    return Player.findByIdAndRemoveAsync(playerId);
  },

  /**
   * Find all the players present in the database
   * @return {Promise} the players found in the database
   */
  findAllPlayers: function() {
    return Player.findAsync({});
  },

  /**
   * Find a specific player present in the database
   * @param  {ObjectId} playerId - id of the player
   * @return {Promise} the player found in the database
   */
  findPlayer: function(playerId) {
    return Player.findOneAsync({
      _id: playerId
    });
  },

  /**
   * Find all the rankings appearing in the table Player
   * @return {Promise} the rankings found in the database
   */
  findAllRankingPossible: function() {
    return Player.valueOf().schema.paths.aftRanking.enumValues;
  },

  /**
   * Find a specific player by his name in the database
   * @param  {String} name - name of the player 
   * @return {Promise} the player found in the database
   */
  findPlayerByName: function(name) {
    name = name.trim();
    var nameSplitted = name.split(' ');
    return Player.findOneAsync({
      firstName: nameSplitted[0],
      lastName: nameSplitted[1]
    })
  },

  /**
   * Find a specific player by his keyEmailConfirm in the database
   * @param  {String} keyEmailConfirm - key of the confirmation link of the player's email
   * @return {Promise} the player found in the database
   */
  findPlayerByKeyEmailConfirm: function(keyEmailConfirm) {
    return Player.findOneAsync({
      keyEmailConfirm: keyEmailConfirm
    });
  },

  /**
   * Find a specific player by his lastname, firstname and birthdate in the database
   * @param  {String} _lastName - lastname of the player
   * @param  {String} _firstName - firstname of the player
   * @param  {Date} _brithdate - birthdate of the player
   * @return {Promise} the player found in the database, exists if not null
   */
  checkPlayerExists: function(_lastName, _firstName, _birthdate) {
    return Player.findOneAsync({
      lastName: _lastName,
      firstName: _firstName,
      birthdate: _birthdate
    });
  },

  /**
   * Find a specific player by his firstname, lastname, title, gsm or email in the database
   * @param  {String} criteria - firstname, lastname, title, gsm or email of the player
   * @return {Promise} the player found in the database
   */
  findPlayers: function(criteria) {
    logger.debug('CRITERIA : ' + criteria);
    return new Promise(function(fullfill, reject) {
      var regex = new RegExp('^(.+)?' + criteria + '(.+)?$', "i");

      var query = {
        '$or': [{
          'firstName': regex
        }, {
          'lastName': regex
        }, {
          'title': regex
        }, {
          'gsm': regex
        }, {
          'email': regex
        }, ]
      };


      Player.find(query).exec(function(err, res) {
        if (err) {
          reject(err);
        } else {
          fullfill(res);
        }
      });
    });
  },

  /**
   * Populate database with some Players
   * @return {Promise} the players created
   */
  populate: function() {

    var promises = [];

    promises.push(this.createPlayer({
      firstName: 'Grace',
      lastName: 'Musuvaho',
      street: "Rue de l'Ephec",
      streetNumber: "32",
      zipCode: 1348,
      city: "Louvain-la-Neuve",
      title: 'Mrs',
      gsm: '0480000000',
      email: 'graceMusuvaho@test.com',
      birthdate: new Date(1999, 11, 27),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Ninon',
      lastName: 'De Greef',
      street: "Rue du ponney",
      streetNumber: 8,
      zipCode: 5900,
      city: "Nivelle",
      title: 'Mrs',
      gsm: '0480000111',
      email: 'ninonDeGreef@test.com',
      birthdate: new Date(1999, 11, 31),
      aftRanking: 'C30.5'
    }));


    promises.push(this.createPlayer({
      firstName: 'Dan',
      lastName: 'Martens',
      street: "Sentier de la nuit blanche",
      streetNumber: 24,
      zipCode: 1000,
      city: "Bruxelles",
      title: 'Mr',
      gsm: '04399993',
      email: 'oxynad@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30.4'
    }));

    promises.push(this.createPlayer({
      firstName: 'Arthur',
      lastName: 'Glanbert',
      street: "Boulevard du mainstream",
      streetNumber: 1,
      zipCode: 1410,
      city: "Waterloo",
      title: 'Mr',
      gsm: '04399993',
      email: 'ArthurEnglanbert@spam.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30.3'
    }));

    promises.push(this.createPlayer({
      firstName: 'Igor',
      lastName: 'Kopestenski',
      title: 'Mr',
      street: "Passage fitness",
      streetNumber: 1000,
      zipCode: 1420,
      city: "Braine l'alleud",
      gsm: '04399993',
      email: 'halterophile@heavy.org',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30.2'
    }));

    promises.push(this.createPlayer({
      firstName: 'Greg',
      lastName: 'Schot',
      street: "avenue de l'humour",
      streetNumber: 1000,
      zipCode: 1420,
      city: "Jette",
      title: 'Mr',
      gsm: '04399993',
      email: 'esi@informaticien.be',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30.1'
    }));

    promises.push(this.createPlayer({
      firstName: 'Barack',
      lastName: 'Obama',
      title: 'Mr',
      gsm: '04399993',
      email: 'lololol@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30'
    }));

    promises.push(this.createPlayer({
      firstName: 'Vladimir',
      lastName: 'Poutine',
      title: 'Mr',
      gsm: '04399993',
      email: 'lololol@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Nicolas',
      lastName: 'Sarkozy',
      title: 'Mr',
      gsm: '04399993',
      email: 'lololol@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Francois',
      lastName: 'Hollande',
      title: 'Mr',
      gsm: '04399993',
      email: 'lololol@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Omar',
      lastName: 'Souleyman',
      title: 'Mr',
      gsm: '04399993',
      email: 'lololol@gmail.com',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Justin',
      lastName: 'Bieber',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Roland',
      lastName: 'Chambry',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1999, 3, 15),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Nico',
      lastName: 'Trololol',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1999, 3, 10),
      aftRanking: 'C30',
      alreadyInPoule: true
    }));

    promises.push(this.createPlayer({
      firstName: 'Armand',
      lastName: 'Badant',
      street: "tg",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1986, 3, 15),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Roland',
      lastName: 'Badand',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(2006, 3, 10),
      aftRanking: 'C30',
      alreadyInPoule: true
    }));

    promises.push(this.createPlayer({
      firstName: 'Colin',
      lastName: 'Brezil',
      street: "tg",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1986, 3, 15),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Bazille',
      lastName: 'Brezil',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(2006, 3, 10),
      aftRanking: 'C30',
      alreadyInPoule: true
    }));

     promises.push(this.createPlayer({
      firstName: 'Quentin',
      lastName: 'Dumont',
      street: "tg",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(1986, 3, 15),
      aftRanking: 'N.C.'
    }));

    promises.push(this.createPlayer({
      firstName: 'Alain',
      lastName: 'Dumon',
      street: "Castor street",
      streetNumber: 666,
      zipCode: 6000,
      city: "Liege",
      title: 'Mr',
      gsm: '04399993',
      email: 'ego@badsinger.ca',
      birthdate: new Date(2006, 3, 10),
      aftRanking: 'C30',
      alreadyInPoule: true
    }));

    return Promise.all(promises);
  }

};