var StaffPersister = require('./StaffPersister');
var PlayerPersister = require('./PlayerPersister');
var CourtPersister = require('./CourtPersister');
var PairPersister = require('./PairPersister');
var ExtraPersister = require('./ExtraPersister');
var TournamentPersister = require('./TournamentPersister');
var HistoryEntryPersister = require('./HistoryEntryPersister');

module.exports = {
  PlayerPersister: PlayerPersister,
  StaffPersister: StaffPersister,
  CourtPersister: CourtPersister,
  PairPersister: PairPersister,
  ExtraPersister: ExtraPersister,
  TournamentPersister: TournamentPersister,
  HistoryEntryPersister: HistoryEntryPersister,
  All: [PlayerPersister, StaffPersister, CourtPersister, PairPersister, ExtraPersister, TournamentPersister]
};