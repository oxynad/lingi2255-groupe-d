var mongoose = require('mongoose');
var Promise = require('bluebird');

var PairSchema = new mongoose.Schema({
	players:{
		type: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Player'
		}],
		validate: [arrayLimit, '{PATH} has not the size of 2']
	},
	extra: [{
		quantity: Number,
		extra: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Extra'
		}
	}],
	payment: {
		type: String,
		default: null,
	},
	montant: {
		type: Number,
		default: null,
	},
    comment: {
    	type: String,
    	default: '',
    }
});

function arrayLimit(val){
	return val.length == 2;
}

var Pair = mongoose.model('Pair', PairSchema);

Promise.promisifyAll(Pair);
Promise.promisifyAll(Pair.prototype);

module.exports = Pair;