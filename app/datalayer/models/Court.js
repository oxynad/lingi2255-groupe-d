var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');

var address = new Schema ({
	street: String,
  	box: Number,
  	zipCode: Number,
  	city: String
});

var commentarySchema = new Schema ({
	who: String,
	commentary: String,
	createdAt: { "type": Date, "default": Date.now }
});

var editionSchema = new Schema ({
	year: Number,
	saturdayFree: Boolean,
	sundayFree: Boolean,
	encodedAt: Date,
	encodedBy: String,
	courtConditions: {type: String, enum: ['Mauvais','Bon','Excellent']},
	commentaries: [commentarySchema]
});

var ownerSchema = new Schema({
	title : {type: String, enum:['M', 'Mme']},
	firstName : String,
	lastName : String,
	street : String,
	streetNumber: Number,
  	box: Number,
  	zipCode: Number,
  	city: String,
  	cellphone:Number,
  	email: String,
  	phone: Number,
  	fax: Number
});

var courtSchema = new Schema ({
	// id: Number,
	courtNumber: Number,
	courtAddress: String,
	courtSurface: {type: String, enum: ['Gazon','Synthétique','Brique pilée','Béton']},
	typeOwner: {type: String, enum: ['Privé','Club']},
	specialInstructions: {
		type: String,
		default: ''
	},
	staffResponsible: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Staff'
  }],
	ownerAddress: [address],
	latitude: String,
	longitude: String,
	owner: [ownerSchema],
	editions: [editionSchema]
});

// Create model
var Court = mongoose.model('Court', courtSchema);
var Edition = mongoose.model('Edition', editionSchema);
var Owner = mongoose.model('Owner', ownerSchema);
var Commentary = mongoose.model('Commentary', commentarySchema);

// Promisify all mongoose functions
Promise.promisifyAll(Court);
Promise.promisifyAll(Court.prototype);

module.exports = Court;