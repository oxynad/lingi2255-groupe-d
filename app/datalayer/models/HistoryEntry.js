var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');

// Create schema
var historyEntrySchema = new Schema({
  date: Date,
  author: String,
  updatedId: mongoose.Schema.Types.ObjectId,
  updatedCollection: String,
  updatedField: String,
  oldValue: String
});

// Create model
var HistoryEntry = mongoose.model('HistoryEntry', historyEntrySchema);

// Promisify all mongoose functions
Promise.promisifyAll(HistoryEntry);
Promise.promisifyAll(HistoryEntry.prototype);

module.exports = HistoryEntry;