var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');

// function AllFieldsRequiredByDefautlt(schema) {
//     for (var i in schema.paths) {
//         var attribute = schema.paths[i]
//         if (attribute.isRequired == undefined) {
//             attribute.required(true);
//         }
//     }
// }


// Create schema
var playerSchema = new Schema({
  title: {type: String, enum: ['Mr', 'Mrs']},
  firstName: String,
  lastName: String,
  street: String ,
  streetNumber: Number,
  box: Number,
  zipCode: Number,
  city: String,
  gsm: String,
  email: String,
  birthdate: Date,
  aftRanking: {type: String, enum: ['N.C.', 'C30.5', 'C30.4', 'C30.3', 'C30.2', 'C30.1', 'C30', 'C15.5', 'C15.4', 'C15.3', 'C15.2', 'C15.1', 'C15', 'B+4/6', 'B+2/6', 'B0', 'B-2/6', 'B-4/6']},
  alreadyInPoule: Boolean,
  keyEmailConfirm: String,
  emailConfirmed: {type: Boolean, default: false},
  historyTournaments:  [{
    year: Number
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

// AllFieldsRequiredByDefautlt(playerSchema);

// Create model
var Player = mongoose.model('Player', playerSchema);

// Promisify all mongoose functions
Promise.promisifyAll(Player);
Promise.promisifyAll(Player.prototype);

module.exports = Player;