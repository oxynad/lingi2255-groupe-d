var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');

// var groupSchema = require('./Group').schema;


var gameHistorySchema = new Schema({

  // in the array, the two pairs playing against each other
  pairs: [{
    pair: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Pair'
    },
    score: Number
  }],
  timeEncoded: {
    type: Date,
    default: Date.now
  }

});

// referencing objects in schemas : https://alexanderzeitler.com/articles/mongoose-referencing-schema-in-properties-and-arrays/

// Create schema
var groupSchema = new Schema({

  court: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Court'
  },
  leader: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player'
  },
  // max 5 pairs on saturday, 6 on sunday
  pairs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pair'
  }],
  playersLevelInterval: [Number], // 2 values in the array, min and max level
  // 1 to 3 winner pairs
  maxWinners: {
    type: Number,
    default: 2
  },
  gamesHistory: [gameHistorySchema],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

var knockOffFighter = {
  id: String,
  type: {
    type: String,
    enum: ['Pair', 'Game', 'ToBeDetermined']
  },
  generatorId:Number,
  score: [Number]
};


var knockOffGameSchema = new Schema({
  court: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Court'
  },

  winner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pair'
  },

  id: String,

  fighterA: knockOffFighter,
  fighterB: knockOffFighter,

  isFinale: {
    type: Boolean,
    default: false
  }
});


// Create schema
var tournamentSchema = new Schema({
  status: {type: String, enum:['Conception', 'Poule', 'Elimination', 'Assignment', 'KnockOff', 'Terminé'], default:'Conception'},
  category: {type: String, enum: ['Pré-minimes', 'Minimes', 'Cadets', 'Scolaire', 'Junior', 'Seniors', 'Elites', 'Familles']},
  day: {type: String, enum: ['Saturday', 'Sunday']},
  type: {type: String, enum: ['homme', 'femme', 'mixte']}, 
  groups: [groupSchema], // Groups in the tournament
  ageMin: Number,
  ageMax: {type: Number, default: 0},
  staffResponsible: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Staff'
  }],
  nbOfKnockOffPairs: Number,
  knockOffTournament: [knockOffGameSchema],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

// Create model
var Tournament = mongoose.model('Tournament', tournamentSchema);
mongoose.model('Group', groupSchema);
mongoose.model('GroupGameHistory', gameHistorySchema);

// Promisify all mongoose functions
Promise.promisifyAll(Tournament);
Promise.promisifyAll(Tournament.prototype);

module.exports = Tournament;