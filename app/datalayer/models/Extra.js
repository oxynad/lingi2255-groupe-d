var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');

// Create schema
var extraSchema = new Schema({
  price: Number,
  extraTitle: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

// Create model
var Extra = mongoose.model('Extra', extraSchema);

// Promisify all mongoose functions
Promise.promisifyAll(Extra);
Promise.promisifyAll(Extra.prototype);

module.exports = Extra;