var Player = require('./Player');
var Staff = require('./Staff');
var Pair = require('./Pair');
var Extra = require('./Extra');
var Court = require('./Court');
var Tournament = require('./Tournament');
var HistoryEntry = require('./HistoryEntry');

module.exports = {
  Player: Player,
  Staff: Staff,
  Pair: Pair,
  Extra: Extra,
  Court: Court,
  Tournament: Tournament,
  HistoryEntry: HistoryEntry,
  All: [Court,Staff,Extra,Player,Pair,Tournament, HistoryEntry]
};