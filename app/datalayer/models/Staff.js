var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Promise = require('bluebird');
var Court = require('./Court');
var Tournament = require('./Tournament');


// Create schema
var staffSchema = new Schema({
  username: String,
  password: String,
  moderationLevel: {
    type: String,
    enum: ['Admin', 'Staff']
  },
  phoneNumber: String,
  name: String,
  mail: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },

  tournamentMngt: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tournament'
  }]

});

staffSchema.pre('remove', function(next) {

  var promises = [];

  promises.push(Court.updateAsync({
    staffResponsible: {
      $in: [this._id]
    }
  }, {
    $pull: {
      staffResponsible: this._id
    }
  }));

  promises.push(Tournament.updateAsync({
    staffResponsible: {
      $in: [this._id]
    }
  }, {
    $pull: {
      staffResponsible: this._id
    }
  }));

  Promise.all(promises).then(function(){
    next();
  })

})

// Create model
var Staff = mongoose.model('Staff', staffSchema);

// Promisify all mongoose functions
Promise.promisifyAll(Staff);
Promise.promisifyAll(Staff.prototype);

module.exports = Staff;