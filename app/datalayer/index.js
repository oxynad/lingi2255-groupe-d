var mongoose = require('mongoose');
var Promise = require('bluebird');
var _ = require('underscore');
var logger = require('winston');
var models = require('./models');
var persisters = require('./persistence');

/**
 *     The datalayer base file.
 *     @module app/datalayer/index
 *     @type {Object}
 */
module.exports = {

  /**
   *     Starts mongoose
   *     @param  {String} url - the mongoDB database url
   *     @returns {Void}
   */
  startUp: function(url) {
    mongoose.connect(url);
    mongoose.connection.on('open', function() {
      logger.info("Connected to MongoDB");
    });
    mongoose.connection.on('error', function() {
      logger.error("====== An error happened while opening the MongoDB ======");
    });
    mongoose.connection.on('close', function() {
      logger.info("Disconnected from MongoDB");
    });
  },

  /**
   *     Cleans the whole database
   *     @returns {Promise} A promise all the database cleaned
   */
  clean: function() {
    logger.verbose("start cleaning db ...");
    var promises = models.All.map(function (model) {
      return model.removeAsync();
    });
    return Promise.all(promises).then(function() {
      logger.verbose("db cleared");
    });
  },

  /**
   *     Populates the database
   *     @returns {Promise} A promise with all the persister's populate method called
   */
  populate: function() {
    logger.verbose("start populating db ...");
   var TournamentPersister = persisters.All.pop();

    var promises = persisters.All.map(function(persister) {
      return persister.populate();
    });

    return Promise.all(promises).then(function(){
      return TournamentPersister.populate(); // Populate tournaments
    }).then(function() {
      logger.verbose("population of db done");
    });
  },  

  /**
   *     Closes the mongoose connection
   */
  shutDown: function() {
    mongoose.disconnect();
  },

  models: models,
  persisters: persisters
};