var $ = require('jquery');



$(document).ready(function() {

  $('.parallax').parallax();

/*
  Load all js modules
  */
  require('./modules/staff')();
  require('./modules/player')();
  require('./modules/court')();
  require('./modules/tournament')();
  require('./modules/group')();
  require('./modules/owner.js')();
  require('./modules/pairs')();



  


/*
  Materialize select override
  */
  $('select').material_select();
  console.log('Launching App');

});