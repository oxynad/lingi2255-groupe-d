var $ = require('jquery');

module.exports = function() {

    const listSize = 5;
    var playersList;
    var playersRecordedList;
    var nbPages;
    var currentPage;
    var courtsList;
    var courtRecorded;
    var nbPagesCourts;
    var currentPageCourts;
    var courtSelected = false;

    function numberPages(dataArray) {
        if (dataArray.length % listSize === 0)
            return dataArray.length / listSize;
        return Math.floor(dataArray.length / listSize) + 1;
    };

    function checkButtons() {
        var nbPairs = playersRecordedList.length;
        var hasTL = ($('#tLeader').val() !== null && $('#tLeader').val() !== "");
        var nbPairsNextStep = $('#nbWiningPairs').val() !== "";

        if (nbPairs >= 2 && courtSelected === true && hasTL === true && nbPairsNextStep >= 1) {
            $('#reset').removeClass('disabled');
            $('#confirm').removeClass('disabled');
            $('#sendMail').removeClass('disabled');
            $('#validate').removeClass('disabled');
        } else {
            $('#reset').addClass('disabled');
            $('#confirm').addClass('disabled');
            $('#sendMail').addClass('disabled');
            $('#validate').addClass('disabled');
        }
    };

    function swipeRightPlayers() {
        currentPage++;
        injectPlayersData();
        if (currentPage === nbPages) {
            $('#rightArrow').addClass('disabled');
            $('#rightArrow').removeClass('waves-effect');
        }
    };

    function injectPlayersData() {
        nbPages = numberPages(playersList);
        $('#playersList').empty();
        if (playersList.length > 0) {
            $('#playersList').append('<table class=\"bordered simpleBackground\">' +
                '<thead><tr>' + '<th data-field=\"playersName\">Noms des joueurs</th><th data-field=\"actions\">Ajouter</th>' +
                '</tr></thead><tbody>');
            for (var i = 0; i < listSize && (currentPage * listSize + i < playersList.length); i++) {
                var currentPair = playersList[currentPage * listSize + i];
                var html = "";
                if (currentPair["wish"] != null && currentPair["wish"].length > 0) {
                    html += '<tr class=\"wishes\"><td name="' + currentPair["id"] + '" class="wishesClickable">' +
                        '<i class="small material-icons">info_outline</i> '+currentPair["name"] + '</td><td><a id=' + (currentPage * listSize + i) +
                        ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons" style="background:#3C8AD2">add</i></a></td></tr>';
                    var modal = createWishModal(currentPair["id"], currentPair["name"], currentPair["wish"]);
                    $('#modalsZone').append(modal);
                } else {
                    html += '<tr><td>' + currentPair["name"] + '</td><td><a id=' +
                        (currentPage * listSize + i) + ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons" style="background:#3C8AD2">add</i></a></td></tr>'
                }
                $('#playersList tbody').append(html);
            };
            $('#playersList table').append('<ul class=\"pagination\"><li id=\"leftArrow\" class=\"waves-effect\"><i class=\"material-icons\">chevron_left</i></li>');
            for (var i = 0; i < nbPages; i++) {
                $('#playersList table ul').append('<li id=page' + (i + 1) + ' class=\"waves-effect pageNumber\">' + (i + 1) + '</li>');
            };
            $('#playersList table ul').append('<li id=\"rightArrow\" class=\"waves-effect\"><i class=\"material-icons\">chevron_right</i></li></ul>');
            $('#page' + (currentPage + 1)).addClass('active');
        } else {
            $('#playersList').append('<h4>Aucune paire de joueurs disponible</h4>');
        }

    };

    function swipeLeftPlayers() {
        currentPage--;
        injectPlayersData();
        if (currentPage === 0) {
            $('#leftArrow').addClass('disabled');
            $('#leftArrow').removeClass('waves-effect');
        }
    };

    function changePlayersPage(nPage) {
        currentPage = nPage - 1;
        injectPlayersData();
        if (currentPage === 0) {
            $('#leftArrow').addClass('disabled');
            $('#leftArrow').removeClass('waves-effect');
        }
        if (nPage === nbPages) {
            $('#rightArrow').addClass('disabled');
            $('#rightArrow').removeClass('waves-effect');
        }
    }

    function updateTeamLeaderSelect() {
        $('#tLeader').empty();
        $('#tLeader').append('<option value="" disabled selected>Choissisez le chef d\'équipe</option>');
        for (var pairs = 0; pairs < playersRecordedList.length; pairs++) {
            var players = playersRecordedList[pairs]["name"].split('-');
            for (var i = 0; i < players.length; i++) {
                $('#tLeader').append($('<option>', {
                    value: players[i].trim(),
                    text: players[i].trim()
                }));
            }
        }
        $('#tLeader').material_select();
    }

    function addParticipatingPair(pos) {
        var html = "";
        if (playersList[pos]["wish"] != null && playersList[pos]["wish"].length > 0) {
            html += '<tr class="wishes"><td class="wishesClickable" name="' + playersList[pos]["id"] + '">' + 
            	'<i class="small material-icons">info_outline</i>'+playersList[pos]["name"] + '</td><td><a id=' + playersList[pos]["id"] +
                ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons" style="background:#3C8AD2">remove</i>' +
                '</a></td><td style="display: none;">' + playersList[pos]["id"] + '</td></tr>';
        } else {
            html += '<tr class="backgroundReverse"><td>' + playersList[pos]["name"] + '</td><td><a id=' +
                playersList[pos]["id"] + ' class="btn-floating btn-medium waves-effect waves-light">' +
                '<i class="material-icons" style="background:#3C8AD2">remove</i></a></td><td style="display: none;">' + playersList[pos]["id"] + '</td></tr>';
        }
        $('#participatingPairs').append(html);
        playersRecordedList.push(playersList[pos]);

        updateTeamLeaderSelect();
        checkButtons();

    };

    function checkPosParticipatingPair(id) {
        for (var pos = 0; pos < playersRecordedList.length; pos++) {
            if (playersRecordedList[pos]["id"] === id) {
                return pos;
            }
        }
    }

    function removeParticipatingPair(pos, rowId) {
        playersList.push(playersRecordedList[pos]);
        playersRecordedList.splice(pos, 1);
        $('#' + rowId).parent().parent().remove();
        updateTeamLeaderSelect();
    }

    function createPairsList(dataArray) {
        playersList = dataArray;
        playersRecordedList = [];
        currentPage = 0;

        updatePairsList();

    };

    function updatePairsList() {
        $('#playersList').empty();
        if (playersList.length > 0) {
            injectPlayersData();
            $('#page1').addClass('active');
            if (currentPage === nbPages) {
                $('#rightArrow').addClass('disabled');
                $('#rightArrow').removeClass('waves-effect');
            }
        } else {
            $('#playersList').append('<h4>Aucune paire de joueurs disponible</h4>');
        }
    }

    function swipeRightCourts() {
        currentPageCourts++;
        injectCourtsData(!courtSelected);
        if (currentPageCourts === nbPagesCourts) {
            $('#courtRightArrow').addClass('disabled');
            $('#courtRightArrow').removeClass('waves-effect');
        }
    };

    function createWishModal(id, title, content) {
        var html = "";
        html += '<div id="modal' + id + '" class="modal"><div class="modal-content">' +
            '<h3>Souhait pour : ' + title + '</h3><p>' + content + '</p>' +
            '</div></div>';
        return html;
    }

    function injectCourtsData(addButton) {
        nbPagesCourts = numberPages(courtsList);
        $('#courtsList').empty();
        if (courtsList.length > 0) {
            var tHeader = '<table class=\"bordered simpleBackground\"><thead><tr>' + '<th data-field=\"cAddress\">Addresse du terrain</th>';
            if (addButton)
                tHeader += '<th data-field=\"actions\">Ajouter</th>';
            tHeader += '</tr></thead><tbody>';
            $('#courtsList').append(tHeader);
            for (var i = 0; i < listSize && (currentPageCourts * listSize + i < courtsList.length); i++) {
                var currentCourt = courtsList[currentPageCourts * listSize + i];
                var text = currentCourt["address"];
                if (currentCourt["wish"] != null && currentCourt["wish"].length > 0) {
                    var tableRow = '<tr class=\"wishes backgroundReverse\"><td class="wishesClickable" name="' + currentCourt["id"] + '">' + 
                    	'<i class="small material-icons">info_outline</i>' + text + '</td>';
                    var modal = createWishModal(currentCourt["id"], currentCourt["address"], currentCourt["wish"]);
                    $('#modalsZone').append(modal);
                } else {
                    var tableRow = '<tr class=\"backgroundReverse\"><td>' + text + '</td>';
                }
                if (addButton)
                    tableRow += '<td><a id=' + (currentPageCourts * listSize + i) + ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons">add</i></a></td>';
                tableRow += '</tr>';
                $('#courtsList tbody').append(tableRow);
            };
            $('#courtsList table').append('<ul class=\"pagination\"><li id=\"courtLeftArrow\" class=\"waves-effect\"><i class=\"material-icons\">chevron_left</i></li>');
            for (var i = 0; i < nbPagesCourts; i++) {
                $('#courtsList table ul').append('<li id=pageCourts' + (i + 1) + ' class=\"waves-effect courtPageNumber\">' + (i + 1) + '</li>');
            };
            $('#courtsList table ul').append('<li id=\"courtRightArrow\" class=\"waves-effect\"><i class=\"material-icons\">chevron_right</i></li></ul>');
            $('#pageCourts' + (currentPage + 1)).addClass('active');
        } else {
            $('#courtsList').append('<h4>Aucun terrain disponible</h4>');
        }
    };

    function swipeLeftCourts() {
        currentPageCourts--;
        injectCourtsData(!courtSelected);
        if (currentPageCourts === 0) {
            $('#courtLeftArrow').addClass('disabled');
            $('#courtLeftArrow').removeClass('waves-effect');
        }
    };

    function createPaginedCourtsList(dataArray) {
        courtsList = dataArray;
        currentPageCourts = 0;

        $('#courtsList').empty();
        if (dataArray.length > 0) {
            injectCourtsData(!courtSelected);
            $('#pageCourts1').addClass('active');
            if (currentPageCourts === nbPagesCourts) {
                $('#courtRightArrow').addClass('disabled');
                $('#courtRightArrow').removeClass('waves-effect');
            }
        } else {
            $('#courtsList').append('<h4>Aucun terrain disponible</h4>');
        }

    };

    function changeCourtPage(nPage) {
        currentCourtsPage = nPage - 1;
        injectCourtsData(!courtSelected);
        if (currentCourtsPage === 0) {
            $('#courtLeftArrow').addClass('disabled');
            $('#courtLeftArrow').removeClass('waves-effect');
        }
        if (nPage === nbPagesCourts) {
            $('#courtRightArrow').addClass('disabled');
            $('#courtRightArrow').removeClass('waves-effect');
        }
    };

    function addSelectedCourt(pos) {
        if (courtSelected === false) {
        	courtSelected = true;
            courtRecorded = courtsList[pos];
            var courtHtml = "";
            if (courtRecorded["wish"] != null && courtRecorded["wish"].length > 0) {
                courtHtml += '<tr class="wishes backgroundReverse"><td class="wishesClickable" name="' + courtRecorded["id"] +
                    '"><i class="small material-icons">info_outline</i>' + courtRecorded["address"] + '</td><td><a class="btn-floating btn-medium waves-effect waves-light">' +
                    '<i class="material-icons">remove</i></a></td>' + '<td style="display: none;">' + courtRecorded["id"] + '</td></tr>';
                var modal = createWishModal(courtRecorded["id"], courtRecorded["address"], courtRecorded["wish"]);
                $('#modalsZone').append(modal);
            } else {
                courtHtml += '<tr class="backgroundReverse"><td>' + courtRecorded["address"] +
                    '</td><td><a class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons">remove</i></a></td>' +
                    '<td style="display: none;">' + courtRecorded["id"] + '</td></tr>';
            }
            $('#selectedCourt').append(courtHtml);       
            checkButtons();
        }
    };

    function removeSelectedCourt() {
        courtSelected = false;
        courtsList.push(courtRecorded);
        courtRecorded = null;
        injectCourtsData(!courtSelected);
        $("#selectedCourt").empty();
    }

    function resetInfos() {
        $('#selectedCourt').empty();
        $('#participatingPairs').empty();
        courtSelected = false;
        $('#tLeader').empty();
        $('#tLeader').append('<option value="" disabled selected>Choissisez le chef d\'équipe</option>');
        $('#tLeader').material_select();
    };

    function insertGroupInfo() {
        for (var pair = 0; pair < playersRecordedList.length; pair++) {
            var html = "";
            var currentPair = playersRecordedList[pair];
            if (currentPair["wish"] != null && currentPair["wish"].length > 0) {
                html += '<tr class="wishes backgroundReverse"><td class="wishesClickable" name="' + currentPair["id"] + '"><i class="small material-icons">info_outline</i>' + 
                	currentPair["name"] + '</td><td><a id=' + currentPair["id"] +
                    ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons">remove</i>' +
                    '</a></td><td style="display: none;">' + currentPair["id"] + '</td></tr>';
                var modal = createWishModal(currentPair["id"], currentPair["name"], currentPair["wish"]);
                $('#modalsZone').append(modal);
            } else {
                html += '<tr class="backgroundReverse"><td>' + currentPair["name"] + '</td><td><a id=' +
                    currentPair["id"] + ' class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons">remove</i>' +
                    '</a></td><td style="display: none;">' + currentPair["id"] + '</td></tr>';
            }
            $('#participatingPairs').append(html);
        }

        var courtHtml = "";
        if (courtRecorded["wish"] != null && courtRecorded["wish"].length > 0) {
            courtHtml += '<tr class="wishes backgroundReverse"><td class="wishesClickable" name="' + courtRecorded["id"] +
                '"><i class="small material-icons">info_outline</i>' + courtRecorded["address"] + '</td><td><a class="btn-floating btn-medium waves-effect waves-light">' +
                '<i class="material-icons">remove</i></a></td>' + '<td style="display: none;">' + courtRecorded["id"] + '</td></tr>';
            var modal = createWishModal(courtRecorded["id"], courtRecorded["address"], courtRecorded["wish"]);
            $('#modalsZone').append(modal);
        } else {
            courtHtml += '<tr class="backgroundReverse"><td>' + courtRecorded["address"] +
                '</td><td><a class="btn-floating btn-medium waves-effect waves-light"><i class="material-icons">remove</i></a></td>' +
                '<td style="display: none;">' + courtRecorded["id"] + '</td></tr>';
        }
        $('#selectedCourt').append(courtHtml);
        courtSelected = true;

        injectCourtsData(!courtSelected);
        updateTeamLeaderSelect();
    }

    $(document).on('click', '.wishesClickable', function() {
        var name = $(this).attr('name');
        $('#modal' + name).openModal();
    });

    $(document).on('click', '#rightArrow', function() {
        if (currentPage < nbPages - 1)
            swipeRightPlayers();
    });

    $(document).on('click', '#leftArrow', function() {
        if (currentPage > 0)
            swipeLeftPlayers();
    });

    $(document).on('click', '#courtRightArrow', function() {
        if (currentPageCourts < nbPagesCourts - 1)
            swipeRightCourts();
    });

    $(document).on('click', '#courtLeftArrow', function() {
        if (currentPage > 0)
            swipeLeftCourts();
    });

    $(document).on('click', '#playersList tbody a', function() {
        addParticipatingPair(this.id);
        playersList.splice(this.id, 1);
        currentPage = 0;
        injectPlayersData();
    });

    $(document).on('click', '#courtsList tbody a', function() {
        addSelectedCourt(this.id);
        courtsList.splice(this.id, 1);
        currentPageCourts = 0;
        injectCourtsData(!courtSelected);
    });

    $(document).on('click', '#participatingPairs a', function() {
        var pos = checkPosParticipatingPair(this.id);
        removeParticipatingPair(pos, this.id);
        updatePairsList();
        checkButtons();
    });

    $(document).on('click', '#selectedCourt a', function() {
        removeSelectedCourt();
        checkButtons();
    });

    $(document).on('click', '.pageNumber', function() {
        var nPage = parseInt($(this).text());
        changePage(nPage);
    });

    $(document).on('click', '.courtPageNumber', function() {
        var nPage = parseInt($(this).text());
        changeCourtPage(nPage);
    });

    $(document).ready(function() {
        var tournamentId = $('#tournamentId').val();
        $('.modal-trigger').leanModal();
        if (window.location.pathname.indexOf('/group/new') > 0 || window.location.pathname.indexOf('/group/edit') > 0) {
            $('#modalsZone').empty();
            var promisePlayers = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/pairsAvailable',
                method: 'GET'
            });
            var promiseCourts = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/courtsAvailable',
                method: 'GET'
            });
            promisePlayers.done(function(result) {
                createPairsList(result);
            });
            promiseCourts.done(function(result) {
                createPaginedCourtsList(result);
            });

            if (window.location.pathname.indexOf('/group/edit/') > 0) {
                var pos = window.location.pathname.indexOf('/group/edit/');
                pos += 12;
                var promiseGroup = $.ajax({
                    url: '/staff/tournaments/edit/' + tournamentId +
                        '/group/groupInfo/' + window.location.pathname.slice(pos),
                    method: 'GET'
                });
                promiseGroup.done(function(result) {
                    playersRecordedList = result["pairs"];
                    courtRecorded = result["court"];
                    insertGroupInfo();
                    $("#nbWiningPairs").val(result["maxWin"].toString());
                    $("#nbWiningPairs").material_select();
                    $("#tLeader").val(result["tLeader"]);
                    $("#tLeader").material_select();
                    checkButtons();
                });
            }
        }
    });

    $('#tLeader').on('change', function() {
        checkButtons();
    });

    $('#reset').on('click', function() {

        if (!$('#reset').hasClass('disabled')) {
            resetInfos();
            checkButtons();
            $('#modalsZone').empty();
            var tournamentId = $('#tournamentId').val();
            var promisePlayers = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/pairsAvailable',
                method: 'GET'
            });
            var promiseCourts = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/courtsAvailable',
                method: 'GET'
            });
            promisePlayers.done(function(result) {
                createPairsList(result);
            });
            promiseCourts.done(function(result) {
                createPaginedCourtsList(result);
            });
        }
    });

    $('#confirm').on('click', function() {
        if (!$('#confirm').hasClass('disabled')) {
            var tournamentId = $('#tournamentId').val();
            var courtAddress = courtRecorded["id"];
            var tLeader = $('#tLeader').val();
            var nbPairsNextStep = $('#nbWiningPairs').val();
            var pairs = [];
            for (var i = 0; i < playersRecordedList.length; i++) {
                pairs.push(playersRecordedList[i]["id"]);
            }
            var params = {
                "pairs": pairs,
                "tLeader": tLeader,
                "nbPairs": nbPairsNextStep,
                "courtId": courtAddress
            };

            params = JSON.stringify(params);

            var persist = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/group/create/' + params,
                method: 'POST',
                success: function(msg) {
                    window.location.href = msg["redirect"];
                }
            });
        }

    });

    $('#validate').on('click', function() {
        if (!$('#validate').hasClass('disabled')) {
            var tournamentId = $('#tournamentId').val();
            var courtAddress = courtRecorded["id"];
            var tLeader = $('#tLeader').val();
            var nbPairsNextStep = $('#nbWiningPairs').val();
            var pairs = [];
            for (var i = 0; i < playersRecordedList.length; i++) {
                pairs.push(playersRecordedList[i]["id"]);
            }
            var pos = window.location.pathname.indexOf('/group/edit/');
            pos += 12;
            var groupId = window.location.pathname.slice(pos);
            var params = {
                "pairs": pairs,
                "tLeader": tLeader,
                "nbPairs": nbPairsNextStep,
                "courtId": courtAddress,
                "groupId": groupId
            };

            params = JSON.stringify(params);

            var persist = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/group/update/' + params,
                method: 'POST',
                success: function(msg) {
                    window.location.href = msg["redirect"];
                }
            });
        }
    });

    $('#sendMail').on('click', function() {
        if (!$('#sendMail').hasClass('disabled')) {
            var tournamentId = $('#tournamentId').val();
            var pos = window.location.pathname.indexOf('/group/edit/');
            pos += 12;
            var groupId = window.location.pathname.slice(pos);
            var persist = $.ajax({
                url: '/staff/tournaments/edit/' + tournamentId + '/group/' + groupId + '/sendMail',
                method: 'POST',
                success: function(msg) {
                    window.location.href = msg["redirect"];
                }
            });
        }
    });
}