var $ = require('jquery');
var config = require('../../../config');

var concatAddress = function (street, streetNumber, zipCode, city) {
      if (street === undefined ||
          streetNumber === undefined ||
          zipCode === undefined ||
          city === undefined) {

          return "Incomplet";
      } else {
          return street.concat(streetNumber, ', ', zipCode, ', ', city);
      }
};

module.exports = function() {

  $("#player-search").on('change keyup copy paste cut', function() {
    var criteria = $("#player-search").val();


    var promise = $.ajax({
      url: "/staff/search/" + criteria,
      method: 'GET'
    });
    promise.done(function(result) {
      console.log(result);
      $('tbody').empty();
      if (result.length > 0) {
        for (var i = 0; i < result.length; i++) {
          var player = result[i];
          $('tbody').append('<tr><td>' + player.firstName + '</td><td>' + player.lastName + '</td><td>' + player.gsm + '</td><td>' + player.email + '</td><td>' + concatAddress(player.street, player.streetNumber, player.zipCode, player.city) + '</td><td><a href="/staff/edit/' + player._id + '">Modifier</a>' + '</td></tr>');
        };
      } else {
        // No results found
      }
    });

  });


  

}