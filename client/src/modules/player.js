var $ = require('jquery');

module.exports = function() {

    /*
        Settings for the datepicker plugin
     */
  var $pick = $('.datepicker').pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',
    selectMonths: true,
    selectYears: 200,
    formatSubmit: 'yyyy/mm/dd',
    monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
    weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mecredi', 'Jeudi', 'Vendredi', 'Samedi'],
    weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined,
    format: 'yyyy/mm/dd',

    // Buttons
    today: 'Ajd',
    clear: 'Effacer',
    close: 'Valider',

  });

  if ($('#bdate').val()) {
    var picker = $pick.pickadate('picker');
    var date = $('#bdate').val();
    var d = new Date(date);
    picker.set('select', d); 
  };

  $('.collapsible').collapsible({
    accordion: true
  });
};