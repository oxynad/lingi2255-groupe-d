var $ = require('jquery');


module.exports = function() {

    function displaySortedPlayers(datas) {
        $('#sortedPlayers').empty();
        var hmtlTab = [];

        var html = '<div class="row">';

        var pairs = JSON.parse(datas["pairs"]);
        var players = JSON.parse(datas["players"]);

        var length = pairs.length;

        var orderedPlayers = [];
        for (var posPlayer = 0; posPlayer < players.length; posPlayer++) {
            orderedPlayers[players[posPlayer]["_id"]] = players[posPlayer];
        }

        for (var pairsPos = 0; pairsPos < length; pairsPos++) {
            var pair = pairs[pairsPos];
            html += '<div class="card col s4 m6 white-text text-center"><div class="blue lighten-1 pair-card">' +
                '<div class="row"><i class="medium material-icons prefix col s3">supervisor_account</i>' +
                '<ul class="col s9">';

            for (var posPlayer = 0; posPlayer < pair["players"].length; posPlayer++) {
                var player = orderedPlayers[pair["players"][posPlayer]["_id"]];
                html += '<li>' + player["firstName"] + ' ' + player["lastName"] + '</li>';
            }
            html += '</ul>' +
                '</div></div><div class="card-content">' +
                '<span class="card-title activator grey-text text-darken-4">' +
                '<i class="material-icons right">settings</i></span><span class="black-text"><p>' +
                'Montant :';
            if (pairs[pairsPos]["montant"] != null) {
                html += pairs[pairsPos]["montant"];
            }
            html += '</p><p>Etat du payement : ';
            if (pairs[pairsPos]["payment"] != null) {
                html += 'Effectué';
            }
            html += '</p></span></div>' +
                '<div class="card-reveal"><span class="card-title grey-text text-darken-4">' +
                'Modification de la paire<i class="material-icons right">close</i></span>' +
                '<form action="/staff/pair/edit/'+pairs[pairsPos]["_id"]+'" method="post"><div class="input-field" style="margin-top: 20px;">' +
                '<input id="montant" type="number" class="validate grey-text" name="montant" value="';
            if (pairs[pairsPos]["montant"] != null) {
                html += pairs[pairsPos]["montant"];
            }
            html += '"><label for="montant" class="black-text">Montant</label>' +
                '</div><div class="input-field grey-text" style="margin-top: 40px;">' +
                '<select name="etatpayement" id="etatpayement">';
            if (pairs[pairsPos]["payment"] != null) {
                html += '<option value="' + pairs[pairsPos]["payment"] + '" selected>Effectué</option>' +
                    '<option value="En Attente">En attente</option>';
            } else {
                html += '<option value="' + pairs[pairsPos]["payment"] + '" selected>En attente</option>' +
                    '<option value="Effectue">Effectué </option>';
            }
            html += '</select><label for="etatpayement" class="black-text active">Etat du payement' +
                '</label></div><button  style="margin-right:10px" class="btn waves-effect waves-light" type="submit" name="action">' +
                'Enregistrer</button>' +
                '<p class="center">' +
                '<a class="btn waves-effect waves-light red" href="/staff/pair/del/{{this._id}}" >' +
                'Supprimer cette paire' +
                '</a>' +
                '</p></form></div><div class="card-action pair-action"><ul>';
            for (var posPlayer = 0; posPlayer < pair["players"].length; posPlayer++) {
                var player = orderedPlayers[pair["players"][posPlayer]["_id"]];
                html += '<li><a href="/staff/edit/' + player["_id"] + '">Modifier ' + player["firstName"] + ' ' +
                    player["lastName"] + '</a></li>';
            }
            html += '</ul></div></div>';
        }
        html += '</div>';
        $('#sortedPlayers').append(html);
        $('select').material_select();
    };

    $('#sortSelect').on('change', function(value) {
        if (window.location.pathname.indexOf('/staff/pair') >= 0) {
            var sortedPlayers = $.ajax({
                url: '/staff/pair/sort/' + $(this).val(),
                method: 'GET'
            });
            sortedPlayers.done(function(result) {
                displaySortedPlayers(result);
            });
        }
    });
}
