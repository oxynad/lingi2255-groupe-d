
var $ = require('jquery');

module.exports = function() {

	$('#AddressCourt').hide();
	if( $("#sameAddress").is(":checked") ) {
		$('#AddressCourt').show();
	} 
	$('#sameAddress').click(function (){
		$('#AddressCourt').toggle(500);
	});
};