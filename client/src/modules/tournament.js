var $ = require('jquery');

module.exports = function() {

  /*
    When encoding a match result, selecting a pair in the dropdown automatically shows the remaining pairs that that pair has to play with
   */
  $("#pair1").on('change', function() {

    var tournamentId = $("input#tournamentId").val();
    var groupId = $("input#groupId").val();

    $.get("/staff/tournaments/edit/" + tournamentId + "/group/encode/" + groupId + "/getValidOpponents/" + this.value, function(res) {
      if (res.length == 0) {
        $("#error-msg").hide().html("Cette paire a deja jouée avec toutes les autres paires").fadeIn(500);
      } else {
        console.log(res);
        $("#pair2").html("");
        var str = "<option value=\"\" disabled selected>Choissisez une paire </option>";
        for (var index in res) {
          // console.log(res[index]);
          $("#pair2").append($("<option>").attr('value', res[index]._id).text(res[index].players[0].firstName + " " + res[index].players[0].lastName + " - " + res[index].players[1].firstName + " " + res[index].players[1].lastName));
        }
        // console.log(str);
        // $("#pair2").html(str);
        $("#pair2").trigger('contentChanged');
      }
    })

  });
  
  $('#pair2').on('contentChanged', function() {
    // re-initialize (update)
    $(this).material_select();
    $("#pair2-wrapper").hide().fadeIn(500);
  });

  $('#pair1_scores').on('focus', function (element) {
    $('#pair1_label').addClass('active');
  });

  $('#pair1_scores').on('blur', function (element) {
    console.log($(this).val());
    if ($(this).val().length === 0) {
      $('#pair1_label').removeClass('active');  
    };
  });

  $('#pair2_score').on('focus', function (element) {
    $('#pair2_label').addClass('active');
  });

  $('#pair2_score').on('blur', function (element) {
    if ($(this).val().length === 0) {
      $('#pair2_label').removeClass('active');  
    };
  });

}