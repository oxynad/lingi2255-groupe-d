var $ = require('jquery');
var config = require('../../../config');

module.exports = function() {

  $('.fDate').each(function(){
    var v = $(this).text();
    var d = new Date(v),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var result = [day, month, year].join('/');
    $(this).text('['+result+']');
  });

  if($("#lat").length && $("#long").length && $("#addressString").length){
   var l = $('#lat').val();
   var lg = $('#long').val();
   var address = $('#addressString').val();

   var myLatLng = {lat: parseFloat(l), lng: parseFloat(lg) };
   var mapCanvas = document.getElementById('map');
   var mapOptions = {
    center: myLatLng,
    zoom: 18,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(mapCanvas, mapOptions); 
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: address
  });
}



$(document).on('click','.clickable-row', function() {
  var outp = $(this).data("href");
  window.location.href = "/court/"+outp;
});

$("#court-search").on('change keyup copy paste cut', function() {
  var criteria = $("#court-search").val();


  if(criteria === '') {
    var promise = $.ajax({
      url: "/courts_list/search/",
      method: 'GET'
    });
    promise.done(function(result) {
      $('tbody').empty();
      for (var i = 0; i < result.length; i++) {
        var court = result[i];
        $('tbody').append('<tr class=\"clickable-row backgroundReverse\" data-href="'+court._id+'"><td>' + court.courtAddress + '</td><td>' + court.courtSurface+ '</td><td>' + court.typeOwner + '</td><th><a href="'+ court._id+'">Modifier</a></th></tr>');
      };
    });

  }else{

    var promise = $.ajax({
      url: "/courts_list/search/" +  criteria,
      method: 'GET'
    });
    promise.done(function (result) {
      $('tbody').empty();
      if (result.length > 0) {          
        for (var i = 0; i < result.length; i++) {
          var court = result[i];
          $('tbody').append('<tr class=\"clickable-row backgroundReverse\" data-href="'+court._id+'"><td>' + court.courtAddress + '</td><td>' + court.courtSurface+ '</td><td>' + court.typeOwner + '</td><th><a href="'+ court._id+'">Modifier</a></th></tr>');
        };
      } else {

        // No results found
      }
    });
  }
});

}