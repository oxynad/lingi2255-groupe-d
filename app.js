/**
 * Start the node application
 *  NODE_ENV is a global variable accessed through process.env and contains one of the following :  'dev', 'prod' & 'test'.  Default is 'dev'
 *  PORT is a global variable accessed through process.env.  Default is 3000.
 */

 var express = require('express');
 var http = require('http');
 var path = require('path');
 var bodyParser = require('body-parser');
 var methodOverride = require('method-override');
 var morgan = require('morgan');
 var passport = require('passport');
 var exphbs = require('express-handlebars');
 var session = require('express-session');
 var logger = require('winston');
 var _ = require("underscore");



 var app = express();
 var env = process.env.NODE_ENV || 'dev';

 if (env == 'test') {
  logger.level = 'error';
} else if (env == 'dev') {
  logger.level = 'info';
  logger.add(logger.transports.File, { filename: "./logs/development.log" });
} else if (env == 'prod') {
  logger.level = 'info';
  logger.add(logger.transports.File, { filename: "./logs/production.log" });
}

/*
 App Start
 */
 logger.info('Launching App');
 var config = require('./config');


// start datalayer

var datalayer = require('./app/datalayer');
datalayer.startUp(config.db.url);


if (env == 'dev') {
  //get argv
  var argv = require('minimist')(process.argv.slice(2));

  if (_.contains(argv._, "fresh-db")) {
    datalayer.clean().then(function() {
      return datalayer.populate();
    });
  }
}

app.set('port', process.env.PORT || 3000);

/*
  Define view templating engine and its layout directory
  */ 
  var handlebarsHelpers = require('./views/helpers');
  app.engine('handlebars', exphbs.create({
    defaultLayout: 'main',
    layoutsDir: app.get('views') + '/layouts',
    helpers: handlebarsHelpers
  }).engine);
  app.set('views', path.join(__dirname, '/views'));
  app.set('view engine', 'handlebars');


  app.use(methodOverride());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));

// Use session
app.use(session(config.session));
app.use(passport.initialize());
app.use(passport.session());

if (env != 'test') {
  app.use(morgan('dev'));
}

/*
  Define express's public server folders
  */
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/bower_components', express.static(__dirname + '/bower_components'));

/*
  General route
  */
  app.use(function(req, res, next) {
  // req.session.user = {username: "admin17"};

  var msgStatus, msgError;

  if (req.session.msgStatus) {
    msgStatus = req.session.msgStatus;
    delete req.session.msgStatus;
  }
  if (req.session.msgError) {
    msgError = req.session.msgError;
    delete req.session.msgError;
  }
  req.msgStatus = msgStatus;
  req.msgError = msgError;

  if (req.session.user) {
    res.locals.username = req.session.user.username;
    if (req.session.user.moderationLevel === 'Admin')
      res.locals.admin = true;
  }

  next();
});

/*
  API routes
  */
  require('./app/api/routes/auth')(app);
  require('./app/api/routes/player')(app);
  require('./app/api/routes/staff')(app);
  require('./app/api/routes/court')(app);
  require('./app/api/routes/owner')(app);
  require('./app/api/routes/tournament')(app);
  require('./app/api/routes/admin')(app);
  require('./app/api/routes/paypal')(app, config.paypal);


/*
  Root route
  */
  app.get('/', function(req, res) {


   res.render('index', {
    homepage: true
  });

 });

/*
  Error route
  */
  app.get('*', function(req, res) {


    res.status(404);
    res.render('404', {
      url: req.url
    });
  });

/*
  Create and launch server
  */
  var server = http.createServer(app);

  var boot = function() {
    server.listen(app.get('port'), function() {
      logger.info('ASMAE website started: ' + app.get('port') + ' (' + env + ')');
    });
    server.on('close', function() {
      logger.info('ASMAE website closed');
    })
  };

  var shutdown = function(done) {
    datalayer.shutDown();
    server.close(done);
  };

/*
  If app.js was ran directly from Node
  */
  if (require.main === module) {
    boot();
  } else {
    exports.boot = boot;
    exports.shutdown = shutdown;
    exports.port = app.get('port');
    exports.server = server;
  }